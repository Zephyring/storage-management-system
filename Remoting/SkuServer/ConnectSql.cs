﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace SkuServer
{
    public class ConnectSql : System.MarshalByRefObject
    {
        private SqlConnection conn;
        private String connString;
        private SqlCommand cmd;
        private SqlDataAdapter sda = new SqlDataAdapter();
        private DataSet ds = new DataSet();      
        private Server server = Server.getInstance();
        private String IPAddress;
        private String databaseName;

        public ConnectSql()
        {
            databaseName = server.databaseNameTextBox.Text;
            try
            {
                if (server.loginMode.Equals("0"))
                    connString = @"server=localhost;Integrated Security=True;database=" + databaseName;
                else
                    connString = @"server=localhost;UID="+server.accText.Text+";Password="+server.passText.Text+";database="+databaseName;
                conn = new SqlConnection(connString);
                conn.Open();
                cmd = new SqlCommand();
                cmd.Connection = conn;

            }
            catch (Exception ex)
            {

            }
        }

        public String getDatabaseName()
        {
            return this.databaseName;
        }

        public DataSet getDataSet(string sqlCommand)
        {
            try
            {

                cmd.CommandText = sqlCommand;

                sda.SelectCommand = cmd;

                sda.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void setClientIPAddress(String IPAddress)
        {
            this.IPAddress = IPAddress;
        }

        public void addText(String text)
        {
            server.textBox.Text = text + server.textBox.Text;
            //server.textBox.Text += "New Connection from " + IPAddress + "! " + System.DateTime.Now + "\r\n";
        }

        public bool isExistReport(String doc, String path)
        {
            String direct = Directory.GetCurrentDirectory() + path;
            //MessageBox.Show(direct);
            if (!Directory.Exists(direct))
                Directory.CreateDirectory(direct);
            String report = direct + doc;
            if (File.Exists(report))
                return true;
            else
                return false;

        }

        public byte[] downReport(String doc, String path)
        {
            String direct = Directory.GetCurrentDirectory() + path;
            if (!Directory.Exists(direct))
                Directory.CreateDirectory(direct);
            String report = direct + doc;
            if (File.Exists(report))
            {
                FileInfo fileinfo = new FileInfo(report);
                BufferedStream inStream = new BufferedStream(new FileStream(report, FileMode.Open));
                byte[] buf = new byte[fileinfo.Length];
                inStream.Read(buf, 0, (int)fileinfo.Length);
                inStream.Close();
                return buf;
            }
            else
                return null;
        }

        public List<String[]> getRunningSlipOrder()
        {
            return server.runningSlipOrder;
        }

        public void addRunningSlipOrder(String slipOrder, String IPAddress, String dateTime)
        {
            String[] s = new String[3] { slipOrder.Trim(), IPAddress.Trim(), dateTime.Trim() };
            server.runningSlipOrder.Add(s);
        }

        public void removeRunningSlipOrder(String slipOrder, String IPAddress, String dateTime)
        {
            for (int i=0; i < server.runningSlipOrder.Count;i++ )
            {
                if (server.runningSlipOrder[i][0].Equals(slipOrder) && server.runningSlipOrder[i][1].Equals(IPAddress) && server.runningSlipOrder[i][2].Equals(dateTime))
                {
                    server.runningSlipOrder.Remove(server.runningSlipOrder[i]);
                }
            }

        }
        public void addNewRunningSlipCount()
        {
            Server.getInstance().newRunningSlipCount += 1;
        }

        public void minusNewRunningSlipCount()
        {
            Server.getInstance().newRunningSlipCount -= 1;
        }

        public int getNewRunningSlipCount()
        {
            return server.newRunningSlipCount;
        }
    }
}
