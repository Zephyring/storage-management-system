﻿namespace SkuServer
{
    partial class Server
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.databaseNameTextBox = new System.Windows.Forms.TextBox();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.accountGB = new System.Windows.Forms.GroupBox();
            this.passText = new System.Windows.Forms.TextBox();
            this.accText = new System.Windows.Forms.TextBox();
            this.accRadio = new System.Windows.Forms.RadioButton();
            this.winRadio = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.accountGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(12, 76);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ReadOnly = true;
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox.Size = new System.Drawing.Size(564, 294);
            this.textBox.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(242, 376);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // databaseNameTextBox
            // 
            this.databaseNameTextBox.Location = new System.Drawing.Point(6, 13);
            this.databaseNameTextBox.Name = "databaseNameTextBox";
            this.databaseNameTextBox.Size = new System.Drawing.Size(95, 21);
            this.databaseNameTextBox.TabIndex = 3;
            this.databaseNameTextBox.Text = "skuDatabase";
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(18, 47);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(95, 23);
            this.confirmBtn.TabIndex = 5;
            this.confirmBtn.Text = "Connect";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.databaseNameTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(103, 40);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database Name";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.accountGB);
            this.groupBox2.Controls.Add(this.accRadio);
            this.groupBox2.Controls.Add(this.winRadio);
            this.groupBox2.Location = new System.Drawing.Point(121, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(455, 69);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Authentication Mode";
            // 
            // accountGB
            // 
            this.accountGB.Controls.Add(this.passText);
            this.accountGB.Controls.Add(this.accText);
            this.accountGB.Location = new System.Drawing.Point(143, 13);
            this.accountGB.Name = "accountGB";
            this.accountGB.Size = new System.Drawing.Size(306, 49);
            this.accountGB.TabIndex = 3;
            this.accountGB.TabStop = false;
            this.accountGB.Text = "Account               Password";
            this.accountGB.Visible = false;
            // 
            // passText
            // 
            this.passText.Location = new System.Drawing.Point(139, 20);
            this.passText.Name = "passText";
            this.passText.Size = new System.Drawing.Size(161, 21);
            this.passText.TabIndex = 3;
            this.passText.UseSystemPasswordChar = true;
            // 
            // accText
            // 
            this.accText.Location = new System.Drawing.Point(6, 20);
            this.accText.Name = "accText";
            this.accText.Size = new System.Drawing.Size(127, 21);
            this.accText.TabIndex = 2;
            // 
            // accRadio
            // 
            this.accRadio.AutoSize = true;
            this.accRadio.Location = new System.Drawing.Point(6, 16);
            this.accRadio.Name = "accRadio";
            this.accRadio.Size = new System.Drawing.Size(119, 16);
            this.accRadio.TabIndex = 1;
            this.accRadio.Text = "Account/Password";
            this.accRadio.UseVisualStyleBackColor = true;
            // 
            // winRadio
            // 
            this.winRadio.AutoSize = true;
            this.winRadio.Checked = true;
            this.winRadio.Location = new System.Drawing.Point(6, 46);
            this.winRadio.Name = "winRadio";
            this.winRadio.Size = new System.Drawing.Size(113, 16);
            this.winRadio.TabIndex = 0;
            this.winRadio.TabStop = true;
            this.winRadio.Text = "Windows Default";
            this.winRadio.UseVisualStyleBackColor = true;
            this.winRadio.CheckedChanged += new System.EventHandler(this.winRadio_CheckedChanged);
            // 
            // Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 411);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.confirmBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Server";
            this.Text = "Server";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.accountGB.ResumeLayout(false);
            this.accountGB.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox databaseNameTextBox;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox accountGB;
        private System.Windows.Forms.RadioButton accRadio;
        private System.Windows.Forms.RadioButton winRadio;
        public System.Windows.Forms.TextBox passText;
        public System.Windows.Forms.TextBox accText;
    }
}

