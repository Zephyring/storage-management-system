﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Tcp;
using System.Net.NetworkInformation;

namespace SkuServer
{
    public partial class Server : Form
    {
        private static Server server;
        private String databaseName;
        private DateTime reportTime;
        private DataSet slipDs;
        private DataSet skuDs;
        private DataSet skuDcp;
        private DataTable skuDt = new DataTable();
        private DataSet itemDs;
        private DataTable itemDt = new DataTable();
        private DataSet itemDcp;
        private DataSet newItemDs;
        private DataTable newItemDt = new DataTable();
        private System.Windows.Forms.Timer t = new Timer();//report timer
        private System.Windows.Forms.Timer tt = new Timer();//unlock slip order timer
        
        public String loginMode = "0";//windows default 0;  account/password 1
        public List<String[]> runningSlipOrder = new List< String[]>();
        public int newRunningSlipCount = 0;

        public Server()
        {
            InitializeComponent();            
        }

        private void initRegister()
        {
            databaseName = this.databaseNameTextBox.Text;
            RegisterChannel();
            t.Start();
            tt.Start();
            timerCount();          
        }

        private void RegisterChannel()
        {
            if (confirmBtn.Text.Equals("Connect"))
            {
                int port = 6666;
                TcpServerChannel channel = new TcpServerChannel("channel",port);
                ChannelServices.RegisterChannel(channel, false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(ConnectSql), "ConnectSql", WellKnownObjectMode.SingleCall);
                ConnectSql conn = new ConnectSql();
                try
                {
                    if (conn.getDataSet("use " + databaseName + " select count(*) from item").Tables[0].Rows[0][0] != null)
                        textBox.Text = ("Server is successfully connected to database " + databaseName + ".\r\n") + textBox.Text;
                }
                catch(Exception ex)
                {
                    ChannelServices.UnregisterChannel(ChannelServices.GetChannel("channel"));
                    textBox.Text = "Some errors occur when connecting to database.\r\n" + textBox.Text;
                    confirmBtn.Text = "Disconnect";
                    databaseNameTextBox.ReadOnly = !databaseNameTextBox.ReadOnly;
                    accText.ReadOnly = !accText.ReadOnly;
                    passText.ReadOnly = !passText.ReadOnly;
                    accRadio.Enabled = !accRadio.Enabled;
                    winRadio.Enabled = !winRadio.Enabled;
                }               

                
            }
            else
            {
                ChannelServices.UnregisterChannel(ChannelServices.GetChannel("channel"));
                textBox.Text = ("Server is disconnected.\r\n") + textBox.Text;
            }
        }

  
        public static Server getInstance()
        {
            return server;
        }

        public static void setInstance(Server _server)
        {
            server = _server;
        }

        private void timerCount()
        {
            DateTime now = DateTime.Now;
            if (now.Hour >= 20)
                reportTime = Convert.ToDateTime((now.AddDays(1)).ToString("yyyy-MM-dd ") + "20:00:00");
            else
                reportTime = Convert.ToDateTime(now.ToString("yyyy-MM-dd ") + "20:00:00");
            double ts = Math.Ceiling(reportTime.Subtract(now).Duration().TotalSeconds);

            t.Interval = 1000 * (int)ts;
            
            //System.Timers.Timer t = new System.Timers.Timer(1000);
            //t.AutoReset = false;
            t.Tick += new EventHandler(writeDailyReport);
            
            //t.Stop();
            tt.Interval = 1000 * 5;
            tt.Tick+=new EventHandler(maintain);
            
        }

        private void maintain(object sender, EventArgs e)
        {
            //maintain server
            
            /*
            Ping p;
            PingReply reply;
            foreach (String[] s in runningSlipOrder)
            {
                p = new Ping();
                reply = p.Send(s[1], 3000);
                textBox.Text += "Ping to: " + s[1] + " about SlipOrder: " + s[0] + " at time: " + s[2] + " " + reply.Status + "\r\n";
                if (reply.Status != IPStatus.Success)
                {
                    MessageBox.Show(s[1]+" is offline."+" Count: "+newRunningSlipCount);
                }
            }
             * */
            for (int i = 0; i < runningSlipOrder.Count; i++)
            {
                DateTime t = Convert.ToDateTime(runningSlipOrder[i][2]);
                if (DateTime.Now.Subtract(t).Hours >= 4)
                {
                    runningSlipOrder.RemoveAt(i);
                }
            }
                        
        }
        private void writeDailyReport(object sender, EventArgs e)
        {
            //maintain server
            runningSlipOrder.Clear();
            newRunningSlipCount = 0;

            t.Tick -= new EventHandler(writeDailyReport);
            textBox.Text = "Server is generating report.\r\n" + textBox.Text;
            
            //write report
            String str = reportTime.ToString("yyyy-MM-dd");
            String shipDate = reportTime.ToShortDateString() + " ";
            Document report = new Document(PageSize.A4);
            String direct = Directory.GetCurrentDirectory() + @"\Report\Daily\";
            if(!Directory.Exists(direct))
                Directory.CreateDirectory(direct);
            PdfWriter writer = PdfWriter.GetInstance(report, new FileStream( direct + "D"+ str + ".pdf" , FileMode.Create));
            report.Open();
            report.Add(new Paragraph("Daily Report: " + str +"\n"));
            ConnectSql connSO = new ConnectSql();
            slipDs = connSO.getDataSet("use " + databaseName + " select slipOrder.slipOrder, name, address, shipdate from slipOrder where charindex('" + shipDate + "',shipDate)!=0");
            ConnectSql connDes = new ConnectSql();
            ConnectSql connSKU = new ConnectSql();
            if (slipDs!= null)
            {
                for (int i = 0; i < slipDs.Tables[0].Rows.Count; i++ )
                {
                    string[] sArray = slipDs.Tables[0].Rows[i][2].ToString().Trim().Split('@');
                    String address = "";
                    int n = 0;
                    foreach (string s in sArray)
                    {
                        n++;
                        if (n!=sArray.Length)
                        address += s.ToString() + " ";
                    }
                    float[] widthTitle= {0.3f,0.7f};
                    PdfPTable table = new PdfPTable(widthTitle);
                    table.SpacingBefore = 15f;
                    table.AddCell("SlipOrder");
                    table.AddCell(slipDs.Tables[0].Rows[i][0].ToString().Trim());
                    table.AddCell("ShipDate");
                    table.AddCell(Convert.ToDateTime(slipDs.Tables[0].Rows[i][3]).ToString("yyyy-MM-dd"));
                    table.AddCell("Customer Name");
                    table.AddCell(slipDs.Tables[0].Rows[i][1].ToString().Trim());
                    table.AddCell("Address");
                    table.AddCell(address);
                    table.WidthPercentage = 100;
                    report.Add(table);
                    connSKU = new ConnectSql();
                    skuDs = connSKU.getDataSet("use " + databaseName + " select sku, skuQuantity from slipOrder_sku where slipOrder='" + slipDs.Tables[0].Rows[i][0].ToString() + "'");
                    float[] widthDetail = { 0.2f, 0.5f, 0.1f, 0.2f };
                    if (skuDs != null)
                    {

                        table = new PdfPTable(widthDetail);
                        table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        PdfPCell cell = new PdfPCell(new Phrase("Details"));
                        cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        cell.PaddingTop = 3.5f;
                        cell.FixedHeight = 20;
                        cell.Colspan = 4;
                        cell.BorderWidthTop = 1.5f;
                        table.AddCell(cell);
                        table.DefaultCell.BackgroundColor = new BaseColor(200, 200, 200);
                        table.AddCell("SKU#");
                        table.AddCell("Description");
                        table.AddCell("Quantity");
                        table.AddCell("Remarks");
                        table.DefaultCell.BackgroundColor = BaseColor.WHITE;
                        for (int k = 0; k < skuDs.Tables[0].Rows.Count; k++)
                        {
                            connDes = new ConnectSql();
                            skuDcp = connDes.getDataSet("use " + databaseName + " select productDetails from sku where sku.sku='" + skuDs.Tables[0].Rows[k][0].ToString() + "'");
                            table.AddCell(skuDs.Tables[0].Rows[k][0].ToString().Trim());
                            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            table.AddCell(skuDcp.Tables[0].Rows[0][0].ToString().Trim());
                            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(skuDs.Tables[0].Rows[k][1].ToString().Trim());
                            table.AddCell("");
                        }
                        table.WidthPercentage = 100;
                        report.Add(table);
                        itemDt = new DataTable();
                        DataColumn dc10 = new DataColumn();
                        DataColumn dc11 = new DataColumn();
                        itemDt.Columns.Add(dc10);
                        itemDt.Columns.Add(dc11);
                        ConnectSql connItem = new ConnectSql();
                        for (int k = 0; k < skuDs.Tables[0].Rows.Count; k++)
                        {
                            connItem = new ConnectSql();
                            itemDs = connItem.getDataSet("use " + databaseName + " select item, itemQuantity from sku_item where sku='" + skuDs.Tables[0].Rows[k][0].ToString().Trim() + "'");

                            foreach (DataRow itemDsRow in itemDs.Tables[0].Rows)
                            {
                                Boolean isNew = true;
                                foreach (DataRow r in itemDt.Rows)
                                {
                                    if (r[0].ToString().Equals(itemDsRow[0].ToString()))
                                    {
                                        isNew = false;
                                        break;
                                    }
                                }
                                if (isNew)
                                {
                                    DataRow newRow = itemDt.NewRow();
                                    newRow[0] = itemDsRow[0];
                                    newRow[1] = Convert.ToInt32(itemDsRow[1]) * Convert.ToInt32(skuDs.Tables[0].Rows[k][1]);
                                    itemDt.Rows.Add(newRow);
                                }
                                else
                                {
                                    foreach (DataRow itemDtRow in itemDt.Rows)
                                    {
                                        if (itemDtRow[0].ToString().Equals(itemDsRow[0].ToString()))
                                        {
                                            itemDtRow[1] = (Convert.ToInt32(itemDtRow[1]) + Convert.ToInt32(itemDsRow[1]) * Convert.ToInt32(skuDs.Tables[0].Rows[k][1])).ToString();
                                        }
                                    }
                                }
                            }
                        }
                         ConnectSql connNewItem = new ConnectSql();
                         newItemDs = connNewItem.getDataSet("use " + databaseName + " select item, itemQuantity from slipOrder_newItem where slipOrder='" + slipDs.Tables[0].Rows[i][0].ToString() + "'");
                    foreach (DataRow dtRow in newItemDs.Tables[0].Rows)
                    {
                        Boolean isNew = true;
                        foreach (DataRow r in itemDt.Rows)
                        {
                            if (r[0].ToString().Trim().Equals(dtRow[0].ToString().Trim()))
                            {
                                isNew = false;
                                break;
                            }
                        }
                        if (isNew)
                        {
                            DataRow newRow = itemDt.NewRow();
                            newRow[0] = dtRow[0];
                            newRow[1] = Convert.ToInt32(dtRow[1]);
                            itemDt.Rows.Add(newRow);
                        }
                        else
                        {
                            for (int k = 0; k < itemDt.Rows.Count; k++)
                            {
                                if (itemDt.Rows[k][0].ToString().Trim().Equals(dtRow[0].ToString().Trim()))
                                {
                                    int quantity = Convert.ToInt32(itemDt.Rows[k][1]) + Convert.ToInt32(dtRow[1]);
                                    if (quantity == 0)
                                    {
                                        itemDt.Rows.Remove(itemDt.Rows[k]);
                                    }
                                    else
                                    {
                                        itemDt.Rows[k][1] = quantity.ToString();
                                    }
                                }
                            }
                        }
                    }
                           table = new PdfPTable(widthDetail);
                        table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        table.DefaultCell.BackgroundColor = new BaseColor(200, 200, 200);
                        table.AddCell("Item#");
                        table.AddCell("Description");
                        table.AddCell("Quantity");
                        table.AddCell("Remarks");
                        table.DefaultCell.BackgroundColor = BaseColor.WHITE;
                        for (int k = 0; k < itemDt.Rows.Count; k++)
                        {
                            connDes = new ConnectSql();
                            itemDcp = connDes.getDataSet("use " + databaseName + " select description from item where item.item='" + itemDt.Rows[k][0].ToString().Trim() + "'");
                            table.AddCell(itemDt.Rows[k][0].ToString().Trim());
                            table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            table.AddCell(itemDcp.Tables[0].Rows[0][0].ToString().Trim());
                            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            table.AddCell(itemDt.Rows[k][1].ToString().Trim());
                            table.AddCell("");
                        }
                        table.WidthPercentage = 100f;
                        table.SpacingAfter = 30f;
                        report.Add(table);

                }
                
            }
            report.Close();
            textBox.Text = "Daily report is generated.\r\n" + textBox.Text;
            
            if (reportTime.DayOfWeek.ToString() == "Friday")
            {
                writeDaysReport(7);
            }
            if (reportTime.AddDays(1).Day.ToString() == "1")
            {
                writeDaysReport(Convert.ToInt32(reportTime.Day));
            }
            timerCount();
        }
        }

        private void writeDaysReport(int days)
        {
            ConnectSql connDes = new ConnectSql();
            Document report = new Document(PageSize.A4);
            String direct;
            PdfWriter writer;
            String shipDate = reportTime.AddDays(-days + 1).ToShortDateString() + " ";
            if (days == 7)
                direct = Directory.GetCurrentDirectory() + @"\Report\Weekly\";
            else
                direct = Directory.GetCurrentDirectory() + @"\Report\Monthly\";
            if (!Directory.Exists(direct))
                Directory.CreateDirectory(direct);
            if (days == 7)
            {
                writer = PdfWriter.GetInstance(report, new FileStream(direct + "W" + reportTime.ToString("yyyy-MM-dd") + ".pdf", FileMode.Create));
                report.Open();
                textBox.Text = "Weekly report is generated.\r\n" + textBox.Text;
                report.Add(new Paragraph("Weekly Report: " + reportTime.ToString("yyyy-MM-dd")));
            }
            else
            {
                writer = PdfWriter.GetInstance(report, new FileStream(direct + "M" + reportTime.ToString("yyyy-MM") + ".pdf", FileMode.Create));
                report.Open();
                textBox.Text = "Monthly report is generated.\r\n" + textBox.Text;
                report.Add(new Paragraph("Monthly Report: " + reportTime.ToString("yyyy-MM")));
            }
            ConnectSql connSO = new ConnectSql();
            for (int i = 0; i <= days ; i++)
            {
                slipDs = connSO.getDataSet("use " + databaseName + " select slipOrder.slipOrder from slipOrder where charindex('" + shipDate + "',shipDate)!=0");
                shipDate = ((Convert.ToDateTime(shipDate).AddDays(1)).ToShortDateString() + " ");
            }
            skuDt = new DataTable();
            DataColumn dc0 = new DataColumn();
            DataColumn dc1 = new DataColumn();
            skuDt.Columns.Add(dc0);
            skuDt.Columns.Add(dc1);
            ConnectSql connSKU = new ConnectSql();
            foreach (DataRow slipDsRow in slipDs.Tables[0].Rows)
            {
                skuDs = connSKU.getDataSet("use " + databaseName + " select sku, skuQuantity from slipOrder_sku where slipOrder='" + slipDsRow[0].ToString() + "'");
            }
            if (skuDs != null)
            {
                foreach (DataRow skuDsRow in skuDs.Tables[0].Rows)
                {
                    Boolean isNew = true;
                    foreach (DataRow r in skuDt.Rows)
                    {
                        if (r[0].ToString().Equals(skuDsRow[0].ToString()))
                        {
                            isNew = false;
                            break;
                        }
                    }
                    if (isNew)
                    {
                        DataRow newRow = skuDt.NewRow();
                        newRow[0] = skuDsRow[0];
                        newRow[1] = skuDsRow[1];
                        skuDt.Rows.Add(newRow);
                    }
                    else
                    {
                        foreach (DataRow dtRow in skuDt.Rows)
                        {
                            if (dtRow[0].ToString().Equals(skuDsRow[0].ToString()))
                            {
                                dtRow[1] = (Convert.ToInt32(dtRow[1]) + Convert.ToInt32(skuDsRow[1])).ToString();
                            }
                        }
                    }
                }
            }
            newItemDt = new DataTable();
            DataColumn dc10 = new DataColumn();
            DataColumn dc11 = new DataColumn();
            newItemDt.Columns.Add(dc10);
            newItemDt.Columns.Add(dc11);
            ConnectSql connItem = new ConnectSql();
            foreach (DataRow slipDsRow in slipDs.Tables[0].Rows)
            {
                newItemDs = connItem.getDataSet("use " + databaseName + " select item, itemQuantity from slipOrder_newItem where slipOrder='" + slipDsRow[0].ToString() + "'");
            }
            
            if (newItemDs != null)
            {
                foreach (DataRow newItemRow in newItemDs.Tables[0].Rows)
                {
                    Boolean isNew = true;
                    foreach (DataRow r in newItemDt.Rows)
                    {
                        if (r[0].ToString().Equals(newItemRow[0].ToString()))
                        {
                            isNew = false;
                            break;
                        }
                    }
                    if (isNew)
                    {
                        DataRow newRow = newItemDt.NewRow();
                        newRow[0] = newItemRow[0];
                        newRow[1] = newItemRow[1];
                        newItemDt.Rows.Add(newRow);
                    }
                    else
                    {
                        foreach (DataRow dtRow in newItemDt.Rows)
                        {
                            if (dtRow[0].ToString().Equals(newItemRow[0].ToString()))
                            {
                                dtRow[1] = (Convert.ToInt32(dtRow[1]) + Convert.ToInt32(newItemRow[1])).ToString();
                            }
                        }
                    }
                }
            }
            itemDt = new DataTable();
            DataColumn dc20 = new DataColumn();
            DataColumn dc21 = new DataColumn();
            itemDt.Columns.Add(dc20);
            itemDt.Columns.Add(dc21);
            ConnectSql connNewItem = new ConnectSql();
            foreach (DataRow skuDtRow in skuDt.Rows)
            {
                connNewItem = new ConnectSql();
                itemDs = connNewItem.getDataSet("use " + databaseName + " select item, itemQuantity from sku_item where sku='" + skuDtRow[0].ToString() + "'");


                //if (itemDs != null)
                //{
                foreach (DataRow itemDsRow in itemDs.Tables[0].Rows)
                {
                    Boolean isNew = true;
                    foreach (DataRow r in itemDt.Rows)
                    {
                        if (r[0].ToString().Equals(itemDsRow[0].ToString()))
                        {
                            isNew = false;
                            break;
                        }
                    }
                    if (isNew)
                    {
                        DataRow newRow = itemDt.NewRow();
                        newRow[0] = itemDsRow[0];
                        newRow[1] = Convert.ToInt32(itemDsRow[1]) * Convert.ToInt32(skuDtRow[1]);
                        itemDt.Rows.Add(newRow);
                    }
                    else
                    {
                        foreach (DataRow itemDtRow in itemDt.Rows)
                        {
                            if (itemDtRow[0].ToString().Equals(itemDsRow[0].ToString()))
                            {
                                itemDtRow[1] = (Convert.ToInt32(itemDtRow[1]) + Convert.ToInt32(itemDsRow[1]) * Convert.ToInt32(skuDtRow[1])).ToString();
                            }
                        }
                    }
                }
            }
            foreach (DataRow dtRow in newItemDt.Rows)
            {
                Boolean isNew = true;
                foreach (DataRow r in itemDt.Rows)
                {
                    if (r[0].ToString().Trim().Equals(dtRow[0].ToString().Trim()))
                    {
                        isNew = false;
                        break;
                    }
                }
                if (isNew)
                {
                    DataRow newRow = itemDt.NewRow();
                    newRow[0] = dtRow[0];
                    newRow[1] = Convert.ToInt32(dtRow[1]);
                    itemDt.Rows.Add(newRow);
                }
                else
                {
                    for (int i = 0; i < itemDt.Rows.Count; i++)
                    {
                        if (itemDt.Rows[i][0].ToString().Trim().Equals(dtRow[0].ToString().Trim()))
                        {
                            int quantity = Convert.ToInt32(itemDt.Rows[i][1]) + Convert.ToInt32(dtRow[1]);
                            if (quantity == 0)
                            {
                                itemDt.Rows.Remove(itemDt.Rows[i]);
                            }
                            else
                            {
                                itemDt.Rows[i][1] = quantity.ToString();
                            }
                        }
                    }
                }
                
            }
            float[] widthSKU = { 0.2f, 0.5f, 0.1f, 0.2f }; 
            PdfPTable table = new PdfPTable(widthSKU);
            table.SpacingBefore = 15f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            PdfPCell title = new PdfPCell(new Paragraph("SKU Sales Status"));
            title.Colspan =4;
            title.FixedHeight = 25;
            title.VerticalAlignment = Element.ALIGN_MIDDLE;
            title.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(title);
            table.AddCell("SKU#");
            table.AddCell("Description");
            table.AddCell("Quantity");
            table.AddCell("Remarks");
            if (skuDt != null)
            {
                for (int i = 0; i < skuDt.Rows.Count; i++)
                {
                    connDes = new ConnectSql();
                    skuDcp = connDes.getDataSet("use " + databaseName + " select productDetails from sku where sku.sku='" + skuDt.Rows[i][0].ToString().Trim() + "'");
                    table.AddCell(skuDt.Rows[i][0].ToString().Trim());
                    table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(skuDcp.Tables[0].Rows[0][0].ToString().Trim());
                    table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(skuDt.Rows[i][1].ToString().Trim());
                    table.AddCell("");
                }
            }
            else
            {
                table.AddCell("Null");
                table.AddCell("");
                table.AddCell("");
                table.AddCell("");
            }
            table.WidthPercentage = 100f;
            report.Add(table);
            float[] widthItem = { 0.17f, 0.5f, 0.1f, 0.1f, 0.13f }; 
            table = new PdfPTable(widthItem);
            table.SpacingBefore = 40f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            title.Phrase = new Paragraph("Item Sales Status");
            title.Colspan = 5;
            table.AddCell(title);
            table.AddCell("Item#");
            table.AddCell("Description");
            table.AddCell("Quantity");
            table.AddCell("Rest");
            table.AddCell("Remarks");
            if (itemDt != null)
            {
                for (int i = 0; i < itemDt.Rows.Count; i++)
                {
                    connDes = new ConnectSql();
                    itemDcp = connDes.getDataSet("use " + databaseName + " select description,quantity from item where item.item='" + itemDt.Rows[i][0].ToString().Trim() + "'");
                    table.AddCell(itemDt.Rows[i][0].ToString().Trim());
                    table.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    table.AddCell(itemDcp.Tables[0].Rows[0][0].ToString().Trim());
                    table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    table.AddCell(itemDt.Rows[i][1].ToString().Trim());
                    table.AddCell(itemDcp.Tables[0].Rows[0][1].ToString().Trim());
                    table.AddCell("");
                }
            }
            else
            {
                table.AddCell("Null");
                table.AddCell("");
                table.AddCell("");
                table.AddCell("");
                table.AddCell("");
            }
            table.WidthPercentage = 100f;
            report.Add(table);
            report.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Do you want to close the server?", "Close Operation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            databaseNameTextBox.ReadOnly = !databaseNameTextBox.ReadOnly;
            accText.ReadOnly = !accText.ReadOnly;
            passText.ReadOnly = !passText.ReadOnly;
            accRadio.Enabled = !accRadio.Enabled;
            winRadio.Enabled = !winRadio.Enabled;
            initRegister();
            if (databaseNameTextBox.ReadOnly)
                confirmBtn.Text = "Disconnect";
            else confirmBtn.Text = "Connect";
        }

        private void winRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (winRadio.Checked)
            {
                accountGB.Visible = false;
                loginMode = "0";
            }
            else
            {
                accountGB.Visible = true;
                loginMode = "1";
            }

        }

        
        
    }
}
