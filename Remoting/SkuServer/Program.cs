﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SkuServer
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Server server = new Server();
            Server.setInstance(server);
            Application.Run(Server.getInstance());
        }
    }
}
