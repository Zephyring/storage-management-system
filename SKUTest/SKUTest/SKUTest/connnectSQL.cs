﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace DockSample.DataSource
{
    public class SQLServer 
    {
        private SqlConnection connection;
        private SqlDataReader reader;
        private SqlCommand command;
        private String provider;
        private String serverName;
        private String userName;
        private String passWord;
        private String databaseName;
        private int AuthenticationMode = 0;  //0浠ｈ〃Windows Authentication锛?1浠ｈ〃Sql Server Authentication

        /*
        public SqlConnection getSQLServerConnection()
        {
            string connectionString = "server=(" + this.getServerName() + ");Integrated Security=SSPI;database=" + this.getDatabaseName();
            this.connection = new SqlConnection(@"Initial Catalog=mydb;Data Source=CD5D5864647D4FE\SQLEXPRESS;Integrated Security=SSPI");
            try
            {
                this.connection.Open();
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return this.connection;
        }
         */

        //By LiTao
        public SqlConnection getSQLServerConnection()
        {
            if (this.AuthenticationMode == 0)
            {
                this.connection = new SqlConnection(@"Data Source=" + this.getServerName() + ";Integrated Security=True;Initial Catalog=" + this.getDatabaseName());
            }
            else
            {
                this.connection = new SqlConnection(@"Data Source=" + this.getServerName() + ";UID=" + this.getUserName() + ";Password=" + this.getPassword() + ";Initial Catalog=" + this.getDatabaseName());
            }
            try
            {
                this.connection.Open();
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return this.connection;
        }

        public SqlDataReader executeReader(string sqlCommand)
        {
            this.command = this.connection.CreateCommand();
            this.command.CommandText = sqlCommand;
            this.reader = this.command.ExecuteReader();
            return reader;
        }

        public object executeScalar(string sqlCommand)
        {
            this.command = this.connection.CreateCommand();
            this.command.CommandText = sqlCommand;
            return command.ExecuteScalar();
        }

        public void executeNonQuery(string sqlCommand)
        {
            this.command = this.connection.CreateCommand();
            this.command.CommandText = sqlCommand;
            command.ExecuteNonQuery();
        }

        public void close()
        {
            if (this.reader != null)
            {
                reader.Close();
            }
            if (this.connection != null)
            {
                connection.Close();
            }
        }


        public List<string> showTables(String databaseName)
        {
            this.setDatabaseName(databaseName);
            this.getSQLServerConnection();
            SqlDataReader reader = this.executeReader("USE " + this.getDatabaseName() + " SELECT name FROM sys.sysobjects WHERE type='U' ORDER BY name");
            List<string> result = new List<string> ();

            while (reader.Read())
            {
                for (int i = 0; i<reader.FieldCount; i++)
                {
                    result.Add(reader[i].ToString());
                }
            }
            this.close();
            return result;
        }
        public List<string>  showFields(String databaseName, String tableName)
        {
            this.setDatabaseName(databaseName);
            List<string>  result = new List<string> ();
            result = this.showFields(tableName);
            return result;
        }
        public List<string>  showFields(String tableName)
        {
            this.getSQLServerConnection();
            SqlDataReader reader = this.executeReader("Select Name From SysColumns Where id=Object_Id('" + tableName + "')");
            List<string>  result = new List<string> ();
            for (int i = 0; reader.Read(); )
            {
                result.Add(reader[0].ToString());
            }
            this.close();
            return result;
        }

        //By LiTao
        public List<string>  showDatabases()
        {
            this.getSQLServerConnection();
            SqlDataReader reader = this.executeReader("SELECT Name FROM Master..SysDatabases ORDER BY Name");
            List<string>  result = new List<string> ();
            if (reader != null)
            {
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        result.Add(reader[i].ToString());
                    }
                }
            }
            this.close();
            return result;
        }

        //By LiTao
        public int getAuthenticationMode()
        {
            return this.AuthenticationMode;
        }

        //By LiTao
        public void setAuthenticationMode(int mode)
        {
            this.AuthenticationMode = mode;
        }

        public string getProvider()
        {
            return this.provider;
        }

        public void setProvider(string provider)
        {
            this.provider = provider;
        }

        public string getServerName()
        {
            return this.serverName;
        }

        public void setServerName(string serverName)
        {
            this.serverName = serverName;
        }

        public string getDatabaseName()
        {
            return this.databaseName;
        }

        public void setDatabaseName(string databaseName)
        {
            this.databaseName = databaseName;
        }

        public string getUserName()
        {
            return this.userName;
        }

        public void setUserName(string userName)
        {
            this.userName = userName;
        }

        public string getPassword()
        {
            return this.passWord;
        }

        public void setPassword(string password)
        {
            this.passWord = password;
        }
    }
}