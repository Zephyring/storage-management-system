﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SKUTest
{
    public partial class SKU : Form
    {
        private DataSet skuDataSet;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
        
        private String sku;
        private Boolean failure = false;
        private AddItem addItem = null;
        private Boolean hasClickSaveBtn = true;
        private String currentValue;
        
        public Boolean newASku;//used for judging whether it is check a sku or new a sku
        public DataTable finalTable = null;
        public List<String> newItems = new List<String>();//signaled by AddItem winform to add new item
        public List<String> deleteItems = new List<String>();//signaled by AddItem winform to delete item

        public SKU()
       {
            InitializeComponent();
            initFinalTable();
            initSku();
            newASku = true;//new a sku
        }

        public SKU(string sku)
        {
            this.sku = sku;
            InitializeComponent();
            initFinalTable();
            initSkuDetails();
            newASku = false;//check a sku
        }

        private void initFinalTable()
        {
            finalTable = new DataTable();
            DataColumn dc0 = new DataColumn("Item#");
            DataColumn dc1 = new DataColumn("Description");
            DataColumn dc2 = new DataColumn("Quantity");
            finalTable.Columns.Add(dc0);
            finalTable.Columns.Add(dc1);
            finalTable.Columns.Add(dc2);
            itemDataGridView.DataSource = finalTable;
            itemDataGridView.Columns[0].Width = 100;
            itemDataGridView.Columns[1].Width = 295;
            itemDataGridView.Columns[2].Width = 100;
            itemDataGridView.ReadOnly = true;  

        }

       

        private void initSku()
        {
            //new a sku
            //generate sku number by the max value plus one
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            skuDataSet = conn.getDataSet("use " + databaseName + " select count(*) from sku");
            conn.setClientIPAddress(localIPAddress);

            skuGroupBox.Text = (Convert.ToInt32(skuDataSet.Tables[0].Rows[0][0].ToString().Trim()) + 1000).ToString();
            sku = skuGroupBox.Text;
        }

        private void initSkuDetails()
        {
            //check a sku
            skuGroupBox.Text = sku;
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            skuDataSet = conn.getDataSet("use " + databaseName + " select * from sku where sku='" + sku + "'");
            conn.setClientIPAddress(localIPAddress);

            DataTable dt = skuDataSet.Tables[0];

            //row[0][1] stores product details of that sku
            productDetailsText.Text = dt.Rows[0][1].ToString().Trim();
            skuText.Text = dt.Rows[0][2].ToString().Trim();
            sizeText.Text = dt.Rows[0][3].ToString().Trim();
            weightText.Text = dt.Rows[0][4].ToString().Trim();
            priceText.Text = dt.Rows[0][5].ToString().Trim();

            initItemGridView();
        }

        private void initItemGridView()
        {
            try
            {
                //init all items that belong to this sku
                SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                DataSet sku_item_DataSet = conn1.getDataSet("use " + databaseName + " select * from sku_item where sku='" + sku + "'");
                conn1.setClientIPAddress(localIPAddress);

                foreach (DataRow row in sku_item_DataSet.Tables[0].Rows)
                {
                    DataRow temp = finalTable.NewRow();
                    temp[0] = row[1];//item#
                    temp[1] = conn1.getDataSet("use " + databaseName + " select description from item where item='" + row[1].ToString() + "';").Tables[0].Rows[0][0].ToString();//description
                    temp[2] = row[2];//itemQuantity
                    finalTable.Rows.Add(temp);
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }

        }
        


        private void saveBtn_Click(object sender, EventArgs e)
        {
            hasClickSaveBtn = true;
            String warningMsg;
            if (finalTable.Rows.Count == 0)
                warningMsg = "There are currently no items in this SKU, do you want to save?";
            else
                warningMsg = "Do you want to save?";
            if (MessageBox.Show(this, warningMsg, "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    if (skuText.Text == "")
                        MessageBox.Show("Please input SKU#");
                    else if (productDetailsText.Text == "")
                        MessageBox.Show("Please input product details");
                    else 
                    {
                        if (!newASku)
                        {
                            //check sku

                            //update data about productDescription

                            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                            conn.getDataSet("use " + databaseName + " update sku set productDetails='" + productDetailsText.Text.Trim() + "', sku='"+skuText.Text.Trim()+"', size='"+sizeText.Text.Trim()+"', weight='"+weightText.Text+"', price='"+priceText.Text+"' where [sku]='" + sku + "'");
                            conn.addText("Client: " + localIPAddress + " has updated Product Details of SKU: " + sku + " "+skuText.Text.Trim()+"\r\n");

                            //update data about items contained in this sku
                            SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                            DataSet oldItems = conn1.getDataSet("use " + databaseName + " select * from sku_item where sku='" + sku + "'");

                            foreach (DataRow row in finalTable.Rows)
                            {
                                Boolean isNew = false;
                                foreach (String s in newItems)
                                {
                                    if (row[0].Equals(s))
                                        isNew = true;
                                }
                                if (isNew)
                                {
                                    //this item is new in the sku
                                    conn1.getDataSet("use " + databaseName + " insert into sku_item (sku, item, itemQuantity) values ('" + sku + "','" + row[0].ToString() + "','" + row[2].ToString() + "');");
                                    conn1.addText("Client: " + localIPAddress + " has added new Item: " + row[0].ToString() + " to SKU: " + sku + " "+skuText.Text+"\r\n");
                                }
                                else
                                {
                                    //this item is not new in the sku
                                    conn1.getDataSet("use " + databaseName + " update sku_item set itemQuantity='" + row[2].ToString() + "' where sku='" + sku + "' and item='" + row[0] + "'");
                                    conn1.addText("Client: " + localIPAddress + " has modified Item: " + row[0].ToString() + " in SKU: " + sku + " "+skuText.Text.Trim()+"\r\n");
                                }
                            }

                            //delete items if needed
                            foreach (String s in deleteItems)
                            {
                                if (!newItems.Contains(s))
                                {
                                    conn1.getDataSet("use " + databaseName + " delete from sku_item where sku='" + sku + "' and item='" + s + "';");
                                    conn1.addText("Client: " + localIPAddress + " has deleted Item: " + s + " in SKU: " + sku + " "+skuText.Text.Trim()+"\r\n");
                                }
                            }
                            MessageBox.Show("You have successfully saved data into database.");
                            this.Close();
                        }
                        else
                        {
                            //create a new sku
                            //insert new line into current sku table
                            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                            conn.getDataSet("use " + databaseName + " insert into sku(skuNum, productDetails, sku, size, weight, price) values ('" + skuGroupBox.Text + "','" + productDetailsText.Text.Trim() + "','" + skuText.Text.Trim() + "','" + sizeText.Text.Trim() + "','" + weightText.Text.Trim() + "','" + priceText.Text.Trim() + "');");
                            conn.addText("Client: " + localIPAddress + " has created a new SKU: " + skuGroupBox.Text + " "+skuText.Text.Trim()+"\r\n");

                            foreach (DataRow row in finalTable.Rows)
                                conn.getDataSet("use " + databaseName + " insert into sku_item (sku, item, itemQuantity) values ('" + sku + "','" + row[0].ToString() + "','" + row[2].ToString() + "');");
                            MessageBox.Show("You have successfully saved data into database.");
                            this.Close();

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                
            }
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            hasClickSaveBtn = false;
            addItem = new AddItem(this);
            addItem.ShowDialog();
            
        }

        private void productDetailsText_TextChanged(object sender, EventArgs e)
        {
            String description = productDetailsText.Text;
            if (description.Length > 998)
            {
                productDetailsText.Text = description.Remove(description.Length - 1);
                MessageBox.Show("The length cannot be more than 1000 letters.");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!hasClickSaveBtn)
            {
                if (MessageBox.Show(this, "You have not clicked 'Save' button after the changes. Do you want to close?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            else this.Close();
        }

        private void itemDataGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            currentValue = itemDataGridView.CurrentCell.Value.ToString();
        }

        private void itemDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (Convert.ToDecimal(itemDataGridView.CurrentCell.Value.ToString()) <= 0)
                {
                    MessageBox.Show("Quantity should be number more than 0.");
                    itemDataGridView.CurrentCell.Value = currentValue;
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Quantity should be number more than 0.");
                itemDataGridView.CurrentCell.Value = currentValue;
            }
        }

        private void itemDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            
            if (itemDataGridView.CurrentCell.ColumnIndex != 2 || itemDataGridView.CurrentCell.RowIndex == (itemDataGridView.Rows.Count - 1))
                itemDataGridView.ReadOnly = true;
            else itemDataGridView.ReadOnly = false;
        }

        private void skuText_TextChanged(object sender, EventArgs e)
        {
            String sku = skuText.Text;
            if (sku.Length > 48)
            {
                skuText.Text = sku.Remove(sku.Length - 1);
                //MessageBox.Show("The length cannot be more than 50 letters.");
            }
        }

        private void sizeText_TextChanged(object sender, EventArgs e)
        {
            String size = sizeText.Text;
            if (size.Length > 20)
            {
                sizeText.Text = size.Remove(size.Length - 1);
                MessageBox.Show("The length cannot be more than 20 letters.");
            }
        }

        private void weightText_TextChanged(object sender, EventArgs e)
        {
            String weight = weightText.Text;
            
            for (int i = 0; i < weight.Length; i++)
            {
                char c = weight[i];
                if (c != '.')
                {
                    if (!('0' <= c && c <= '9'))
                        weightText.Text = weight.Remove(i, 1);
                }
            }
            if (weight.Length > 10)
            {
                weightText.Text = weight.Remove(weight.Length - 1);
                MessageBox.Show("The length cannot be more than 10 letters.");
            }
            
        }

        private void priceText_TextChanged(object sender, EventArgs e)
        {
            String price = priceText.Text;
            for (int i = 0; i < price.Length; i++)
            {
                char c = price[i];
                if (c != '.')
                {
                    if (!('0' <= c && c <= '9'))
                        priceText.Text = price.Remove(i, 1);
                }
            }
            if (price.Length > 10)
            {
                priceText.Text = price.Remove(price.Length - 1);
                MessageBox.Show("The length cannot be more than 10 letters.");
            }
        }

        
    }
}
        

    
     
       

   

     
