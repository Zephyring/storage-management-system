﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SKUTest
{
    public partial class AddSlipItem : Form
    {
        
        private PackingSlip packingSlip;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
        private int itemQty = 0;
        private DataSet addItemDs = new DataSet();
        private Boolean hasClickSaveBtn = false;
        private List<String[]> tempEditItemByItem = new List<String[]>();
  

        public AddSlipItem(PackingSlip packingSlip)
        {
            InitializeComponent();
            this.packingSlip = packingSlip;
            changeBtn.Enabled = false;
            initAddItemDataGridView();
            itemBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet ds = conn.getDataSet("use " + databaseName + " select item from item where [item]<> \' Item Number\'");
            DataTable dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                itemBox.Items.Add(row[0].ToString().Trim());
            }
        }

        private void initAddItemDataGridView()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            DataColumn c0 = new DataColumn("Item");
            DataColumn c1 = new DataColumn("Description");
            DataColumn c2 = new DataColumn("New Added Quantity");
            DataColumn c3 = new DataColumn("Rest Quantity In Database");
            DataColumn c4 = new DataColumn("Rest Quantity In Slip Order");
            DataTable dt = new DataTable();
            dt.Columns.Add(c0);
            dt.Columns.Add(c1);
            dt.Columns.Add(c2);
            dt.Columns.Add(c3);
            dt.Columns.Add(c4);
            addItemDs.Tables.Add(dt);
            addItemDataGridView.DataSource = addItemDs.Tables[0];
            addItemDataGridView.ReadOnly = true;
            addItemDataGridView.Columns[1].Width = 200;

            //create temp editItemByItem list for the sake of cancel button
            foreach (String[] s in packingSlip.editItemByItem)
            {
                tempEditItemByItem.Add(s);
            }
            //pop list
            //init addItemDs if there are items in packingSlip.editItemByItem
            packingSlip.filterList();
            foreach (String[] s in packingSlip.editItemByItem)
            {
                DataRow r = addItemDs.Tables[0].NewRow();
                r[0] = s[0];//item#
                r[1] = conn.getDataSet("use "+databaseName+" select description from item where item='"+s[0]+"';").Tables[0].Rows[0][0].ToString();
                r[2] = s[1];//new edited quantity

                //rest quantity in database
                Decimal qtyInEditItemByItem = Convert.ToDecimal(s[1]);
                Decimal qtyInEditItemBySku = 0;
                foreach (String[] ss in packingSlip.editItemBySku)
                {
                    if (ss[0].Equals(s[0]))
                        qtyInEditItemBySku += Convert.ToDecimal(ss[1]);
                }
                r[3] = -qtyInEditItemByItem - qtyInEditItemBySku + Convert.ToDecimal(conn.getDataSet("use " + databaseName + " select quantity from item where item='" + s[0] + "';").Tables[0].Rows[0][0]);
                
                //rest quantity in slip order
                foreach (DataRow rr in packingSlip.itemDs.Tables[0].Rows)
                {
                    if (rr[0].ToString().Equals(s[0]))
                        r[4] = Convert.ToDecimal(rr[2]);
                }
                addItemDs.Tables[0].Rows.Add(r);
            }
            packingSlip.editItemByItem.Clear();

        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Your change will be applied to this slip order, but do not saved into database until you save this slip order. Do you want to continue?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                hasClickSaveBtn = true;
                //add items in addItemDs to slipOrder
                foreach (DataRow itemRow in addItemDs.Tables[0].Rows)
                {
                    Boolean isNew = true;
                    DataTable dt = packingSlip.itemDs.Tables[0];
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row[0].ToString().Trim().Equals(itemRow[0].ToString()))
                        {
                            isNew = false;
                            break;
                        }
                    }

                    if (isNew)//is new in slip order
                    {
                        SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

                        DataRow row = dt.NewRow();
                        row[0] = itemRow[0].ToString();//item#
                        row[1] = itemRow[1].ToString();//description
                        row[2] = itemRow[4].ToString();//rest qty in slip order
                        dt.Rows.Add(row);                     
                       

                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i][0].ToString().Trim().Equals(itemRow[0].ToString().Trim()))
                            {
                                dt.Rows[i][2] = itemRow[4].ToString();
                            }
                        }
                    }

                }
              
                packingSlip.calculateWeight();
                this.Close();
            }
            
        }

        private void itemBox_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private Boolean breakSkuIntegrity(Decimal restInSlipOrder)
        {
            foreach (DataRow row in packingSlip.itemDs.Tables[0].Rows)
            {     
                if (row[0].ToString().Trim().Equals(itemBox.SelectedItem.ToString().Trim()))
                {
                    if (restInSlipOrder  < itemQty)
                        return true;
                    else return false;
                }
                
            }
            return false;
        }

        private void itemBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeBtn.Enabled = true;
            okBtn.Enabled = true;
            calculateNecessaryQtyConsistingSku();
        }
        private void calculateNecessaryQtyConsistingSku()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            itemQty = 0;
            foreach (DataRow row in packingSlip.skuDs.Tables[0].Rows)
            {
                DataSet ds = conn.getDataSet("use " + databaseName + " select itemQuantity from sku_item where sku='" + row[0].ToString() + "' and item='" + itemBox.SelectedItem.ToString() + "';");
                if (ds.Tables[0].Rows.Count > 0)
                    itemQty += Convert.ToInt32(ds.Tables[0].Rows[0][0]) * Convert.ToInt32(row[2]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!hasClickSaveBtn)
            {
                if (MessageBox.Show(this, "You have not clicked 'OK' button after the changes. Do you want to close?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            else this.Close();
        }

        private void changeBtn_Click(object sender, EventArgs e)
        {
            hasClickSaveBtn = false;
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
           
            //get qtyInDatabase
            Decimal qtyInDatabase = 0;
            qtyInDatabase = Convert.ToDecimal(conn.getDataSet("use " + databaseName + " select quantity from item where item='" + itemBox.SelectedItem.ToString().Trim() + "';").Tables[0].Rows[0][0]);
            //get qtyInEditItemBySku
            Decimal qtyInEditItemBySku = 0;
            foreach (String[] s in packingSlip.editItemBySku)
            {
                if (s[0].Equals(itemBox.SelectedItem.ToString()))
                    qtyInEditItemBySku += Convert.ToDecimal(s[1]);
            }
            //get qtyInEditItemByItem
            Decimal qtyInEditItemByItem = 0;
            foreach (DataRow r in addItemDs.Tables[0].Rows)
            {
                if (r[0].ToString().Equals(itemBox.SelectedItem.ToString()))
                    qtyInEditItemByItem += Convert.ToDecimal(r[2]);
            }
            //restQtyIndatabase
            Decimal restQtyInDatabase = -qtyInEditItemBySku - qtyInEditItemByItem + qtyInDatabase;
            //restQtyInSlipOrder
            Decimal restQtyInSlipOrder=0;
            foreach (DataRow r in packingSlip.itemDs.Tables[0].Rows)
            {
                if (r[0].ToString().Equals(itemBox.SelectedItem.ToString()))
                    restQtyInSlipOrder += Convert.ToDecimal(r[2]);
            }

            //check isNew
            Boolean isNew = true;
            foreach (DataRow r in addItemDs.Tables[0].Rows)
            {
                if (r[0].ToString().Equals(itemBox.SelectedItem.ToString()))
                    isNew = false;
            }

            if (isNew)
            {
                DataRow r = addItemDs.Tables[0].NewRow();

                r[0] = itemBox.SelectedItem.ToString();//item#
                r[1] = conn.getDataSet("use " + databaseName + " select description from item where item='" + itemBox.SelectedItem.ToString().Trim() + "';").Tables[0].Rows[0][0].ToString();
                r[2] = quantityNumber.Value.ToString();//new added quantity
                r[3] = restQtyInDatabase - quantityNumber.Value;//rest qty in database
                r[4] = restQtyInSlipOrder + quantityNumber.Value;//rest qty in slip order
                if (Convert.ToDecimal(r[3]) < 0)
                    MessageBox.Show("Error! This makes Item#: " + r[0].ToString() + " quantity in database less than 0.");
                else if (Convert.ToDecimal(r[4]) < 0)
                    MessageBox.Show("Error! This makes Item#: " + r[0].ToString() + " quantity in Slip Order less than 0.");
                else
                {
                    addItemDs.Tables[0].Rows.Add(r);
                    if (breakSkuIntegrity(Convert.ToDecimal(r[4])))
                        MessageBox.Show("Warning! This operation may cause Item#: " + itemBox.SelectedItem.ToString() + " necessary to consisting those SKUs less than the required.");
                }
 

                
            }
            else
            {
                //locate the datarow and modify the qtys
                foreach (DataRow r in addItemDs.Tables[0].Rows)
                {
                    if (r[0].ToString().Equals(itemBox.SelectedItem.ToString()))
                    {
                        
                        r[2] = Convert.ToDecimal(r[2]) + quantityNumber.Value;//new added quantity
                        r[3] = Convert.ToDecimal(r[3]) - quantityNumber.Value;//rest qty in database
                        r[4] = Convert.ToDecimal(r[4]) + quantityNumber.Value;//rest qty in slip order
                        Boolean overflow=false;
                        if (Convert.ToDecimal(r[3]) < 0)
                        {
                            overflow = true;
                            MessageBox.Show("Error! This makes Item#: " + r[0].ToString() + " quantity in database less than 0.");
                        }
                        else if (Convert.ToDecimal(r[4]) < 0)
                        {
                            overflow = true;
                            MessageBox.Show("Error! This makes Item#: " + r[0].ToString() + " quantity in Slip Order less than 0.");
                        }
                        if (overflow)
                        {
                            r[2] = Convert.ToDecimal(r[2]) - quantityNumber.Value;//new added quantity
                            r[3] = Convert.ToDecimal(r[3]) + quantityNumber.Value;//rest qty in database
                            r[4] = Convert.ToDecimal(r[4]) - quantityNumber.Value;//rest qty in slip order
                        }

                        if (breakSkuIntegrity(Convert.ToDecimal(r[4])))
                            MessageBox.Show("Warning! This operation may cause Item#: " + itemBox.SelectedItem.ToString() + " necessary to consisting those SKUs less than the required.");
                                                
                    }
                }
            }
           
        }

        private void pushList()
        {
            //store the edited items for further use
            foreach (DataRow itemRow in addItemDs.Tables[0].Rows)
            {
                String[] newItem = new String[2];
                newItem[0] = itemRow[0].ToString();
                newItem[1] = itemRow[2].ToString();
                packingSlip.editItemByItem.Add(newItem);


                /*
                //store the new items for further use
                Boolean isNewToExtraItem = true;
                foreach (String[] s in packingSlip.newItem)
                {
                    if (s[0].Trim().Equals(newItem[0].Trim()))
                    {
                        isNewToExtraItem = false;
                        break;
                    }
                }

                if (isNewToExtraItem)
                    packingSlip.newItem.Add(newItem);
                else
                {
                    for (int i = 0; i < packingSlip.newItem.Count; i++)
                    {
                        if (newItem[0].ToString().Trim().Equals(packingSlip.newItem[i][0].Trim()))
                            packingSlip.newItem[i][1] = (Convert.ToInt32(packingSlip.newItem[i][1]) + Convert.ToInt32(newItem[1])).ToString();
                        //if (Convert.ToInt32(packingSlip.newItem[i][1]) <= 0)
                        //    packingSlip.newItem.Remove(packingSlip.newItem[i]);
                    }
                }*/
            }
        }

        private void pushPreviousList()
        {
            foreach (String[] s in tempEditItemByItem)
            {
                packingSlip.editItemByItem.Add(s);
            }
        }

        private void AddSlipItem_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (hasClickSaveBtn)
                pushList();
            else
                pushPreviousList();
        }

        private void addItemDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           itemBox.SelectedItem = addItemDataGridView[0,addItemDataGridView.CurrentCell.RowIndex].Value.ToString();
        }

       
       
    }
}
