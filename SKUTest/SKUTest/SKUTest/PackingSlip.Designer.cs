﻿namespace SKUTest
{
    partial class PackingSlip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.slipOrderLabel = new System.Windows.Forms.Label();
            this.informationGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.shipDatePicker = new System.Windows.Forms.DateTimePicker();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.TextBox();
            this.zipcodeNumber = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.stateText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cityText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.address2Text = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.address1Text = new System.Windows.Forms.TextBox();
            this.checkBtn = new System.Windows.Forms.Button();
            this.shipMethodGroupBox = new System.Windows.Forms.GroupBox();
            this.uspsCombo = new System.Windows.Forms.ComboBox();
            this.upsCombo = new System.Windows.Forms.ComboBox();
            this.radioUsps = new System.Windows.Forms.RadioButton();
            this.radioUps = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.hSizeText = new System.Windows.Forms.TextBox();
            this.wSizeText = new System.Windows.Forms.TextBox();
            this.lSizeText = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.processorText = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.skuDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.itemDataGridView = new System.Windows.Forms.DataGridView();
            this.saveBtn = new System.Windows.Forms.Button();
            this.addSkuBtn = new System.Windows.Forms.Button();
            this.packingGroupBox = new System.Windows.Forms.GroupBox();
            this.packingCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.actualWeightText = new System.Windows.Forms.TextBox();
            this.totalWeightText = new System.Windows.Forms.TextBox();
            this.shippingCheckBox = new System.Windows.Forms.CheckBox();
            this.addItemBtn = new System.Windows.Forms.Button();
            this.payWayGroupBox = new System.Windows.Forms.GroupBox();
            this.payCombo = new System.Windows.Forms.ComboBox();
            this.orderLevelGroupBox = new System.Windows.Forms.GroupBox();
            this.expCombo = new System.Windows.Forms.ComboBox();
            this.radioExp = new System.Windows.Forms.RadioButton();
            this.radioRgl = new System.Windows.Forms.RadioButton();
            this.checkExtraItemBtn = new System.Windows.Forms.Button();
            this.closeBtn = new System.Windows.Forms.Button();
            this.shippingGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.bornTimeTextBox = new System.Windows.Forms.TextBox();
            this.panel = new System.Windows.Forms.Panel();
            this.reportBtn = new System.Windows.Forms.Button();
            this.informationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zipcodeNumber)).BeginInit();
            this.shipMethodGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.skuDataGridView)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemDataGridView)).BeginInit();
            this.packingGroupBox.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.payWayGroupBox.SuspendLayout();
            this.orderLevelGroupBox.SuspendLayout();
            this.shippingGroupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // slipOrderLabel
            // 
            this.slipOrderLabel.AutoSize = true;
            this.slipOrderLabel.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.slipOrderLabel.Location = new System.Drawing.Point(359, 9);
            this.slipOrderLabel.Name = "slipOrderLabel";
            this.slipOrderLabel.Size = new System.Drawing.Size(109, 20);
            this.slipOrderLabel.TabIndex = 0;
            this.slipOrderLabel.Text = "Slip Label";
            // 
            // informationGroupBox
            // 
            this.informationGroupBox.Controls.Add(this.label1);
            this.informationGroupBox.Controls.Add(this.shipDatePicker);
            this.informationGroupBox.Controls.Add(this.nameLabel);
            this.informationGroupBox.Controls.Add(this.nameText);
            this.informationGroupBox.Controls.Add(this.zipcodeNumber);
            this.informationGroupBox.Controls.Add(this.label6);
            this.informationGroupBox.Controls.Add(this.stateText);
            this.informationGroupBox.Controls.Add(this.label5);
            this.informationGroupBox.Controls.Add(this.cityText);
            this.informationGroupBox.Controls.Add(this.label4);
            this.informationGroupBox.Controls.Add(this.address2Text);
            this.informationGroupBox.Controls.Add(this.label3);
            this.informationGroupBox.Controls.Add(this.label2);
            this.informationGroupBox.Controls.Add(this.address1Text);
            this.informationGroupBox.Location = new System.Drawing.Point(0, 2);
            this.informationGroupBox.Name = "informationGroupBox";
            this.informationGroupBox.Size = new System.Drawing.Size(303, 214);
            this.informationGroupBox.TabIndex = 1;
            this.informationGroupBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 21;
            this.label1.Text = "ShipDate:";
            // 
            // shipDatePicker
            // 
            this.shipDatePicker.Location = new System.Drawing.Point(77, 185);
            this.shipDatePicker.Name = "shipDatePicker";
            this.shipDatePicker.Size = new System.Drawing.Size(209, 21);
            this.shipDatePicker.TabIndex = 6;
            this.shipDatePicker.TabStop = false;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(30, 23);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(35, 12);
            this.nameLabel.TabIndex = 14;
            this.nameLabel.Text = "Name:";
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(77, 20);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(209, 21);
            this.nameText.TabIndex = 1;
            this.nameText.TextChanged += new System.EventHandler(this.nameText_TextChanged);
            // 
            // zipcodeNumber
            // 
            this.zipcodeNumber.Location = new System.Drawing.Point(77, 155);
            this.zipcodeNumber.Maximum = new decimal(new int[] {
            99950,
            0,
            0,
            0});
            this.zipcodeNumber.Name = "zipcodeNumber";
            this.zipcodeNumber.Size = new System.Drawing.Size(99, 21);
            this.zipcodeNumber.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "ZipCode:";
            // 
            // stateText
            // 
            this.stateText.Location = new System.Drawing.Point(77, 128);
            this.stateText.Name = "stateText";
            this.stateText.Size = new System.Drawing.Size(209, 21);
            this.stateText.TabIndex = 6;
            this.stateText.TextChanged += new System.EventHandler(this.stateText_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "State:";
            // 
            // cityText
            // 
            this.cityText.Location = new System.Drawing.Point(77, 101);
            this.cityText.Name = "cityText";
            this.cityText.Size = new System.Drawing.Size(209, 21);
            this.cityText.TabIndex = 5;
            this.cityText.TextChanged += new System.EventHandler(this.cityText_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "City:";
            // 
            // address2Text
            // 
            this.address2Text.Location = new System.Drawing.Point(77, 74);
            this.address2Text.Name = "address2Text";
            this.address2Text.Size = new System.Drawing.Size(209, 21);
            this.address2Text.TabIndex = 3;
            this.address2Text.TextChanged += new System.EventHandler(this.address2Text_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "Address2:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Address1:";
            // 
            // address1Text
            // 
            this.address1Text.Location = new System.Drawing.Point(77, 47);
            this.address1Text.Name = "address1Text";
            this.address1Text.Size = new System.Drawing.Size(209, 21);
            this.address1Text.TabIndex = 2;
            this.address1Text.TextChanged += new System.EventHandler(this.address1Text_TextChanged);
            // 
            // checkBtn
            // 
            this.checkBtn.Location = new System.Drawing.Point(447, 612);
            this.checkBtn.Name = "checkBtn";
            this.checkBtn.Size = new System.Drawing.Size(90, 34);
            this.checkBtn.TabIndex = 11;
            this.checkBtn.TabStop = false;
            this.checkBtn.Text = "Check Validity";
            this.checkBtn.UseVisualStyleBackColor = true;
            this.checkBtn.Click += new System.EventHandler(this.checkBtn_Click);
            // 
            // shipMethodGroupBox
            // 
            this.shipMethodGroupBox.Controls.Add(this.uspsCombo);
            this.shipMethodGroupBox.Controls.Add(this.upsCombo);
            this.shipMethodGroupBox.Controls.Add(this.radioUsps);
            this.shipMethodGroupBox.Controls.Add(this.radioUps);
            this.shipMethodGroupBox.Enabled = false;
            this.shipMethodGroupBox.Location = new System.Drawing.Point(6, 91);
            this.shipMethodGroupBox.Name = "shipMethodGroupBox";
            this.shipMethodGroupBox.Size = new System.Drawing.Size(200, 78);
            this.shipMethodGroupBox.TabIndex = 1;
            this.shipMethodGroupBox.TabStop = false;
            this.shipMethodGroupBox.Text = "Ship Method";
            // 
            // uspsCombo
            // 
            this.uspsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uspsCombo.Enabled = false;
            this.uspsCombo.FormattingEnabled = true;
            this.uspsCombo.Items.AddRange(new object[] {
            "Parcel",
            "Priority",
            "Express"});
            this.uspsCombo.Location = new System.Drawing.Point(87, 53);
            this.uspsCombo.Name = "uspsCombo";
            this.uspsCombo.Size = new System.Drawing.Size(100, 20);
            this.uspsCombo.TabIndex = 14;
            this.uspsCombo.TabStop = false;
            this.uspsCombo.SelectedIndexChanged += new System.EventHandler(this.uspsCombo_SelectedIndexChanged);
            // 
            // upsCombo
            // 
            this.upsCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.upsCombo.Enabled = false;
            this.upsCombo.FormattingEnabled = true;
            this.upsCombo.Items.AddRange(new object[] {
            "Ground",
            "3-Day",
            "2-Day",
            "Next-Day"});
            this.upsCombo.Location = new System.Drawing.Point(87, 21);
            this.upsCombo.Name = "upsCombo";
            this.upsCombo.Size = new System.Drawing.Size(100, 20);
            this.upsCombo.TabIndex = 12;
            this.upsCombo.TabStop = false;
            this.upsCombo.SelectedIndexChanged += new System.EventHandler(this.upsCombo_SelectedIndexChanged);
            // 
            // radioUsps
            // 
            this.radioUsps.AutoSize = true;
            this.radioUsps.Location = new System.Drawing.Point(8, 54);
            this.radioUsps.Name = "radioUsps";
            this.radioUsps.Size = new System.Drawing.Size(47, 16);
            this.radioUsps.TabIndex = 0;
            this.radioUsps.Text = "USPS";
            this.radioUsps.UseVisualStyleBackColor = true;
            this.radioUsps.CheckedChanged += new System.EventHandler(this.radioUsps_CheckedChanged);
            // 
            // radioUps
            // 
            this.radioUps.AutoSize = true;
            this.radioUps.Location = new System.Drawing.Point(8, 25);
            this.radioUps.Name = "radioUps";
            this.radioUps.Size = new System.Drawing.Size(41, 16);
            this.radioUps.TabIndex = 20;
            this.radioUps.Text = "UPS";
            this.radioUps.UseVisualStyleBackColor = true;
            this.radioUps.CheckedChanged += new System.EventHandler(this.radioUps_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.hSizeText);
            this.groupBox3.Controls.Add(this.wSizeText);
            this.groupBox3.Controls.Add(this.lSizeText);
            this.groupBox3.Location = new System.Drawing.Point(6, 46);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(219, 41);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Package Size";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(145, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "H";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(79, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 12);
            this.label9.TabIndex = 13;
            this.label9.Text = "W";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "L";
            // 
            // hSizeText
            // 
            this.hSizeText.Location = new System.Drawing.Point(156, 13);
            this.hSizeText.Name = "hSizeText";
            this.hSizeText.ReadOnly = true;
            this.hSizeText.Size = new System.Drawing.Size(51, 21);
            this.hSizeText.TabIndex = 11;
            this.hSizeText.TextChanged += new System.EventHandler(this.hSizeText_TextChanged);
            // 
            // wSizeText
            // 
            this.wSizeText.Location = new System.Drawing.Point(90, 13);
            this.wSizeText.Name = "wSizeText";
            this.wSizeText.ReadOnly = true;
            this.wSizeText.Size = new System.Drawing.Size(51, 21);
            this.wSizeText.TabIndex = 10;
            this.wSizeText.TextChanged += new System.EventHandler(this.wSizeText_TextChanged);
            // 
            // lSizeText
            // 
            this.lSizeText.Location = new System.Drawing.Point(24, 13);
            this.lSizeText.Name = "lSizeText";
            this.lSizeText.ReadOnly = true;
            this.lSizeText.Size = new System.Drawing.Size(51, 21);
            this.lSizeText.TabIndex = 9;
            this.lSizeText.TabStop = false;
            this.lSizeText.TextChanged += new System.EventHandler(this.sizeText_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.processorText);
            this.groupBox5.Location = new System.Drawing.Point(6, 8);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(131, 44);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Processor";
            // 
            // processorText
            // 
            this.processorText.Location = new System.Drawing.Point(5, 17);
            this.processorText.Name = "processorText";
            this.processorText.ReadOnly = true;
            this.processorText.Size = new System.Drawing.Size(120, 21);
            this.processorText.TabIndex = 20;
            this.processorText.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.skuDataGridView);
            this.groupBox7.Location = new System.Drawing.Point(9, 301);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(762, 129);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "SKUs List";
            // 
            // skuDataGridView
            // 
            this.skuDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.skuDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skuDataGridView.Location = new System.Drawing.Point(3, 17);
            this.skuDataGridView.Name = "skuDataGridView";
            this.skuDataGridView.RowTemplate.Height = 23;
            this.skuDataGridView.Size = new System.Drawing.Size(756, 109);
            this.skuDataGridView.TabIndex = 0;
            this.skuDataGridView.TabStop = false;
            this.skuDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.skuDataGridView_CellEndEdit);
            this.skuDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.skuDataGridView_CellEnter);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.itemDataGridView);
            this.groupBox8.Location = new System.Drawing.Point(9, 430);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(762, 174);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Total Items List";
            // 
            // itemDataGridView
            // 
            this.itemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemDataGridView.Location = new System.Drawing.Point(3, 17);
            this.itemDataGridView.Name = "itemDataGridView";
            this.itemDataGridView.RowTemplate.Height = 23;
            this.itemDataGridView.Size = new System.Drawing.Size(756, 154);
            this.itemDataGridView.TabIndex = 0;
            this.itemDataGridView.TabStop = false;
            this.itemDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemDataGridView_CellEndEdit);
            this.itemDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemDataGridView_CellEnter);
            // 
            // saveBtn
            // 
            this.saveBtn.Enabled = false;
            this.saveBtn.Location = new System.Drawing.Point(560, 612);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(90, 34);
            this.saveBtn.TabIndex = 12;
            this.saveBtn.TabStop = false;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // addSkuBtn
            // 
            this.addSkuBtn.Location = new System.Drawing.Point(0, 232);
            this.addSkuBtn.Name = "addSkuBtn";
            this.addSkuBtn.Size = new System.Drawing.Size(90, 32);
            this.addSkuBtn.TabIndex = 7;
            this.addSkuBtn.TabStop = false;
            this.addSkuBtn.Text = "Modify SKUs";
            this.addSkuBtn.UseVisualStyleBackColor = true;
            this.addSkuBtn.Click += new System.EventHandler(this.addSkuBtn_Click);
            // 
            // packingGroupBox
            // 
            this.packingGroupBox.Controls.Add(this.packingCheckBox);
            this.packingGroupBox.Controls.Add(this.groupBox9);
            this.packingGroupBox.Controls.Add(this.groupBox3);
            this.packingGroupBox.Location = new System.Drawing.Point(312, 2);
            this.packingGroupBox.Name = "packingGroupBox";
            this.packingGroupBox.Size = new System.Drawing.Size(231, 155);
            this.packingGroupBox.TabIndex = 16;
            this.packingGroupBox.TabStop = false;
            // 
            // packingCheckBox
            // 
            this.packingCheckBox.AutoSize = true;
            this.packingCheckBox.Location = new System.Drawing.Point(11, 23);
            this.packingCheckBox.Name = "packingCheckBox";
            this.packingCheckBox.Size = new System.Drawing.Size(66, 16);
            this.packingCheckBox.TabIndex = 7;
            this.packingCheckBox.TabStop = false;
            this.packingCheckBox.Text = "Packing";
            this.packingCheckBox.UseVisualStyleBackColor = true;
            this.packingCheckBox.CheckedChanged += new System.EventHandler(this.packingCheckBox_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label8);
            this.groupBox9.Controls.Add(this.actualWeightText);
            this.groupBox9.Controls.Add(this.totalWeightText);
            this.groupBox9.Location = new System.Drawing.Point(6, 101);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(219, 41);
            this.groupBox9.TabIndex = 18;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Total Weight / Actual Weight(LBS)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(98, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 16);
            this.label8.TabIndex = 6;
            this.label8.Text = "/";
            // 
            // actualWeightText
            // 
            this.actualWeightText.Location = new System.Drawing.Point(115, 14);
            this.actualWeightText.Name = "actualWeightText";
            this.actualWeightText.ReadOnly = true;
            this.actualWeightText.Size = new System.Drawing.Size(98, 21);
            this.actualWeightText.TabIndex = 19;
            this.actualWeightText.TabStop = false;
            this.actualWeightText.TextChanged += new System.EventHandler(this.actualWeightText_TextChanged);
            // 
            // totalWeightText
            // 
            this.totalWeightText.Location = new System.Drawing.Point(8, 14);
            this.totalWeightText.Name = "totalWeightText";
            this.totalWeightText.ReadOnly = true;
            this.totalWeightText.Size = new System.Drawing.Size(90, 21);
            this.totalWeightText.TabIndex = 18;
            this.totalWeightText.TabStop = false;
            // 
            // shippingCheckBox
            // 
            this.shippingCheckBox.AutoSize = true;
            this.shippingCheckBox.Enabled = false;
            this.shippingCheckBox.Location = new System.Drawing.Point(14, 22);
            this.shippingCheckBox.Name = "shippingCheckBox";
            this.shippingCheckBox.Size = new System.Drawing.Size(72, 16);
            this.shippingCheckBox.TabIndex = 8;
            this.shippingCheckBox.TabStop = false;
            this.shippingCheckBox.Text = "Shipping";
            this.shippingCheckBox.UseVisualStyleBackColor = true;
            this.shippingCheckBox.CheckedChanged += new System.EventHandler(this.shippingCheckBox_CheckedChanged);
            // 
            // addItemBtn
            // 
            this.addItemBtn.Location = new System.Drawing.Point(109, 232);
            this.addItemBtn.Name = "addItemBtn";
            this.addItemBtn.Size = new System.Drawing.Size(90, 32);
            this.addItemBtn.TabIndex = 8;
            this.addItemBtn.TabStop = false;
            this.addItemBtn.Text = "Modify Items";
            this.addItemBtn.UseVisualStyleBackColor = true;
            this.addItemBtn.Click += new System.EventHandler(this.addItemBtn_Click);
            // 
            // payWayGroupBox
            // 
            this.payWayGroupBox.Controls.Add(this.payCombo);
            this.payWayGroupBox.Enabled = false;
            this.payWayGroupBox.Location = new System.Drawing.Point(6, 44);
            this.payWayGroupBox.Name = "payWayGroupBox";
            this.payWayGroupBox.Size = new System.Drawing.Size(200, 43);
            this.payWayGroupBox.TabIndex = 19;
            this.payWayGroupBox.TabStop = false;
            this.payWayGroupBox.Text = "Payment Way";
            // 
            // payCombo
            // 
            this.payCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.payCombo.FormattingEnabled = true;
            this.payCombo.Items.AddRange(new object[] {
            "Online",
            "Credit Card",
            "Pay Check",
            "RMA"});
            this.payCombo.Location = new System.Drawing.Point(9, 14);
            this.payCombo.Name = "payCombo";
            this.payCombo.Size = new System.Drawing.Size(178, 20);
            this.payCombo.TabIndex = 10;
            this.payCombo.TabStop = false;
            // 
            // orderLevelGroupBox
            // 
            this.orderLevelGroupBox.Controls.Add(this.expCombo);
            this.orderLevelGroupBox.Controls.Add(this.radioExp);
            this.orderLevelGroupBox.Controls.Add(this.radioRgl);
            this.orderLevelGroupBox.Enabled = false;
            this.orderLevelGroupBox.Location = new System.Drawing.Point(6, 173);
            this.orderLevelGroupBox.Name = "orderLevelGroupBox";
            this.orderLevelGroupBox.Size = new System.Drawing.Size(200, 82);
            this.orderLevelGroupBox.TabIndex = 20;
            this.orderLevelGroupBox.TabStop = false;
            this.orderLevelGroupBox.Text = "Order Level";
            // 
            // expCombo
            // 
            this.expCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.expCombo.Enabled = false;
            this.expCombo.FormattingEnabled = true;
            this.expCombo.Location = new System.Drawing.Point(87, 53);
            this.expCombo.Name = "expCombo";
            this.expCombo.Size = new System.Drawing.Size(100, 20);
            this.expCombo.TabIndex = 17;
            this.expCombo.TabStop = false;
            // 
            // radioExp
            // 
            this.radioExp.AutoSize = true;
            this.radioExp.Location = new System.Drawing.Point(9, 57);
            this.radioExp.Name = "radioExp";
            this.radioExp.Size = new System.Drawing.Size(77, 16);
            this.radioExp.TabIndex = 20;
            this.radioExp.Text = "Expedited";
            this.radioExp.UseVisualStyleBackColor = true;
            this.radioExp.CheckedChanged += new System.EventHandler(this.radioExp_CheckedChanged);
            // 
            // radioRgl
            // 
            this.radioRgl.AutoSize = true;
            this.radioRgl.Location = new System.Drawing.Point(9, 25);
            this.radioRgl.Name = "radioRgl";
            this.radioRgl.Size = new System.Drawing.Size(65, 16);
            this.radioRgl.TabIndex = 15;
            this.radioRgl.Text = "Regular";
            this.radioRgl.UseVisualStyleBackColor = true;
            // 
            // checkExtraItemBtn
            // 
            this.checkExtraItemBtn.Location = new System.Drawing.Point(232, 269);
            this.checkExtraItemBtn.Name = "checkExtraItemBtn";
            this.checkExtraItemBtn.Size = new System.Drawing.Size(81, 32);
            this.checkExtraItemBtn.TabIndex = 9;
            this.checkExtraItemBtn.TabStop = false;
            this.checkExtraItemBtn.Text = " Check   Extra Items";
            this.checkExtraItemBtn.UseVisualStyleBackColor = true;
            this.checkExtraItemBtn.Click += new System.EventHandler(this.checkExtraItemBtn_Click);
            // 
            // closeBtn
            // 
            this.closeBtn.Location = new System.Drawing.Point(674, 612);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(90, 34);
            this.closeBtn.TabIndex = 13;
            this.closeBtn.TabStop = false;
            this.closeBtn.Text = "Cancel";
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // shippingGroupBox
            // 
            this.shippingGroupBox.Controls.Add(this.shipMethodGroupBox);
            this.shippingGroupBox.Controls.Add(this.shippingCheckBox);
            this.shippingGroupBox.Controls.Add(this.orderLevelGroupBox);
            this.shippingGroupBox.Controls.Add(this.payWayGroupBox);
            this.shippingGroupBox.Location = new System.Drawing.Point(549, 2);
            this.shippingGroupBox.Name = "shippingGroupBox";
            this.shippingGroupBox.Size = new System.Drawing.Size(213, 261);
            this.shippingGroupBox.TabIndex = 21;
            this.shippingGroupBox.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Location = new System.Drawing.Point(309, 157);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(231, 106);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.bornTimeTextBox);
            this.groupBox6.Location = new System.Drawing.Point(6, 56);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(219, 44);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Slip Order Born Time";
            // 
            // bornTimeTextBox
            // 
            this.bornTimeTextBox.Location = new System.Drawing.Point(6, 14);
            this.bornTimeTextBox.Name = "bornTimeTextBox";
            this.bornTimeTextBox.ReadOnly = true;
            this.bornTimeTextBox.Size = new System.Drawing.Size(207, 21);
            this.bornTimeTextBox.TabIndex = 0;
            // 
            // panel
            // 
            this.panel.Controls.Add(this.groupBox4);
            this.panel.Controls.Add(this.shippingGroupBox);
            this.panel.Controls.Add(this.addItemBtn);
            this.panel.Controls.Add(this.informationGroupBox);
            this.panel.Controls.Add(this.addSkuBtn);
            this.panel.Controls.Add(this.packingGroupBox);
            this.panel.Location = new System.Drawing.Point(9, 37);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(772, 263);
            this.panel.TabIndex = 23;
            // 
            // reportBtn
            // 
            this.reportBtn.Location = new System.Drawing.Point(12, 612);
            this.reportBtn.Name = "reportBtn";
            this.reportBtn.Size = new System.Drawing.Size(87, 34);
            this.reportBtn.TabIndex = 24;
            this.reportBtn.Text = "Report";
            this.reportBtn.UseVisualStyleBackColor = true;
            this.reportBtn.Click += new System.EventHandler(this.reportBtn_Click);
            // 
            // PackingSlip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 668);
            this.Controls.Add(this.reportBtn);
            this.Controls.Add(this.closeBtn);
            this.Controls.Add(this.checkExtraItemBtn);
            this.Controls.Add(this.slipOrderLabel);
            this.Controls.Add(this.checkBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "PackingSlip";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Packing Slip";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PackingSlip_FormClosing);
            this.informationGroupBox.ResumeLayout(false);
            this.informationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zipcodeNumber)).EndInit();
            this.shipMethodGroupBox.ResumeLayout(false);
            this.shipMethodGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.skuDataGridView)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemDataGridView)).EndInit();
            this.packingGroupBox.ResumeLayout(false);
            this.packingGroupBox.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.payWayGroupBox.ResumeLayout(false);
            this.orderLevelGroupBox.ResumeLayout(false);
            this.orderLevelGroupBox.PerformLayout();
            this.shippingGroupBox.ResumeLayout(false);
            this.shippingGroupBox.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label slipOrderLabel;
        private System.Windows.Forms.GroupBox informationGroupBox;
        private System.Windows.Forms.TextBox address1Text;
        private System.Windows.Forms.GroupBox shipMethodGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox lSizeText;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox processorText;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView skuDataGridView;
        private System.Windows.Forms.DataGridView itemDataGridView;
        private System.Windows.Forms.TextBox cityText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox address2Text;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox stateText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button checkBtn;
        private System.Windows.Forms.NumericUpDown zipcodeNumber;
        private System.Windows.Forms.Button addSkuBtn;
        private System.Windows.Forms.GroupBox packingGroupBox;
        private System.Windows.Forms.DateTimePicker shipDatePicker;
        private System.Windows.Forms.Button addItemBtn;
        public System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.CheckBox shippingCheckBox;
        private System.Windows.Forms.CheckBox packingCheckBox;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox totalWeightText;
        private System.Windows.Forms.GroupBox payWayGroupBox;
        private System.Windows.Forms.GroupBox orderLevelGroupBox;
        private System.Windows.Forms.TextBox actualWeightText;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Button checkExtraItemBtn;
        public System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton radioUsps;
        private System.Windows.Forms.RadioButton radioUps;
        private System.Windows.Forms.ComboBox upsCombo;
        private System.Windows.Forms.ComboBox payCombo;
        private System.Windows.Forms.RadioButton radioExp;
        private System.Windows.Forms.RadioButton radioRgl;
        private System.Windows.Forms.ComboBox uspsCombo;
        private System.Windows.Forms.ComboBox expCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox shippingGroupBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox bornTimeTextBox;
        private System.Windows.Forms.TextBox hSizeText;
        private System.Windows.Forms.TextBox wSizeText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button reportBtn;
    }
}