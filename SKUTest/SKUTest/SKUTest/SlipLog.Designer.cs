﻿namespace SKUTest
{
    partial class SlipLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.logText = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.slipOrderBox = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.processorBox = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.beginTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.endTimePicker = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.logText);
            this.groupBox1.Location = new System.Drawing.Point(11, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(692, 319);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log";
            // 
            // logText
            // 
            this.logText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logText.Location = new System.Drawing.Point(3, 17);
            this.logText.Name = "logText";
            this.logText.ReadOnly = true;
            this.logText.Size = new System.Drawing.Size(686, 299);
            this.logText.TabIndex = 0;
            this.logText.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.slipOrderBox);
            this.groupBox2.Location = new System.Drawing.Point(14, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(142, 50);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Slip Order";
            // 
            // slipOrderBox
            // 
            this.slipOrderBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.slipOrderBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.slipOrderBox.FormattingEnabled = true;
            this.slipOrderBox.Location = new System.Drawing.Point(6, 20);
            this.slipOrderBox.Name = "slipOrderBox";
            this.slipOrderBox.Size = new System.Drawing.Size(130, 20);
            this.slipOrderBox.TabIndex = 0;
            this.slipOrderBox.SelectedIndexChanged += new System.EventHandler(this.slipOrderBox_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.processorBox);
            this.groupBox3.Location = new System.Drawing.Point(558, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(142, 50);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Processor";
            // 
            // processorBox
            // 
            this.processorBox.FormattingEnabled = true;
            this.processorBox.Location = new System.Drawing.Point(6, 20);
            this.processorBox.Name = "processorBox";
            this.processorBox.Size = new System.Drawing.Size(130, 20);
            this.processorBox.TabIndex = 0;
            this.processorBox.SelectedIndexChanged += new System.EventHandler(this.processorBox_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.beginTimePicker);
            this.groupBox4.Location = new System.Drawing.Point(214, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(126, 50);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Begin Time";
            // 
            // beginTimePicker
            // 
            this.beginTimePicker.Location = new System.Drawing.Point(6, 20);
            this.beginTimePicker.Name = "beginTimePicker";
            this.beginTimePicker.Size = new System.Drawing.Size(114, 21);
            this.beginTimePicker.TabIndex = 4;
            this.beginTimePicker.CloseUp += new System.EventHandler(this.beginTimePicker_CloseUp);
            this.beginTimePicker.Enter += new System.EventHandler(this.beginTimePicker_Enter);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.endTimePicker);
            this.groupBox5.Location = new System.Drawing.Point(346, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(126, 50);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "End Time";
            // 
            // endTimePicker
            // 
            this.endTimePicker.Location = new System.Drawing.Point(6, 20);
            this.endTimePicker.Name = "endTimePicker";
            this.endTimePicker.Size = new System.Drawing.Size(114, 21);
            this.endTimePicker.TabIndex = 4;
            this.endTimePicker.CloseUp += new System.EventHandler(this.endTimePicker_CloseUp);
            this.endTimePicker.Enter += new System.EventHandler(this.endTimePicker_Enter);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(328, 397);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SlipLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 432);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "SlipLog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Slip Manipulation Log";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox logText;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox slipOrderBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox processorBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker beginTimePicker;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DateTimePicker endTimePicker;
        private System.Windows.Forms.Button button1;
    }
}