﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace SKUTest
{
     static class Config
    {
        public static String databaseName = "skuDatabase";
        public static String IPAddress = "localhost";
        public static String port = "6666";
        public static String localIPAddress = Dns.GetHostAddresses(Dns.GetHostName()).ElementAt(1).ToString();

        public static void setDatabaseName(String _databaseName)
        {
            databaseName = _databaseName;
        }

        public static void setIPAddress(String _IPAddress)
        {
            IPAddress = _IPAddress;
        }

        public static void setPort(String _port)
        {
            port = _port;
        }
    }
}
