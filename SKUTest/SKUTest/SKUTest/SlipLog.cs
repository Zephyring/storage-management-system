﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SKUTest
{
    public partial class SlipLog : Form
    {
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
        
        private DateTime oldBeginTime;
        private DateTime oldEndTime;
        public SlipLog()
        {
            InitializeComponent();
            beginTimePicker.Value = endTimePicker.Value.AddDays(-1);
            oldBeginTime = beginTimePicker.Value;
            oldEndTime = endTimePicker.Value;
            slipOrderBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet ds = conn.getDataSet("use " + databaseName + " select slipOrder from slipOrder order by slipOrder DESC");

            DataTable dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                slipOrderBox.Items.Add(row[0].ToString());
            }

            processorBox.Items.Clear();
            SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet accountDs = conn1.getDataSet("use " + databaseName + " select account from account");

            foreach (DataRow dr in accountDs.Tables[0].Rows)
            {
                processorBox.Items.Add(dr[0].ToString());
            }
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void processorBox_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void slipOrderBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            logText.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet logDs = conn.getDataSet("use " + databaseName + " select * from slipOrderLog where slipOrder='" + slipOrderBox.SelectedItem.ToString() + "' order by date Desc;");

            foreach (DataRow dr in logDs.Tables[0].Rows)
            {
                String slipOrder = dr[0].ToString();
                String processor = dr[1].ToString();
                String date = dr[2].ToString();
                String manipulationLog = dr[3].ToString();
                logText.Text += "Slip Order: " + slipOrder + "\r\n";
                logText.Text += "Processor: " + processor + "\r\n";
                logText.Text += "Date: " + date + "\r\n";
                logText.Text += "Manipulation Log: " + manipulationLog + "\r\n\r\n";
            }

        }

        private void processorBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            logText.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet logDs = conn.getDataSet("use " + databaseName + " select * from slipOrderLog where processor='" + processorBox.SelectedItem.ToString() + "' order by date Desc;");

            foreach (DataRow dr in logDs.Tables[0].Rows)
            {
                String slipOrder = dr[0].ToString();
                String processor = dr[1].ToString();
                String date = dr[2].ToString();
                String manipulationLog = dr[3].ToString();
                logText.Text += "Slip Order: " + slipOrder + "\r\n";
                logText.Text += "Processor: " + processor + "\r\n";
                logText.Text += "Date: " + date + "\r\n";
                logText.Text += "Manipulation Log: " + manipulationLog + "\r\n\r\n";
            }
        }

        private void beginTimePicker_Enter(object sender, EventArgs e)
        {
            oldBeginTime = beginTimePicker.Value;
        }

        private void endTimePicker_Enter(object sender, EventArgs e)
        {
            oldEndTime = endTimePicker.Value;
        }

        private void endTimePicker_CloseUp(object sender, EventArgs e)
        {
            if (DateTime.Compare(beginTimePicker.Value, endTimePicker.Value) > 0)
            {
                endTimePicker.Value = oldEndTime;
                //MessageBox.Show("Begin Time should not greater than End Time");
            }
            else if (DateTime.Compare(beginTimePicker.Value, endTimePicker.Value) == 0)
            {
                endTimePicker.Value = beginTimePicker.Value.AddDays(1);
            }
            logText.Clear();
            DateTime beginTime = beginTimePicker.Value;
            DateTime endTime = endTimePicker.Value;
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet logDs = conn.getDataSet("use " + databaseName + " select * from slipOrderLog order by date Desc");

            foreach (DataRow dr in logDs.Tables[0].Rows)
            {
                DateTime orderTime = Convert.ToDateTime(dr[2].ToString());
                if (DateTime.Compare(beginTime, orderTime) <= 0 && DateTime.Compare(orderTime, endTime) <= 0)
                {
                    String slipOrder = dr[0].ToString();
                    String processor = dr[1].ToString();
                    String date = dr[2].ToString();
                    String manipulationLog = dr[3].ToString();
                    logText.Text += "Slip Order: " + slipOrder + "\r\n";
                    logText.Text += "Processor: " + processor + "\r\n";
                    logText.Text += "Date: " + date + "\r\n";
                    logText.Text += "Manipulation Log: " + manipulationLog + "\r\n\r\n";
                }

            }     
        }

        private void beginTimePicker_CloseUp(object sender, EventArgs e)
        {
            if (DateTime.Compare(beginTimePicker.Value, endTimePicker.Value) > 0)
            {
                beginTimePicker.Value = oldBeginTime;
                //MessageBox.Show("Begin Time should not greater than End Time");
            }
            else if (DateTime.Compare(beginTimePicker.Value, endTimePicker.Value) == 0)
            {
                beginTimePicker.Value = endTimePicker.Value.AddDays(-1);
            }
            logText.Clear();
            DateTime beginTime = beginTimePicker.Value;
            DateTime endTime = endTimePicker.Value;
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet logDs = conn.getDataSet("use " + databaseName + " select * from slipOrderLog order by date Desc");

            foreach (DataRow dr in logDs.Tables[0].Rows)
            {
                DateTime orderTime = Convert.ToDateTime(dr[2].ToString());
                if (DateTime.Compare(beginTime, orderTime) <= 0 && DateTime.Compare(orderTime, endTime) <= 0)
                {
                    String slipOrder = dr[0].ToString();
                    String processor = dr[1].ToString();
                    String date = dr[2].ToString();
                    String manipulationLog = dr[3].ToString();
                    logText.Text += "Slip Order: " + slipOrder + "\r\n";
                    logText.Text += "Processor: " + processor + "\r\n";
                    logText.Text += "Date: " + date + "\r\n";
                    logText.Text += "Manipulation Log: " + manipulationLog + "\r\n\r\n";
                }                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
