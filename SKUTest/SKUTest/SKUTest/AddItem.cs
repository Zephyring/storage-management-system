﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SKUTest
{
    public partial class AddItem : Form
    {
        private SKU sku=null;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String localIPAddress = Config.localIPAddress;
        private String port=Config.port;
      

        public AddItem(SKU sku)
        {
            InitializeComponent();
            this.sku = sku;
            itemBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet ds = conn.getDataSet("use " + databaseName + " select item from item;");

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                itemBox.Items.Add(row[0].ToString());
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            addItemToSku();
        }

        private void addItemToSku()
        {
            try
            {
                //get Item details from database
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                DataSet ds = conn.getDataSet("use " + databaseName + " select description from item where item='" + itemBox.SelectedItem.ToString() + "'");

                Boolean exist = false;
                foreach (DataRow row in sku.finalTable.Rows)
                {
                    if (row[0].Equals(itemBox.SelectedItem.ToString()))
                        exist = true;
                }

                if (exist==false)
                {
                    //if not already exist in item dataGridView, add this new item
                    DataRow temp = sku.finalTable.NewRow();
                    temp[0] = itemBox.SelectedItem.ToString();//item#
                    temp[1] = ds.Tables[0].Rows[0][0].ToString().Trim();//description
                    temp[2] = quantityNumber.Value.ToString();//quantity
                    if (quantityNumber.Value <= 0)
                        MessageBox.Show("Quantity should be greater than 0");
                    else
                    {
                        sku.finalTable.Rows.Add(temp);
                        sku.newItems.Add(temp[0].ToString());
                        MessageBox.Show("New Item#: "+temp[0].ToString()+" has been added.");
                    }
                }
                else
                {
                    //if exist in item dataGridView, add the quantity of this item
                    foreach (DataRow row in sku.finalTable.Rows)
                    {
                        if (row[0].Equals(itemBox.SelectedItem.ToString()))
                        {
                            if ((Convert.ToInt32(row[2].ToString()) + quantityNumber.Value) < 0)
                                MessageBox.Show("Quantity should be greater than 0");
                            else row[2] = (Convert.ToInt32(row[2].ToString()) + quantityNumber.Value).ToString();
                            if (Convert.ToInt32(row[2].ToString()) == 0)
                            {
                                MessageBox.Show("Item#: " + row[0].ToString() + " has been removed from this SKU.");
                                sku.deleteItems.Add(row[0].ToString());//remove item based on item#
                                sku.finalTable.Rows.Remove(row);
                            }
                        }
                    }
                } 
            }
            catch (Exception ex) { ex.ToString(); }
        }



        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void itemBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            okBtn.Enabled = true;
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            descriptionTextBox.Text = conn.getDataSet("use " + databaseName + " select description from item where item='" + itemBox.SelectedItem.ToString() + "';").Tables[0].Rows[0][0].ToString();
        }

                   

    }
}
