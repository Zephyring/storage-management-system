﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Net;

namespace SKUTest
{
    public partial class Login : Form
    {
        private String account;
        private String password;
        private String role;
        private DataSet accountDataSet;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
        private static MainFrame mainFrame = MainFrame.getInstance();

        private Boolean active = true;
        
        public Login()
        {
            InitializeComponent();
            
            BinaryClientFormatterSinkProvider provider = new BinaryClientFormatterSinkProvider();
            
            IDictionary props = new Hashtable();
            props["port"] = port;
            props["name"] = "login";

            TcpClientChannel channel = new TcpClientChannel(props,provider);
            
            ChannelServices.RegisterChannel(channel,false);
            
         }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            if (checkValidity())
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                conn.addText("New Connection from Client: " + localIPAddress + " " + DateTime.Now + "\r\n");
                this.Visible = false;
                mainFrame.display.Text = "Online";
                mainFrame.accountManagement.Enabled = true;
                mainFrame.dataMaintenance.Enabled = true;
                mainFrame.packingSlip.Enabled = true;
                mainFrame.role = roleCombo.SelectedItem.ToString();
                if (roleCombo.SelectedItem.Equals("Administrator"))
                {
                    mainFrame.skuBtn.Enabled = true;
                    mainFrame.itemBtn.Enabled = true;
                    mainFrame.logBtn.Enabled = true;
                    mainFrame.reportBtn.Enabled = true;  
                    mainFrame.checkSlipBtn.Enabled = true;
                    mainFrame.bookSlipBtn.Enabled = true;
                    mainFrame.loginBtn.Enabled = false;
                    mainFrame.logoutBtn.Enabled = true;
                }
                else if(roleCombo.SelectedItem.Equals("Orderer"))
                {
                    mainFrame.dataMaintenance.Enabled = false;
                    mainFrame.accountManagement.Enabled = false;
                    mainFrame.checkSlipBtn.Enabled = false;
                    mainFrame.bookSlipBtn.Enabled = true;
                    mainFrame.loginBtn.Enabled = false;
                    mainFrame.logoutBtn.Enabled = true;
                }
                else if (roleCombo.SelectedItem.Equals("Packer") || roleCombo.SelectedItem.Equals("Shipper"))
                {
                    mainFrame.dataMaintenance.Enabled = false;
                    mainFrame.accountManagement.Enabled = false;
                    mainFrame.checkSlipBtn.Enabled = true;
                    mainFrame.bookSlipBtn.Enabled = false;
                    mainFrame.loginBtn.Enabled = false;
                    mainFrame.logoutBtn.Enabled = true;
                }
                else if (roleCombo.SelectedItem.Equals("Reporter"))
                {
                    mainFrame.accountManagement.Enabled = false;
                    mainFrame.skuBtn.Enabled = false;
                    mainFrame.itemBtn.Enabled = false;
                    mainFrame.logBtn.Enabled = false;
                    mainFrame.reportBtn.Enabled = true;
                    mainFrame.bookSlipBtn.Enabled = false;
                    mainFrame.checkSlipBtn.Enabled = true;
                    mainFrame.loginBtn.Enabled = false;
                    mainFrame.logoutBtn.Enabled = true;
                }
            }
            else
            {
                if (active)
                    MessageBox.Show("Wrong password!");
                else
                {
                    MessageBox.Show("This account has been deactivated!");
                    active = true;
                }
            }
            
                
        }

        private Boolean checkValidity()
        {

            Boolean valid = false;
            account = accountText.Text; 
            password = passwordText.Text;
            role = roleCombo.SelectedItem.ToString();
            mainFrame.account = account;
            Config.setIPAddress(ipTextBox.Text.Trim());
            this.IPAddress = Config.IPAddress;
            mainFrame.IPAddress = Config.IPAddress;

            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            Config.setDatabaseName(conn.getDatabaseName());
            this.databaseName = Config.databaseName;
            mainFrame.databaseName = Config.databaseName;
            
            
            accountDataSet=conn.getDataSet("use " + databaseName + " select account, password, status from account where role='"+role+"';");
            DataTable dt=null;
            if(accountDataSet!=null)
                dt = accountDataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].Equals(account) && dt.Rows[i][1].Equals(password))
                {
                    if (dt.Rows[i][2].ToString().Trim().Equals("activating"))
                    {
                        valid = true;
                        active = true;
                    }
                    else
                    {
                        active = false;
                    }
                }
            }
            if (account == "")
                valid = false;
            return valid;
        }
       

        private void closeBtn_Click(object sender, EventArgs e)
        {
            ChannelServices.UnregisterChannel(ChannelServices.GetChannel("login"));
            this.Close();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            roleCombo.SelectedIndex = 0;
        }

        

 

           
    }
}
