﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace SKUTest
{
    public partial class EditedSkuOrItem : Form
    {
        private PackingSlip packingSlip = null;
        private DataSet itemDs = new DataSet();
        private DataSet skuDs = new DataSet();
        private DataSet overflowItemDs = new DataSet();
        private String IPAddress = Config.IPAddress;
        private String port = Config.port;
        private String databaseName = Config.databaseName;
        private System.Windows.Forms.Timer t;

        public EditedSkuOrItem(PackingSlip packingSlip)
        {
            this.packingSlip = packingSlip;
            InitializeComponent();
            initDataOfEditedItems();
            initDataOfEditedSkus();
            initDataOfOverflowItems();
            checkOverflow();
            timer();
            
            
        }
        private void timer()
        {
            t = new Timer();
            t.Interval = 1;
            t.Start();
            t.Tick+=new EventHandler(timerCheck);
        }

        private void timerCheck(object sender,EventArgs e)
        {
            t.Stop();
            if (overflowItemDs.Tables[0].Rows.Count != 0)
            {
                packingSlip.saveBtn.Enabled = false;
                MessageBox.Show("Warning! There exists some overflow items. Please modify quantity of them.");
                
            }
           
        }
      

        private void initDataOfEditedSkus()
        {
            DataTable skuDt = new DataTable();
            DataColumn skuDc0 = new DataColumn("SKU#");
            DataColumn skuDc1 = new DataColumn("Product Details");
            DataColumn skuDc2 = new DataColumn("New Edited Quantity");
            skuDt.Columns.Add(skuDc0);
            skuDt.Columns.Add(skuDc1);
            skuDt.Columns.Add(skuDc2);

            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            for (int i = 0; i < packingSlip.skuList.Count; i++)
            {
                for (int j = i + 1; j < packingSlip.skuList.Count; j++)
                {
                    if (packingSlip.skuList[i][0].Equals(packingSlip.skuList[j][0]))
                    {
                        packingSlip.skuList[i][1] = (Convert.ToInt32(packingSlip.skuList[i][1]) + Convert.ToInt32(packingSlip.skuList[j][1])).ToString();
                        packingSlip.skuList.Remove(packingSlip.skuList[j]);
                        j--;
                    }
                }
            }

            foreach (String[] s in packingSlip.skuList)
            {
                DataRow row = skuDt.NewRow();
                row[0] = s[0];
                row[1] = conn.getDataSet("use " + databaseName + " select productDetails from sku where sku='" + s[0] + "';").Tables[0].Rows[0][0];
                row[2] = s[1];
                skuDt.Rows.Add(row);
            }

            skuDs.Tables.Add(skuDt);
            editedSkuDataGridView.DataSource = skuDt;
            editedSkuDataGridView.Columns[0].Width = 100;
            editedSkuDataGridView.Columns[1].Width = 460;
            editedSkuDataGridView.Columns[2].Width = 100;
            editedSkuDataGridView.ReadOnly = true;
        }


        private void initDataOfEditedItems()
        {
            DataTable itemDt = new DataTable();
            DataColumn itemDc0 = new DataColumn("Item#");
            DataColumn itemDc1 = new DataColumn("Description");
            DataColumn itemDc2 = new DataColumn("New Edited Quantity");
            DataColumn itemDc3 = new DataColumn("Rest Quantity");
            itemDt.Columns.Add(itemDc0);
            itemDt.Columns.Add(itemDc1);
            itemDt.Columns.Add(itemDc2);
            itemDt.Columns.Add(itemDc3);

            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            for (int i = 0; i < packingSlip.editItem.Count; i++)
            {
                for (int j = i + 1; j < packingSlip.editItem.Count; j++)
                {
                    if (packingSlip.editItem[i][0].Equals(packingSlip.editItem[j][0]))
                    {
                        packingSlip.editItem[i][1] = (Convert.ToInt32(packingSlip.editItem[i][1]) + Convert.ToInt32(packingSlip.editItem[j][1])).ToString();
                        packingSlip.editItem.Remove(packingSlip.editItem[j]);
                        j--;
                    }
                }
            }

            foreach (String[] s in packingSlip.editItem)
            {
                DataRow row = itemDt.NewRow();
                row[0] = s[0];
                row[1] = conn.getDataSet("use " + databaseName + " select description from item where item='" + s[0] + "';").Tables[0].Rows[0][0];
                row[2] = s[1];
                row[3] = conn.getDataSet("use " + databaseName + " select quantity from item where item='" + s[0] + "';").Tables[0].Rows[0][0];

                itemDt.Rows.Add(row);
                
            }

            itemDs.Tables.Add(itemDt);
            editedItemDataGridView.DataSource = itemDt;
            editedItemDataGridView.Columns[0].Width = 100;
            editedItemDataGridView.Columns[1].Width = 360;
            editedItemDataGridView.Columns[2].Width = 100;
            editedItemDataGridView.Columns[3].Width = 100;
            editedItemDataGridView.ReadOnly = true;
        }

        private void initDataOfOverflowItems()
        {
            DataTable itemDt = new DataTable();
            DataColumn itemDc0 = new DataColumn("Item#");
            DataColumn itemDc1 = new DataColumn("Description");
            DataColumn itemDc2 = new DataColumn("Overflow Quantity");
            
            itemDt.Columns.Add(itemDc0);
            itemDt.Columns.Add(itemDc1);
            itemDt.Columns.Add(itemDc2);

            overflowItemDs.Tables.Add(itemDt);

            overflowItemDataGridView.DataSource = itemDt;
            overflowItemDataGridView.Columns[0].Width = 100;
            overflowItemDataGridView.Columns[1].Width = 450;
            overflowItemDataGridView.Columns[2].Width = 100;
            overflowItemDataGridView.ReadOnly = true;
        }
        private void closeBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void checkOverflow()
        {
            foreach (DataRow r in itemDs.Tables[0].Rows)
            {
                if (Convert.ToInt32(r[2]) > Convert.ToInt32(r[3]))
                {
                    DataRow rr = overflowItemDs.Tables[0].NewRow();
                    rr[0] = r[0];
                    rr[1] = r[1];
                    rr[2] = Convert.ToInt32(r[2]) - Convert.ToInt32(r[3]);
                   
                    overflowItemDs.Tables[0].Rows.Add(rr);
                }
            }
        }
    }
}
