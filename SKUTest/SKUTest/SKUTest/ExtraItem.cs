﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SKUTest
{
    public partial class ExtraItem : Form
    {
        private PackingSlip packingSlip = null;
        private DataSet itemDs = new DataSet();
        private String IPAddress = Config.IPAddress;
        private String port = Config.port;
        private String databaseName = Config.databaseName;
      
        public ExtraItem(PackingSlip packingSlip)
        {
            this.packingSlip = packingSlip;
            InitializeComponent();
            initDataOfExtraItems();
        }

        private void initDataOfExtraItems()
        {
            DataTable itemDt = new DataTable();
            DataColumn itemDc0 = new DataColumn("Item#");
            DataColumn itemDc1 = new DataColumn("Description");
            DataColumn itemDc2 = new DataColumn("Quantity");
            itemDt.Columns.Add(itemDc0);
            itemDt.Columns.Add(itemDc1);
            itemDt.Columns.Add(itemDc2);

            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
           
            foreach (String[] s in packingSlip.newItem)
            {
                DataRow row = itemDt.NewRow();
                row[0] = s[0];
                row[1] = conn.getDataSet("use " + databaseName + " select description from item where item='" + s[0] + "';").Tables[0].Rows[0][0];
                row[2] = s[1];
                itemDt.Rows.Add(row);
            }

            foreach (String[] s in packingSlip.editItemByItem)
            {
                Boolean isNew = true;
                foreach (DataRow r in itemDt.Rows)
                {
                    if (r[0].ToString().Equals(s[0]))
                    {
                        isNew = false;
                        break;
                    }
                }

                if (!isNew)
                {
                    foreach (DataRow rr in itemDt.Rows)
                    {
                        if (rr[0].ToString().Equals(s[0]))
                            rr[2] = Convert.ToDecimal(rr[2]) + Convert.ToDecimal(s[1]);
                    }
                    
                }
                else//new item
                {
                    DataRow dr = itemDt.NewRow();
                    dr[0] = s[0];//item
                    dr[1] = conn.getDataSet("use " + databaseName + " select description from item where item='" + s[0] + "';").Tables[0].Rows[0][0];
                    dr[2] = s[1];//quantity
                    itemDt.Rows.Add(dr);
                }
                
                              
            }

            itemDs.Tables.Add(itemDt);
            itemDataGridView.DataSource = itemDt;
            itemDataGridView.Columns[0].Width = 100;
            itemDataGridView.Columns[1].Width = 460;
            itemDataGridView.Columns[2].Width = 100;
            itemDataGridView.ReadOnly = true;
        }

        

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
