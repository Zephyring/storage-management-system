﻿namespace SKUTest
{
    partial class SKU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.skuGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.itemDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.priceText = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.weightText = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.sizeText = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.skuText = new System.Windows.Forms.TextBox();
            this.productDetailsText = new System.Windows.Forms.RichTextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.addBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.skuGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // skuGroupBox
            // 
            this.skuGroupBox.Controls.Add(this.groupBox3);
            this.skuGroupBox.Controls.Add(this.groupBox2);
            this.skuGroupBox.Location = new System.Drawing.Point(12, 12);
            this.skuGroupBox.Name = "skuGroupBox";
            this.skuGroupBox.Size = new System.Drawing.Size(561, 444);
            this.skuGroupBox.TabIndex = 0;
            this.skuGroupBox.TabStop = false;
            this.skuGroupBox.Text = "Number";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.itemDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(6, 159);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(546, 280);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Items";
            // 
            // itemDataGridView
            // 
            this.itemDataGridView.AllowUserToResizeColumns = false;
            this.itemDataGridView.AllowUserToResizeRows = false;
            this.itemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemDataGridView.Location = new System.Drawing.Point(3, 17);
            this.itemDataGridView.Name = "itemDataGridView";
            this.itemDataGridView.RowTemplate.Height = 23;
            this.itemDataGridView.Size = new System.Drawing.Size(540, 260);
            this.itemDataGridView.TabIndex = 0;
            this.itemDataGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.itemDataGridView_CellBeginEdit);
            this.itemDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemDataGridView_CellEndEdit);
            this.itemDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemDataGridView_CellEnter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox6);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.productDetailsText);
            this.groupBox2.Location = new System.Drawing.Point(6, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(552, 133);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Product Details";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.priceText);
            this.groupBox6.Location = new System.Drawing.Point(435, 20);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(108, 48);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Price($)";
            // 
            // priceText
            // 
            this.priceText.Location = new System.Drawing.Point(6, 20);
            this.priceText.Name = "priceText";
            this.priceText.Size = new System.Drawing.Size(84, 21);
            this.priceText.TabIndex = 1;
            this.priceText.TextChanged += new System.EventHandler(this.priceText_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.weightText);
            this.groupBox5.Location = new System.Drawing.Point(332, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(97, 48);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Weight(LBS)";
            // 
            // weightText
            // 
            this.weightText.Location = new System.Drawing.Point(6, 20);
            this.weightText.Name = "weightText";
            this.weightText.Size = new System.Drawing.Size(84, 21);
            this.weightText.TabIndex = 1;
            this.weightText.TextChanged += new System.EventHandler(this.weightText_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.sizeText);
            this.groupBox4.Location = new System.Drawing.Point(215, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(111, 48);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Size";
            // 
            // sizeText
            // 
            this.sizeText.Location = new System.Drawing.Point(6, 20);
            this.sizeText.Name = "sizeText";
            this.sizeText.Size = new System.Drawing.Size(99, 21);
            this.sizeText.TabIndex = 1;
            this.sizeText.TextChanged += new System.EventHandler(this.sizeText_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.skuText);
            this.groupBox1.Location = new System.Drawing.Point(6, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 48);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SKU#";
            // 
            // skuText
            // 
            this.skuText.Location = new System.Drawing.Point(6, 20);
            this.skuText.Name = "skuText";
            this.skuText.Size = new System.Drawing.Size(191, 21);
            this.skuText.TabIndex = 1;
            // 
            // productDetailsText
            // 
            this.productDetailsText.Location = new System.Drawing.Point(6, 74);
            this.productDetailsText.Name = "productDetailsText";
            this.productDetailsText.Size = new System.Drawing.Size(540, 53);
            this.productDetailsText.TabIndex = 0;
            this.productDetailsText.Text = "";
            this.productDetailsText.TextChanged += new System.EventHandler(this.productDetailsText_TextChanged);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(412, 462);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 1;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(21, 462);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 23);
            this.addBtn.TabIndex = 3;
            this.addBtn.Text = "Add Item";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(492, 462);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // SKU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 497);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.skuGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "SKU";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SKU";
            this.skuGroupBox.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox skuGroupBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox productDetailsText;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView itemDataGridView;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox priceText;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox weightText;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox sizeText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox skuText;
    }
}