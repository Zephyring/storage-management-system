﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text.RegularExpressions;

namespace SKUTest
{
    public partial class MainFrame : Form
    {
        private static MainFrame mainFrame;
        public String account;
        public String role;
        //private DataSet itemDataSet;
        private DataSet skuDataSet;
        private DataSet slipOrderDataSet;
        private DataSet accountDataSet;
        //private DataSet slipDs;
        //private DataSet skuDs;
        //private DataSet itemDs;
        //private DataSet newItemDs;
        private DataTable skuDt = new DataTable();
        private DataTable itemDt = new DataTable();
        private DataTable newItemDt = new DataTable();
        private DataTable orderDt = new DataTable();
        public String databaseName = Config.databaseName;
        public String IPAddress = Config.IPAddress;
        public String port = Config.port;
        public String localIPAddress = Config.localIPAddress;
        private String bufferText="";
        public int option= 0;
        private String startDate;
        private String endDate;
        private CurrencyManager cm;
        

        public MainFrame()
        {

            InitializeComponent();
            calendar.TodayDate = DateTime.Today;
            calendar.MaxSelectionCount = 31;
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this,"Do you want to logout?","Logout Message",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                loginBtn.Enabled = true;
                logoutBtn.Enabled = false;
                accountManagement.Enabled = false;
                dataMaintenance.Enabled = false;
                packingSlip.Enabled = false;
                accountGroupBox.Visible = false;
                skuGroupBox.Visible = false;
                slipGroupBox.Visible = false;
                calendarGroupBox.Visible = false;
                registerGroup.Visible = false;
                display.Text = "Offline";

                ChannelServices.UnregisterChannel(ChannelServices.GetChannel("login"));
            }

        }

        private void registerNewBtn_Click(object sender, EventArgs e)
        {
            registerGroup.Visible = !registerGroup.Visible;    
            if (registerGroup.Visible)
            {
                accountGroupBox.Visible = false;
                skuGroupBox.Visible = false;
                calendarGroupBox.Visible = false;
                slipGroupBox.Visible = false;
                display.Text = "User Register";
                rgsAcc.Text = "";
                rgsPwd.Text = "";
                confirmPwd.Text = "";
                rgsName.Text = "";
                rgsTel.Text = "";
                registerBtn.Enabled = false;
            }
            else
            {
                display.Text = "Online";
            }
        }

        private void modifyBtn_Click(object sender, EventArgs e)
        {
            accountGroupBox.Visible = !accountGroupBox.Visible;
            saveAccountBtn.Visible = false;
            revertAccButton.Visible = false;
            if (!accountBox.Visible)
            {
                detailsGroupBox.Visible = false;
                display.Text = "Online";
            }
            else
            {
                skuGroupBox.Visible = false;
                calendarGroupBox.Visible = false;
                slipGroupBox.Visible = false;
                registerGroup.Visible = false;
                display.Text = "User Management";
                accountBox.SelectedIndex = -1;
                statusCheckBox.Checked = false;
                telephoneText.Text = "";
                nameText.Text = "";
                passwordText.Text = "";
                detailsGroupBox.Visible = false;


            }
        }

        private void skuBtn_Click(object sender, EventArgs e)
        {
            skuGroupBox.Visible = !skuGroupBox.Visible;
            if (skuGroupBox.Visible)
            {
                accountGroupBox.Visible = false;
                calendarGroupBox.Visible = false;
                slipGroupBox.Visible = false;
                registerGroup.Visible = false;
                display.Text = "SKU Management";
                skuCheckBtn.Enabled = false;
                skuBox.Items.Clear();
                skuBox.SelectedIndex = -1;
                skuDcpText.Text = "";
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                skuDataSet = conn.getDataSet("use " + databaseName + " select sku, productDetails from sku");
                DataTable dt = skuDataSet.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                    skuBox.Items.Add(dt.Rows[i][0].ToString().Trim());
            }
            else
            {
                display.Text = "Online";
            }
        }

        private void itemBtn_Click(object sender, EventArgs e)
        {
            Item checkItem = new Item();
            checkItem.ShowDialog();
        }

        private void checkSlipBtn_Click(object sender, EventArgs e)
        {
            slipGroupBox.Visible = !slipGroupBox.Visible;
            if (slipGroupBox.Visible)
            {
                accountGroupBox.Visible = false;
                skuGroupBox.Visible = false;
                calendarGroupBox.Visible = false;
                registerGroup.Visible = false;
                display.Text = "Slip Order Management";
                radioSlipNum.Checked = true;
                slipText.Visible = true;
                shipTimePicker.Visible = false;
                option = 0;
                slipText.Text = "";
                liveCheckBox.Checked = true;
                historyCheckBox.Checked = false;
                slipOrderBox.SelectedIndex = -1;
                slipCheckBtn.Enabled = false;
                customerText.Text = "";
                addressText.Text = "";
                timeText.Text = "";
                initialSlipOrderGridView();
            }
            else
            {
                display.Text = "Online";
            }

        }

        private void initialSlipOrderGridView()
        {
            if (slipOrderGridView.DataSource == null)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                DataTable dt = conn.getDataSet("use "+databaseName+" select slipOrder, name, address, processor, shipDate, shipState from slipOrder order by slipOrder DESC").Tables[0];
                DataColumn dc0 = new DataColumn("Slip Order");
                DataColumn dc1 = new DataColumn("Customer");
                DataColumn dc2 = new DataColumn("Address");
                DataColumn dc3 = new DataColumn("Processor");
                DataColumn dc4 = new DataColumn("Ship Date");
                DataColumn dc5 = new DataColumn("Packed");
                DataColumn dc6 = new DataColumn("Shipped");
                orderDt.Columns.Add(dc0);
                orderDt.Columns.Add(dc1);
                orderDt.Columns.Add(dc2);
                orderDt.Columns.Add(dc3);
                orderDt.Columns.Add(dc4);
                orderDt.Columns.Add(dc5);
                orderDt.Columns.Add(dc6);
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow row = orderDt.NewRow();
                    row[0] = dr[0].ToString();
                    row[1] = dr[1].ToString();
                    char[] separator = { '@' };
                    String[] arr = dr[2].ToString().Split(separator);
                    row[2] = arr[0]+" "+arr[1]+" "+arr[2]+" "+arr[3];
                    row[3] = dr[3].ToString();
                    row[4] = dr[4].ToString();
                    if (dr[5].ToString().Equals("00"))
                    {
                        row[5] = "No";
                        row[6] = "No";
                    }
                    else if (dr[5].ToString().Equals("10"))
                    {
                        row[5] = "Yes";
                        row[6] = "No";
                    }
                    else if (dr[5].ToString().Equals("11"))
                    {
                        row[5] = "Yes";
                        row[6] = "Yes";
                    }
                    orderDt.Rows.Add(row);
                }
                slipOrderGridView.DataSource = orderDt;
                slipOrderGridView.ReadOnly = true;
                slipOrderGridView.Columns[2].Width = 200;
                slipOrderGridView.Columns[4].Width = 150;

                screenSlipOrder();
            }



        }

        private void slipCheckBtn_Click(object sender, EventArgs e)
        {
            if (slipOrderBox.SelectedIndex != -1)
            {
                PackingSlip checkingSlip = new PackingSlip(slipOrderBox.SelectedItem.ToString(), account,role);
                checkingSlip.ShowDialog();
            }
        }


        private void bookSlipBtn_Click(object sender, EventArgs e)
        {
            PackingSlip bookingSlip = new PackingSlip(account,role);
            bookingSlip.ShowDialog();
        }

        private void skuAddNewBtn_Click(object sender, EventArgs e)
        {
            SKU newSKU = new SKU();
            newSKU.ShowDialog();
            skuBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            skuDataSet = conn.getDataSet("use " + databaseName + " select sku, productDetails from sku");
            DataTable dt = skuDataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
                skuBox.Items.Add(dt.Rows[i][0].ToString().Trim());
        }

        private void skuCheckBtn_Click(object sender, EventArgs e)
        {
            if (skuBox.SelectedIndex != -1)
            {
                SKU checkSKU = new SKU(skuBox.SelectedItem.ToString());
                checkSKU.ShowDialog();
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                skuDataSet = conn.getDataSet("use " + databaseName + " select sku, productDetails from sku");
            }

        }
        /*
        private void itemCheckBtn_Click(object sender, EventArgs e)
        {
            if (itemBox.SelectedIndex != -1)
            {
                Item checkItem = new Item(itemBox.SelectedItem.ToString().Trim());
                checkItem.ShowDialog();
            }
            else
            {
                Item checkItem = new Item();
                checkItem.ShowDialog();
            }
        }
        */
        private void reportBtn_Click(object sender, EventArgs e)
        {
            calendarGroupBox.Visible = !calendarGroupBox.Visible;
            if (calendarGroupBox.Visible)
            {
                accountGroupBox.Visible = false;
                skuGroupBox.Visible = false;
                slipGroupBox.Visible = false;
                registerGroup.Visible = false;
                display.Text = "Report Viewing";
            }
            else
            {
                display.Text = "Online";
            }
        }

        private void logBtn_Click(object sender, EventArgs e)
        {
            SlipLog slipLog = new SlipLog();
            slipLog.ShowDialog();
            display.Text = "Online";
            accountGroupBox.Visible = false;
            skuGroupBox.Visible = false;
            slipGroupBox.Visible = false;
            calendarGroupBox.Visible = false;
            registerGroup.Visible = false;
        }


        /*
        private void itemBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (itemDataSet.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < itemDataSet.Tables[0].Rows.Count; i++)
                    if (itemBox.SelectedItem.ToString().Trim().Equals(itemDataSet.Tables[0].Rows[i][0].ToString().Trim()))
                    {
                        itemDescriptionText.Text = itemDataSet.Tables[0].Rows[i][1].ToString().Trim();
                        restQtyText.Text = itemDataSet.Tables[0].Rows[i][2].ToString().Trim();
                        weightText.Text = itemDataSet.Tables[0].Rows[i][3].ToString().Trim();
                    }
            }
        }
        */
        private void slipOrderBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (slipOrderBox.SelectedIndex != -1)
            {
                if (slipOrderBox.SelectedItem.ToString() == "(Null)")
                    slipOrderBox.SelectedIndex = -1;
                else
                {
                    slipCheckBtn.Enabled = true;
                    if (slipOrderDataSet != null)
                        for (int i = 0; i < slipOrderDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (slipOrderBox.SelectedItem.ToString().Trim().Equals(slipOrderDataSet.Tables[0].Rows[i][0].ToString().Trim()))
                            {
                                customerText.Text = slipOrderDataSet.Tables[0].Rows[i][1].ToString().Trim();
                                string[] sArray = slipOrderDataSet.Tables[0].Rows[i][2].ToString().Trim().Split('@');
                                String address = "";
                                int n = 0;
                                foreach (string s in sArray)
                                {
                                    n++;
                                    if (n != sArray.Length)
                                        address += s.ToString() + " ";
                                }
                                addressText.Text = address;
                                timeText.Text = Convert.ToDateTime(slipOrderDataSet.Tables[0].Rows[i][3]).ToShortDateString();
                            }
                        }
                }
            }
            else
            {
                slipCheckBtn.Enabled = false;
                customerText.Text = "";
                addressText.Text = "";
                timeText.Text = "";
            }
        }

        private void skuBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (skuBox.SelectedItem.ToString() != "")
                skuCheckBtn.Enabled = true;
            for (int i = 0; i < skuDataSet.Tables[0].Rows.Count; i++)
                if (skuBox.SelectedItem.ToString().Trim().Equals(skuDataSet.Tables[0].Rows[i][0].ToString().Trim()))
                    skuDcpText.Text = skuDataSet.Tables[0].Rows[i][1].ToString().Trim();
        }

        private void accountBox_MouseClick(object sender, MouseEventArgs e)
        {
            accountBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            accountDataSet = conn.getDataSet("use " + databaseName + " select * from account");

            DataTable dt = accountDataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                accountBox.Items.Add(dt.Rows[i][0].ToString().Trim());
            }

        }

        private void saveAccountBtn_Click(object sender, EventArgs e)
        {
            if (nameText.Text == "")
                MessageBox.Show("Null Name");
            else if (passwordText.Text == "")
                MessageBox.Show("Null Password");
            else if (telephoneText.Text == "")
                MessageBox.Show("Null Telephone");
            else if (currentAccountRoleBox.SelectedIndex == -1)
                MessageBox.Show("Null Role");
            else
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                String status;
                if (statusCheckBox.Checked)
                    status = "activating";
                else status = "deactivating";

                conn.getDataSet("use " + databaseName + " update account set password='" + passwordText.Text.Trim() + "',name='" + nameText.Text.Trim() + "',telephone='" + telephoneText.Text.Trim() + "',status='" + status + "',role='"+currentAccountRoleBox.SelectedItem.ToString()+"' where account='" + accountBox.SelectedItem.ToString() + "';");
                conn.addText("Client: " + localIPAddress + " has modified account: " + accountBox.SelectedItem.ToString() + "\r\n");

                MessageBox.Show("Saved");
            }

        }

        private void slipOrderBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (bufferText=="")
            {
                slipOrderBox.Items.Clear();
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                slipOrderDataSet = conn.getDataSet("use " + databaseName + " select slipOrder, name, address, shipDate, shipState from slipOrder order by slipOrder DESC");
                if (slipOrderDataSet != null)
                {
                    DataTable dt = slipOrderDataSet.Tables[0];
                    dt = slipFilter(dt);
                    if (dt.Rows.Count != 0)
                        for (int i = 0; i < dt.Rows.Count; i++)
                            slipOrderBox.Items.Add(dt.Rows[i][0].ToString());
                    else
                        slipOrderBox.Items.Add("(Null)");
                }
                else
                    slipOrderBox.Items.Add("(Null)");
            }
        }

        private DataTable slipFilter(DataTable dt)
        {
            DateTime t = Convert.ToDateTime((DateTime.Today.ToShortDateString()+" 0:00:00"));
            if(!historyCheckBox.Checked && !liveCheckBox.Checked)
            {
                dt.Clear();
                MessageBox.Show("Please CHECK at least one Range Option or you can't find any slip");
            }
            else if (!historyCheckBox.Checked)
            {
                for (int i = 0; i < dt.Rows.Count;i++ )
                {
                    if (Convert.ToDateTime(dt.Rows[i][3]) <= t)
                    {
                        dt.Rows.Remove(dt.Rows[i]);
                        i--;
                    }
                }
            }
            else if(!liveCheckBox.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToDateTime(dt.Rows[i][3]) >= t)
                    {
                        dt.Rows.Remove(dt.Rows[i]);
                        i--;
                    }
                }
            }

            if (packCheckBox.Checked)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][4].ToString() != "11")
                    {
                        dt.Rows.Remove(dt.Rows[i]);
                        i--;
                    }
                }
            }
            else if(shipCheckBox.Checked)
            {
                for(int i=0; i < dt.Rows.Count; i++)
                {
                    if(dt.Rows[i][4].ToString() != "10")
                    {
                        dt.Rows.Remove(dt.Rows[i]);
                        i--;
                    }
                }
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][4].ToString() != "00")
                    {
                        dt.Rows.Remove(dt.Rows[i]);
                        i--;
                    }
                }
            }

            if (bufferText != "")
            {
                String[] sArray = Regex.Split(bufferText," ",RegexOptions.IgnoreCase);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int count = 0;
                    String str = dt.Rows[i][option].ToString().Trim().ToLower();
                    for (int k = 0; k < sArray.Length; k++)
                    {
                        sArray[k]=sArray[k].ToLower();
                        if (str.IndexOf(sArray[k]) == -1)
                            break;
                        else
                            count++;
                    }
                    if (count < sArray.Length)
                    {
                        dt.Rows.Remove(dt.Rows[i]);
                        i--;
                    }
                }
            }
            return dt;
        }

        private void calendar_DateSelected(object sender, DateRangeEventArgs e)
        {

            startDate = e.Start.ToShortDateString();
            endDate = e.End.ToShortDateString();
        }

        private void dailyReportBtn_Click(object sender, EventArgs e)
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            String report = "D" + calendar.SelectionStart.ToString("yyyy-MM-dd") + ".pdf";
            String dir = @"\Report\Daily\";
            if (!conn.isExistReport(report, dir))
                MessageBox.Show("The report is not exist!");
            else
            {
                String path = Directory.GetCurrentDirectory() + dir;
                if(!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                byte[] buf = conn.downReport(report, dir);
                path += report;
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                BinaryWriter writer = new BinaryWriter(fs);
                writer.Write(buf, 0, buf.Length);
                writer.Close();
                fs.Close();
                ProcessStartInfo Info = new ProcessStartInfo(path);
                Process Pro = System.Diagnostics.Process.Start(Info);
             }
        }

        private void selectedDayReportBtn_Click(object sender, EventArgs e)
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            int sub = 5 - Convert.ToInt32(calendar.SelectionStart.DayOfWeek);
            String friday = calendar.SelectionStart.AddDays(sub).ToString("yyyy-MM-dd");
            String report = "W" + friday + ".pdf";
            String dir = @"\Report\Weekly\";
            if (!conn.isExistReport(report, dir))
                MessageBox.Show("The report is not exist!");
            else
            {
                String path = Directory.GetCurrentDirectory() + dir;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                byte[] buf = conn.downReport(report, dir);
                path += report;
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                BinaryWriter writer = new BinaryWriter(fs);
                writer.Write(buf, 0, buf.Length);
                writer.Close();
                fs.Close();
                ProcessStartInfo Info = new ProcessStartInfo(path);
                Process Pro = System.Diagnostics.Process.Start(Info);
            }
        }

        private void monthlyReportBtn_Click(object sender, EventArgs e)
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            String report = "M" + calendar.SelectionStart.ToString("yyyy-MM") + ".pdf";
            String dir = @"\Report\Monthly\";
            if (!conn.isExistReport(report, dir))
                MessageBox.Show("The report is not exist!");
            else
            {
                String path = Directory.GetCurrentDirectory() + dir;
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                byte[] buf = conn.downReport(report, dir);
                path += report;
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                BinaryWriter writer = new BinaryWriter(fs);
                writer.Write(buf, 0, buf.Length);
                writer.Close();
                fs.Close();
                ProcessStartInfo Info = new ProcessStartInfo(path);
                Process Pro = System.Diagnostics.Process.Start(Info);
            }
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.ShowDialog();
        }

        public static MainFrame getInstance()
        {
            return mainFrame;
        }

        public static void setInstance(MainFrame _mainFrame)
        {
            mainFrame = _mainFrame;
        }

        private void calendar_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void passwordText_TextChanged(object sender, EventArgs e)
        {
            String password = passwordText.Text;
            if (password.Length > 8)
            {
                passwordText.Text = password.Remove(password.Length - 1);
                MessageBox.Show("The length of password should be less than 8.");
            }
            else
            {
                Boolean valid = true;
                for (int i = 0; i < password.Length; i++)
                {
                    char c = password[i];
                    if (!(c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                    {
                        password = password.Remove(i);
                        valid = false;
                    }
                }
                passwordText.Text = password;
                if (!valid)
                    MessageBox.Show("The password can only be composed by letters or digits");
            }
        }

        private void nameText_TextChanged(object sender, EventArgs e)
        {
            String name = nameText.Text;
            if (name.Length > 20)
            {
                nameText.Text = name.Remove(name.Length - 1);
                MessageBox.Show("The length of name should be less than 20.");
            }
            else
            {
                Boolean valid = true;

                for (int i = 0; i < name.Length; i++)
                {
                    char c = name[i];
                    if (!(c == '.' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                    {
                        name = name.Remove(i);
                        valid = false;
                    }
                }
                nameText.Text = name;
                if (!valid)
                    MessageBox.Show("The name can only be composed by letters.");
            }
        }

        private void telephoneText_TextChanged(object sender, EventArgs e)
        {
            String telephone = telephoneText.Text;
            if (telephone.Length > 20)
            {
                telephoneText.Text = telephone.Remove(telephone.Length - 1);
                MessageBox.Show("The length of telephone should be less than 20.");
            }
            else
            {
                Boolean valid = true;
                foreach (char c in telephone)
                {
                    if (!(c >= '0' && c <= '9' || c == '-'))
                    {
                        valid = false;
                        telephone = telephone.Remove(telephone.Length - 1);
                    }
                }
                if (!valid)
                {
                    telephoneText.Text = telephone;
                    MessageBox.Show("Telephone should be composed by digits or -");
                }
            }
        }

        private void revertAccButton_Click(object sender, EventArgs e)
        {
            DataTable dt = accountDataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].Equals(accountBox.SelectedItem.ToString()))
                {
                    passwordText.Text = dt.Rows[i][1].ToString();
                    nameText.Text = dt.Rows[i][2].ToString();
                    telephoneText.Text = dt.Rows[i][3].ToString();
                    currentAccountRoleBox.SelectedItem = dt.Rows[i][5].ToString();
                    if (dt.Rows[i][4].ToString().Equals("activating"))
                        statusCheckBox.Checked = true;
                    else statusCheckBox.Checked = false;
                }
            }
        }

        private void accountBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (accountBox.SelectedIndex != -1)
            {
                detailsGroupBox.Visible = true;
                saveAccountBtn.Visible = true;
                revertAccButton.Visible = true;
                DataTable dt = accountDataSet.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].Equals(accountBox.SelectedItem.ToString()))
                    {
                        passwordText.Text = dt.Rows[i][1].ToString();
                        nameText.Text = dt.Rows[i][2].ToString();
                        telephoneText.Text = dt.Rows[i][3].ToString();
                        currentAccountRoleBox.SelectedItem = dt.Rows[i][5].ToString();
                        if (dt.Rows[i][4].ToString().Equals("activating"))
                            statusCheckBox.Checked = true;
                        else statusCheckBox.Checked = false;
                    }
                }
            }
        }

        private void checkBtn_Click(object sender, EventArgs e)
        {
            if (checkAccountValidity())
            {
                MessageBox.Show("Valid account");
                registerBtn.Enabled = true;
            }
            else
            {
                MessageBox.Show("Invalid Account!");
                registerBtn.Enabled = false;
            }
        }

        private Boolean checkAccountValidity()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet ds = conn.getDataSet("use " + databaseName + " select account from account");
            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (rgsAcc.Text.Equals(dt.Rows[i][0]) || rgsAcc.Text == "")
                    return false;
            }
            return true;
        }

        public string getAccount()
        {
            return rgsAcc.Text;
        }

        private void rgsAcc_TextChanged(object sender, EventArgs e)
        {
            String account = rgsAcc.Text;
            if (account.Length > 10)
            {
                rgsAcc.Text = account.Remove(account.Length - 1);
                MessageBox.Show("The length of account should be less than 10.");
            }
            else
            {
                Boolean valid = true;
                for (int i = 0; i < account.Length; i++)
                {
                    char c = account[i];
                    if (!(c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                    {
                        account = account.Remove(i);
                        valid = false;
                    }
                }
                rgsAcc.Text = account;
                if (!valid)
                    MessageBox.Show("The Account can only be composed by letters or digits");
            }
        }

        private void rgsPwd_TextChanged(object sender, EventArgs e)
        {
            String password = rgsPwd.Text;
            if (password.Length > 8)
            {
                rgsPwd.Text = password.Remove(password.Length - 1);
                MessageBox.Show("The length of password should be less than 8.");
            }
            else
            {
                Boolean valid = true;
                for (int i = 0; i < password.Length; i++)
                {
                    char c = password[i];
                    if (!(c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                    {
                        password = password.Remove(i);
                        valid = false;
                    }
                }
                rgsPwd.Text = password;
                if (!valid)
                    MessageBox.Show("The password can only be composed by letters or digits");
            }
        }

        private void confirmPwd_TextChanged(object sender, EventArgs e)
        {
            String password = confirmPwd.Text;
            if (password.Length > 8)
            {
                confirmPwd.Text = password.Remove(password.Length - 1);
                MessageBox.Show("The length of password should be less than 8.");
            }
            else
            {
                Boolean valid = true;
                for (int i = 0; i < password.Length; i++)
                {
                    char c = password[i];
                    if (!(c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'))
                    {
                        password = password.Remove(i);
                        valid = false;
                    }
                }
                confirmPwd.Text = password;
                if (!valid)
                    MessageBox.Show("The password can only be composed by letters or digits");
            }
        }

        private void rgsName_TextChanged(object sender, EventArgs e)
        {
            String name = rgsName.Text;
            if (name.Length > 20)
            {
                rgsName.Text = name.Remove(name.Length - 1);
                MessageBox.Show("The length of name should be less than 20.");
            }
            else
            {
                Boolean valid = true;

                for (int i = 0; i < name.Length; i++)
                {
                    char c = name[i];
                    if (!(c == '.' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c ==' '))
                    {
                        name = name.Remove(i);
                        valid = false;
                    }
                }
                rgsName.Text = name;
                if (!valid)
                    MessageBox.Show("The name can only be composed by letters.");
            }
        }

        private void rgsTel_TextChanged(object sender, EventArgs e)
        {
            String telephone = rgsTel.Text;
            if (telephone.Length > 20)
            {
                rgsTel.Text = telephone.Remove(telephone.Length - 1);
                MessageBox.Show("The length of telephone should be less than 20.");
            }
            else
            {
                Boolean valid = true;
                foreach (char c in telephone)
                {
                    if (!(c >= '0' && c <= '9' || c == '-'))
                    {
                        valid = false;
                        telephone = telephone.Remove(telephone.Length - 1);
                    }
                }
                if (!valid)
                {
                    rgsTel.Text = telephone;
                    MessageBox.Show("Telephone should be composed by digits or -");
                }
            }
        }

        private void registerBtn_Click(object sender, EventArgs e)
        {
            String warning = "The information has some errors: \n";
            if(!checkAccountValidity())
                warning += "The account are used or invalid! \n";
            if (rgsPwd.Text == "")
                warning += "Null Password! \n";
            if (confirmPwd.Text != rgsPwd.Text)
                warning += "You need Confirm Password again! \n";
            if (rgsName.Text == "")
                warning += "Null Name! \n";
            if (rgsTel.Text == "")
                warning += "Null Telephone!";
            if (roleBox.SelectedIndex == -1)
                warning += "Null Role!";
            if (warning != "The information has some errors: \n")
                MessageBox.Show(warning,"Warning");       
            else
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                conn.getDataSet("use " + databaseName + " insert into account(account, password, name, telephone, status, role) values ('" + rgsAcc.Text + "','" + rgsPwd.Text.Trim() + "','" + rgsName.Text + "','" + rgsTel.Text + "','activating','"+roleBox.SelectedItem.ToString()+"')");
                conn.addText("Client: " + localIPAddress + " has registered a new Account: " + rgsAcc.Text + "\r\n");
                MessageBox.Show("Registered");
                rgsAcc.Text = "";
                rgsName.Text = "";
                rgsPwd.Text = "";
                confirmPwd.Text = "";
                rgsTel.Text = "";
                tipTitle.Text = "";
                tipText.Text = "";
                tipTitle.Text = "";
                tipText.Text = "";
                registerGroup.Visible = !registerGroup.Visible;
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            registerGroup.Visible = !registerGroup.Visible;
            rgsAcc.Text = "";
            rgsName.Text = "";
            rgsPwd.Text = "";
            confirmPwd.Text = "";
            rgsTel.Text = "";
        }

        private void rgsAcc_Enter(object sender, EventArgs e)
        {
            tipTitle.Text = "Account";
            tipText.Text = "Maximum of 10 digits and/or letters in length. \r\nYou should press \"Check\" button to verify the uniqueness.";
        }

        private void rgsPwd_Enter(object sender, EventArgs e)
        {
            tipTitle.Text = "Password";
            tipText.Text = "Maximum of 8 digits and/or letters in length. \r\nIt will be relaced by \"*\" on the screen.";
        }

        private void confirmPwd_Enter(object sender, EventArgs e)
        {
            tipTitle.Text = "Confirm Password";
            tipText.Text = "Re-enter the password for verification. \r\nIt will be relaced by \"*\" on the screen too.";
        }

        private void rgsName_Enter(object sender, EventArgs e)
        {
            tipTitle.Text = "Name";
            tipText.Text = "Maximum of 20 letters in length. \r\nOnly one space(or \".\") between letters are valid.";
        }

        private void rgsTel_Enter(object sender, EventArgs e)
        {
            tipTitle.Text = "Telephone";
            tipText.Text = "Maximum of 20 digits in length. \r\nOnly one \"-\" between digits are valid.";
        }

        private void radioShipTime_CheckedChanged(object sender, EventArgs e)
        {
            slipText.Visible = !slipText.Visible;
            shipTimePicker.Visible = !shipTimePicker.Visible;
            if (radioShipTime.Checked)
            {
                option = 3;
                slipText.Text = shipTimePicker.Value.ToShortDateString().Trim()+" ";
            }
            else
                slipText.Text = "";
        }

        private void slipSearchBtn_Click(object sender, EventArgs e)
        {
            bufferText = slipText.Text;
            slipOrderBox.Items.Clear();
            slipCheckBtn.Enabled = false;
            DataTable dt= new DataTable();
            slipOrderDataSet = new DataSet();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            slipOrderDataSet = conn.getDataSet("use " + databaseName + " select [slipOrder], name, address, shipDate, shipState from slipOrder order by slipOrder DESC");
            if (slipOrderDataSet != null || slipOrderDataSet.Tables[0].Rows.Count != 0)
            {
                dt = slipOrderDataSet.Tables[0];
                dt = slipFilter(dt);
                if (dt != null || dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        slipOrderBox.Items.Add(dt.Rows[i][0].ToString().Trim());
                    }
                }
                else
                    slipOrderBox.Items.Add("(Null)");
            }
            else
                slipOrderBox.Items.Add("(Null)");
            slipOrderBox.DroppedDown = true;
        }

        private void radioCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if(radioCustomer.Checked)
                option = 1;
        }

        private void radioSlipNum_CheckedChanged(object sender, EventArgs e)
        {
            if(radioSlipNum.Checked)
                option = 0;
        }

        private void radioAddress_CheckedChanged(object sender, EventArgs e)
        {
            if(radioAddress.Checked)
                option = 2;
        }

        private void shipTimePicker_ValueChanged(object sender, EventArgs e)
        {
            slipText.Text = shipTimePicker.Value.ToShortDateString().Trim() +" ";
        }

        private void skuSearchText_TextChanged(object sender, EventArgs e)
        {
            String text = skuSearchText.Text;
            if (text.Length > 20)
            {
                skuSearchText.Text = text.Remove(text.Length - 1);
                MessageBox.Show("The length of name should be less than 20.");
            }
        }

        private void skuSearch_Click(object sender, EventArgs e)
        {
            if (skuSearchText.Text != "")
            {
                String[] sArray = Regex.Split(skuSearchText.Text, " ", RegexOptions.IgnoreCase);
                skuBox.Items.Clear();
                for (int i = 0; i < skuDataSet.Tables[0].Rows.Count; i++)
                {
                    String str = "";
                    if (skuNameBtn.Checked == true)
                        str = skuDataSet.Tables[0].Rows[i][0].ToString().ToLower();
                    else
                        str = skuDataSet.Tables[0].Rows[i][1].ToString().ToLower();
                    int count = 0;
                    for (int k = 0; k < sArray.Length; k++)
                    {
                        sArray[k] = sArray[k].ToLower();
                        if (str.IndexOf(sArray[k]) != -1)
                            count++;
                        else break;
                        if (count == sArray.Length)
                            skuBox.Items.Add(skuDataSet.Tables[0].Rows[i][0].ToString());
                    }
                }
            }
            else
            {
                for (int i = 0; i < skuDataSet.Tables[0].Rows.Count; i++)
                {
                    skuBox.Items.Add(skuDataSet.Tables[0].Rows[i][0].ToString());
                }
            }
            skuBox.DroppedDown = true;
        }


        private void slipOrderGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (slipOrderGridView.CurrentCell.ColumnIndex == 0 && slipOrderGridView.CurrentCell.Value.ToString()!="")
            {
                int rowCount = slipOrderGridView.CurrentCell.RowIndex;
                String slipOrder = slipOrderGridView.CurrentCell.Value.ToString();
                String customer = slipOrderGridView[1, rowCount].Value.ToString();
                String address = slipOrderGridView[2, rowCount].Value.ToString();
                String date = Convert.ToDateTime(slipOrderGridView[4, rowCount].Value).ToShortDateString();
                slipOrderBox.Items.Clear();
                bufferText = "a";
                slipOrderBox.Items.Add(slipOrder);
                slipOrderBox.SelectedItem = slipOrder;
                customerText.Text = customer;
                timeText.Text = date;
                addressText.Text = address;
                slipCheckBtn.Enabled = true;
            }
        }

        private void updateOrderBtn_Click(object sender, EventArgs e)
        {
            int count = slipOrderGridView.Rows.Count;
            int maxOrder = 10000 + count - 2;
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataTable dt = conn.getDataSet("use " + databaseName + " select slipOrder, name, address, processor, shipDate, shipState from slipOrder where slipOrder>'"+maxOrder+"'").Tables[0];
            if (dt.Rows.Count == 0)
                MessageBox.Show("No New Slip Order has been added!");
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    DataRow row = orderDt.NewRow();
                    row[0] = dr[0].ToString();
                    row[1] = dr[1].ToString();
                    char[] separator = { '@' };
                    String[] arr = dr[2].ToString().Split(separator);
                    row[2] = arr[0] + " " + arr[1] + " " + arr[2] + " " + arr[3];
                    row[3] = dr[3].ToString();
                    row[4] = dr[4].ToString();
                    if (dr[5].ToString().Equals("00"))
                    {
                        row[5] = "No";
                        row[6] = "No";
                    }
                    else if (dr[5].ToString().Equals("10"))
                    {
                        row[5] = "Yes";
                        row[6] = "No";
                    }
                    else if (dr[5].ToString().Equals("11"))
                    {
                        row[5] = "Yes";
                        row[6] = "Yes";
                    }
                    orderDt.Rows.Add(row);
                }
                MessageBox.Show(dt.Rows.Count+" Order(s) have been added!");
            }

        }



        private void shipCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            screenSlipOrder();
        }

        private void packCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!packCheckBox.Checked)
            {
                shipCheckBox.Checked = false;
                shipCheckBox.Enabled = false;
            }
            else
                shipCheckBox.Enabled = true;

            screenSlipOrder();
        }

        private void liveCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            screenSlipOrder();
        }

        private void historyCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            screenSlipOrder();
        }

        private void screenSlipOrder()
        {
            if(allCheckBox.Checked)
                screenSlipOrderState("all");
            else if (packCheckBox.Checked && shipCheckBox.Checked)
                screenSlipOrderState("11");
            else if (packCheckBox.Checked && !shipCheckBox.Checked)
                screenSlipOrderState("10");
            else if (!packCheckBox.Checked)
                screenSlipOrderState("00");

            if (liveCheckBox.Checked && !historyCheckBox.Checked)
                screenSlipOrderRange("live");
            if (!liveCheckBox.Checked && historyCheckBox.Checked)
                screenSlipOrderRange("history");
            if (!liveCheckBox.Checked && !historyCheckBox.Checked)
                screenSlipOrderRange("none");

        }

        private void screenSlipOrderState(String state)
        {
            cm = (CurrencyManager)BindingContext[slipOrderGridView.DataSource];
            for (int i = 0; i < slipOrderGridView.Rows.Count; i++)
                slipOrderGridView.Rows[i].Visible = true;

            if (state == "11")
                for (int i = 0; i < slipOrderGridView.Rows.Count-1; i++)
                {
                    if (slipOrderGridView.Rows[i].Cells[5].Value.ToString() == "Yes" && slipOrderGridView.Rows[i].Cells[6].Value.ToString() == "Yes")
                        continue;
                    else
                    {
                        cm.SuspendBinding();
                        slipOrderGridView.Rows[i].Visible = false;
                        cm.ResumeBinding();
                    }
                }
            if(state == "10")
                for (int i = 0; i < slipOrderGridView.Rows.Count-1; i++)
                {
                    if (slipOrderGridView.Rows[i].Cells[5].Value.ToString() == "Yes" && slipOrderGridView.Rows[i].Cells[6].Value.ToString() == "No")
                        continue;
                    else
                    {
                        cm.SuspendBinding();
                        slipOrderGridView.Rows[i].Visible = false;
                        cm.ResumeBinding();
                    }
                }
            if (state == "00")
                for (int i = 0; i < slipOrderGridView.Rows.Count-1; i++)
                {
                    if (slipOrderGridView.Rows[i].Cells[5].Value.ToString() == "No" && slipOrderGridView.Rows[i].Cells[6].Value.ToString() == "No")
                        continue;
                    else
                    {
                        cm.SuspendBinding();
                        slipOrderGridView.Rows[i].Visible = false;
                        cm.ResumeBinding();
                    }
                }
        }

        private void screenSlipOrderRange(String range)
        {
            cm = (CurrencyManager)BindingContext[slipOrderGridView.DataSource];
            if (range == "none")
                for (int i = 0; i < slipOrderGridView.Rows.Count - 1; i++)
                {
                    cm.SuspendBinding();
                    slipOrderGridView.Rows[i].Visible = false;
                    cm.ResumeBinding();
                }

            if (range == "live")
                for (int i = 0; i < slipOrderGridView.Rows.Count - 1; i++)
                {
                    if (Convert.ToDateTime(slipOrderGridView.Rows[i].Cells[4].Value.ToString()) > DateTime.Now)
                        continue;
                    else
                    {
                        cm.SuspendBinding();
                        slipOrderGridView.Rows[i].Visible = false;
                        cm.ResumeBinding();
                    }
                }
            if (range == "history")
                for (int i = 0; i < slipOrderGridView.Rows.Count - 1; i++)
                {
                    if (Convert.ToDateTime(slipOrderGridView.Rows[i].Cells[4].Value.ToString()) <= DateTime.Now)
                        continue;
                    else
                    {
                        cm.SuspendBinding();
                        slipOrderGridView.Rows[i].Visible = false;
                        cm.ResumeBinding();
                    }
                }
        }

        private void allCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (allCheckBox.Checked)
            {
                shipCheckBox.Checked = false;
                shipCheckBox.Enabled = false;
                packCheckBox.Checked = false;
                packCheckBox.Enabled = false;
            }

            if (!allCheckBox.Checked)
                packCheckBox.Enabled = true;

            screenSlipOrder();
        }







        /*
        private void removeSlipBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Warning! This Slip Order will be removed. Do you still continue?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                conn.getDataSet("use "+databaseName+" insert into removedSlipOrder(slipOrder) values('"+slipOrderBox.SelectedItem.ToString()+"');");
                conn.addText("Client: " + localIPAddress + " has removed Slip Order: " + slipOrderBox.SelectedItem.ToString() + " at " + DateTime.Now + "\r\n");
                slipOrderBox.SelectedIndex = -1;
            }
        }

       /*

        private void itemSearchBtn_Click(object sender, EventArgs e)
        {
            bufferText = itemBox.Text;
            itemBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            if(bufferText!="")
                itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item where charindex('" + bufferText + "',[item])!=0");
            else
                itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item");
            itemDescriptionText.Text = "";
            weightText.Text = "";
            restQtyText.Text = "";
            if (itemDataSet.Tables[0].Rows.Count != 0)
            {
                DataTable dt = itemDataSet.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                    itemBox.Items.Add(dt.Rows[i][0].ToString().Trim());
            }
            else
                itemBox.Items.Add("(Null)");
            itemBox.DroppedDown = true;
        }
        */
        /*
        private void itemAddBtn_Click(object sender, EventArgs e)
        {
            Item checkItem = new Item("Add");
            checkItem.ShowDialog();
        }
         */

        
      

        

    }
}
    

