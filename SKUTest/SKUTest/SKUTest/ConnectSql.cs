﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace SKUTest
{
    class ConnectSql
    {
        private SqlConnection conn;
        private String connString;
        private SqlCommand cmd;
        private SqlDataAdapter sda=new SqlDataAdapter();
        private DataSet ds=new DataSet();
        private String databaseName = "skuDatabase";
        
        public ConnectSql()
        {
            try
            {
                connString = "server=localhost;Integrated Security=True;database="+databaseName;
                conn = new SqlConnection(connString);
                conn.Open();
                cmd = new SqlCommand();
                cmd.Connection = conn;
            }
            catch(Exception ex)
            {

            }
        }

        public DataSet getDataSet(string sqlCommand)
        {
            try
            {
                cmd.CommandText = sqlCommand;
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                return ds;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public SqlDataAdapter getDataAdapter(string sqlCommand)
        {
            try
            {
                cmd.CommandText = sqlCommand;
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                return sda;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /*
        public DataSet getRelationalDataSet(string parentTableName, string childTableName, string keyColumn )
        {
            try
            {
                cmd.CommandText = sqlCommand;
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
           // DataRelation relation =new DataRelation("relation",parentColumn,childColumn);
            //ds.Relations.Add(relation);
        }
         * */

    }
}
