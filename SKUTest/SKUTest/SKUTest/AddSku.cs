﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SKUTest
{
    public partial class AddSku : Form
    {
        private PackingSlip packingSlip;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
        private DataSet addSkuDs = new DataSet();
        private DataSet addItemDs = new DataSet();
        private Boolean hasClickSaveBtn = false;
        private List<String[]> tempSkuList = new List<String[]>();
        private List<String[]> tempItemList = new List<String[]>();
        private List<String[]> tempEditItemByItem = new List<String[]>();

        public AddSku(PackingSlip packingSlip)
        {
            InitializeComponent();
            this.packingSlip = packingSlip;
            changeBtn.Enabled = false;
            initDataGridView();
            skuBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            DataSet ds = conn.getDataSet("use " + databaseName + " select sku from sku");
            DataTable dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                skuBox.Items.Add(row[0].ToString());
            }
        }

        private void initDataGridView()
        {
            DataColumn c0 = new DataColumn("SKU#");
            DataColumn c1 = new DataColumn("Description");
            DataColumn c2 = new DataColumn("New Added Quantity");
            DataColumn c3 = new DataColumn("Rest Quantity In Slip Order");
            DataTable dt = new DataTable();
            dt.Columns.Add(c0);
            dt.Columns.Add(c1);
            dt.Columns.Add(c2);
            dt.Columns.Add(c3);
            addSkuDs.Tables.Add(dt);
            addSkuDataGridView.DataSource = addSkuDs.Tables[0];
            addSkuDataGridView.Columns[1].Width = 200;
            addSkuDataGridView.ReadOnly = true;


            //pop list
            //init addSkuDs if there are skus in packingSlip.skuList
            packingSlip.filterList();
            foreach (String[] s in packingSlip.skuList)
            {
                DataRow r = addSkuDs.Tables[0].NewRow();
                r[0] = s[0];//SKU#       
                r[2] = s[1];//Edited quantity
                foreach (DataRow rr in packingSlip.skuDs.Tables[0].Rows)
                {
                    if (rr[0].ToString().Equals(s[0]))
                    {
                        r[1] = rr[1];//description
                        r[3] = rr[2];//existed quantity
                    }
                }
                addSkuDs.Tables[0].Rows.Add(r);
            }
            
            DataColumn cc0 = new DataColumn("Item#");
            DataColumn cc1 = new DataColumn("Description");
            DataColumn cc2 = new DataColumn("New Added Quantity");
            DataColumn cc3 = new DataColumn("Rest Quantity In Database");
            DataTable dtt = new DataTable();
            dtt.Columns.Add(cc0);
            dtt.Columns.Add(cc1);
            dtt.Columns.Add(cc2);
            dtt.Columns.Add(cc3);
            addItemDs.Tables.Add(dtt);
            addItemDataGridView.DataSource = addItemDs.Tables[0];
            addItemDataGridView.Columns[1].Width = 200;
            addItemDataGridView.ReadOnly = true;

            //init the tempEditByItem list for calculate rest quantity of items
            foreach (String[] s in packingSlip.editItemByItem)
            {
                tempEditItemByItem.Add(s);
            }

            calculateItems();
            //save the skuList and editItemBySku in temporary memory. If user click 'cancel' button, it would be useful.
            foreach (String[] s in packingSlip.skuList)
            {
                tempSkuList.Add(s);
            }
            foreach (String[] s in packingSlip.editItemBySku)
            {
                tempItemList.Add(s);
            }

            packingSlip.skuList.Clear();
            packingSlip.editItemBySku.Clear();
        }

        

        private void okBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Your change will be applied to this slip order, but do not saved into database until you save this slip order. Do you want to continue?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                hasClickSaveBtn = true;
                foreach (DataRow r in addSkuDs.Tables[0].Rows)
                {
                    //justify whether the sku is new or not
                    Boolean isNew = true;
                    DataTable dt = packingSlip.skuDs.Tables[0];
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row[0].ToString().Equals(r[0].ToString()))
                        {
                            isNew = false;
                            break;
                        }
                    }

                    //add new sku to slipOrder in dataGridView
                    if (isNew)
                    {
                        SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

                        DataRow row = dt.NewRow();
                        row[0] = r[0].ToString();//sku#
                        row[1] = conn.getDataSet("use " + databaseName + " select productDetails from sku where sku='"+r[0].ToString()+"';").Tables[0].Rows[0][0].ToString();
                        row[2] = r[2].ToString();
                        dt.Rows.Add(row);
                        
                    }
                    else //add quantity to that sku if already exist in dataGridView
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr[0].ToString().Equals(r[0].ToString()))
                            {
                                dr[2] = r[3];
                                break;
                            }
                        }
                    }
                    
                    //refresh items of that sourceTable
                    packingSlip.filterList();
                    refreshItems();
                
                }
              
                this.Close();
            }
        }

        private void pushList()
        {
            //signal the skuList in packingSlip
            foreach (DataRow r in addSkuDs.Tables[0].Rows)
            {
                String[] editSku = new String[2];
                editSku[0] = r[0].ToString();
                editSku[1] = r[2].ToString();
                packingSlip.skuList.Add(editSku);
            }
            //signal the editItemBySku in packingSlip
            foreach (DataRow r in addItemDs.Tables[0].Rows)
            {
                String[] editItem = new String[2];
                editItem[0] = r[0].ToString();
                editItem[1] = r[2].ToString();
                packingSlip.editItemBySku.Add(editItem);
            }
        }

        private void pushPreviousList()
        {
            foreach (String[] s in tempSkuList)
            {
                packingSlip.skuList.Add(s);
            }
            foreach (String[] s in tempItemList)
            {
                packingSlip.editItemBySku.Add(s);
            }
        }

        private void refreshItems()
        {
            DataTable skuTable = packingSlip.skuDs.Tables[0];
            DataTable itemTable = packingSlip.itemDs.Tables[0];
            packingSlip.refreshItemDataGridView(skuTable, itemTable);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!hasClickSaveBtn)
            {
                if (MessageBox.Show(this, "You have not clicked 'OK' button after the changes. Do you want to close?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    
                    this.Close();
                }
            }
           
        }

        private void changeBtn_Click(object sender, EventArgs e)
        {
            hasClickSaveBtn = false;
            Boolean isNewInAddSku = true;
            Boolean isNewInOrder = true;
            foreach (DataRow r in addSkuDs.Tables[0].Rows)
            {
                if (r[0].ToString().Equals(skuBox.SelectedItem.ToString()))
                {
                    isNewInAddSku = false;
                    break;
                }
            }
            foreach (DataRow rr in packingSlip.skuDs.Tables[0].Rows)
            {
                if (rr[0].ToString().Equals(skuBox.SelectedItem.ToString()))
                {
                    isNewInOrder = false;
                    break;
                }
            }

            if (isNewInAddSku && isNewInOrder)
            {
                if (quantityNumber.Value <= 0)
                    MessageBox.Show("Quantity should be greater than 0.");
                else
                {
                    DataRow r = addSkuDs.Tables[0].NewRow();
                    r[0] = skuBox.SelectedItem.ToString();
                    SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                    r[1] = conn.getDataSet("use " + databaseName + " select productDetails from sku where sku='" + skuBox.SelectedItem.ToString() + "';").Tables[0].Rows[0][0].ToString();
                    r[2] = quantityNumber.Value.ToString();
                    r[3] = quantityNumber.Value.ToString();
                    addSkuDs.Tables[0].Rows.Add(r);
                }
            }
            else if (isNewInAddSku && !isNewInOrder)
            {
                String existedQty = "0";
                foreach (DataRow r in packingSlip.skuDs.Tables[0].Rows)
                {
                    if (r[0].ToString().Equals(skuBox.SelectedItem.ToString()))
                        existedQty = r[2].ToString();
                }
                if (-quantityNumber.Value > Convert.ToInt32(existedQty))
                {
                    MessageBox.Show("Error! This will make quantity of SKU in Slip Order less than 0.");
                }
                else if (quantityNumber.Value == 0)
                    MessageBox.Show("Quantity should be greater than 0.");
                else
                {
                    DataRow r = addSkuDs.Tables[0].NewRow();
                    r[0] = skuBox.SelectedItem.ToString();
                    SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                    r[1] = conn.getDataSet("use " + databaseName + " select productDetails from sku where sku='" + skuBox.SelectedItem.ToString() + "';").Tables[0].Rows[0][0].ToString();
                    r[2] = quantityNumber.Value.ToString();
                    r[3] = Convert.ToInt32(existedQty) + quantityNumber.Value;
                    addSkuDs.Tables[0].Rows.Add(r);
                }
            }
            else if (!isNewInAddSku)
            {
                
                for (int i = 0; i < addSkuDs.Tables[0].Rows.Count;i++ )
                {
                    DataRow r = addSkuDs.Tables[0].Rows[i];
                    if (r[0].ToString().Equals(skuBox.SelectedItem.ToString()))
                    {
                        if (Convert.ToInt32(r[3]) + quantityNumber.Value < 0)
                        {
                            
                            MessageBox.Show("The quantity should not be less than 0.");
                        }
                            /*
                        else if (Convert.ToInt32(r[2]) + quantityNumber.Value == 0)
                        {
                            addSkuDs.Tables[0].Rows.Remove(r);
                        }
                             * */
                        else
                        {
                            r[2] = (Convert.ToInt32(r[2]) + quantityNumber.Value).ToString();
                            r[3] = (Convert.ToInt32(r[3]) + quantityNumber.Value).ToString();
                        }
                    }
                }
            }
            calculateItems();
            detectFailure();
            
           
            
        }
        private void calculateItems()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            addItemDs.Tables[0].Rows.Clear();
            foreach (DataRow r in addSkuDs.Tables[0].Rows)
            {

                DataSet ds = conn.getDataSet("use " + databaseName + " select sku_item.item, item.description,sku_item.itemQuantity, item.quantity from sku_item,item where sku_item.sku='" + r[0].ToString() + "' and item.item=sku_item.item;");

                foreach (DataRow rr in ds.Tables[0].Rows)
                {
                    //get quantity from editItemByItem to synchronize the rest quantity
                    Decimal qtyInEditItemByItem = 0;
                    for (int i = 0; i < tempEditItemByItem.Count; i++)
                    {
                        if (rr[0].ToString().Equals(tempEditItemByItem[i][0]))
                        {
                            qtyInEditItemByItem += Convert.ToDecimal(tempEditItemByItem[i][1]);
                            
                        }
                    }
                    //check new or not to addItemDs
                    Boolean isNewToThis = true;
                    foreach (DataRow rrr in addItemDs.Tables[0].Rows)
                    {
                        if (rrr[0].ToString().Equals(rr[0].ToString()))
                        {

                            
                            //not new/  qty of item in each sku /  qty of added sku
                            rrr[2] = Convert.ToInt32(rrr[2]) + Convert.ToInt32(rr[2]) * Convert.ToInt32(r[2]);

                            rrr[3] = -qtyInEditItemByItem + Convert.ToInt32(conn.getDataSet("use " + databaseName + " select quantity from item where item='" + rr[0].ToString() + "';").Tables[0].Rows[0][0]) - Convert.ToInt32(rrr[2]);
                            
                            isNewToThis = false;

                        }
                    }
                    if (isNewToThis)
                    {
                        DataRow newRow = addItemDs.Tables[0].NewRow();
                        newRow[0] = rr[0].ToString();
                        newRow[1] = rr[1].ToString();
                        newRow[2] = Convert.ToInt32(r[2]) * Convert.ToInt32(rr[2]);
                        

                        newRow[3] = -qtyInEditItemByItem + Convert.ToInt32(conn.getDataSet("use " + databaseName + " select quantity from item where item='" + rr[0].ToString() + "';").Tables[0].Rows[0][0]) - Convert.ToInt32(newRow[2]);
                        addItemDs.Tables[0].Rows.Add(newRow);
                    }
                }
            }
            
            
            if (addSkuDs.Tables[0].Rows.Count == 0)
                addItemDs.Tables[0].Rows.Clear();
        }

        private void detectFailure()
        {
            Boolean failure = false;
            for (int i = 0; i < addItemDs.Tables[0].Rows.Count;i++ )
            {
                DataRow r = addItemDs.Tables[0].Rows[i];
                if (Convert.ToInt32(r[3]) < 0)
                {
                    failure = true;
                }
                /*
                if (Convert.ToInt32(r[2]) == 0)
                {
                    addItemDs.Tables[0].Rows.Remove(r);
                }*/
            }
            if (failure)
            {
               
                foreach (DataRow r in addSkuDs.Tables[0].Rows)
                {
                    if (r[0].ToString().Equals(skuBox.SelectedItem.ToString()))
                    {
                        r[2] = (Convert.ToInt32(r[2]) - quantityNumber.Value).ToString();
                        r[3] = (Convert.ToInt32(r[3]) - quantityNumber.Value).ToString();
                    }
                }
               
                MessageBox.Show("Error! This makes items quantities less than 0.");
                calculateItems();
            }
            else
            {
                okBtn.Enabled = true;
            }
         
        }

        private void skuBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            skuDescriptionTextBox.Text = conn.getDataSet("use " + databaseName + " select productDetails from sku where sku='" + skuBox.SelectedItem.ToString() + "';").Tables[0].Rows[0][0].ToString();
            changeBtn.Enabled = true;
            okBtn.Enabled = true;
        }

        private void AddSku_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (hasClickSaveBtn)
                pushList();
            else
                pushPreviousList();
     
        }

        private void addSkuDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            skuBox.SelectedItem = addSkuDataGridView[0, addSkuDataGridView.CurrentCell.RowIndex].Value.ToString();
        }


        

        
    }
}
