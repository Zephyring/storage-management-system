﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace SKUTest
{
    public partial class Item : Form
    {
        private DataSet ds;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
        private String currentValue;
        private List<String> modifiedItem = new List<String>();
        private DataSet newItem = new DataSet();
        private Boolean hasClickSaveBtn = true;
        private DataSet itemDataSet;
        private String option = "Item#";
        private String quantity = "";
        private CurrencyManager cm;

        public Item()
        {
           InitializeComponent();
        }

        private void Item_Load(object sender, EventArgs e)
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            ds = conn.getDataSet("use " + databaseName + " select itemNum as Number, item as Item#, description as Description, quantity as Quantity, weight as 'Weight(LBS)', size as Size, price as Price, UPC, Barcode, SerialNo from item");
            itemGridView.DataSource = ds.Tables[0];
            cm = (CurrencyManager)BindingContext[itemGridView.DataSource];
            itemGridView.Columns[0].Width = 60;//number
            itemGridView.Columns[1].Width = 110;//itemName
            itemGridView.Columns[2].Width = 280;//description
            itemGridView.Columns[3].Width = 60;//quantity
            itemGridView.Columns[4].Width = 80;//weight
            itemGridView.Columns[5].Width = 60;//size
            itemGridView.Columns[6].Width = 55;//price
            itemGridView.Columns[7].Width = 100;//UPC
            itemGridView.Columns[8].Width = 100;//Barcode
            itemGridView.Columns[9].Width = 100;//SerialNo

            itemGridView.Columns[0].ReadOnly = true;
            DataColumn c0 = new DataColumn("Number");
            DataColumn c1 = new DataColumn("Item#");
            DataColumn c2 = new DataColumn("Description");
            DataColumn c3 = new DataColumn("Quantity");
            DataColumn c4 = new DataColumn("Weight(LBS)");
            DataColumn c5 = new DataColumn("Size");
            DataColumn c6 = new DataColumn("Price");
            DataColumn c7 = new DataColumn("UPC");
            DataColumn c8 = new DataColumn("Barcode");
            DataColumn c9 = new DataColumn("SerialNo");
            DataTable t = new DataTable();
            t.Columns.Add(c0);
            t.Columns.Add(c1);
            t.Columns.Add(c2);
            t.Columns.Add(c3);
            t.Columns.Add(c4);
            t.Columns.Add(c5);
            t.Columns.Add(c6);
            t.Columns.Add(c7);
            t.Columns.Add(c8);
            t.Columns.Add(c9);
            newItem.Tables.Add(t);

            SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            itemDataSet = conn1.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item");

            if (itemDataSet.Tables[0].Rows.Count != 0)
            {
                DataTable dt = itemDataSet.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                    itemBox.Items.Add(dt.Rows[i][0].ToString().Trim());
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            hasClickSaveBtn = true;
            defaultList();
            if (MessageBox.Show(this, "Do you want to save?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                foreach (DataRow r in newItem.Tables[0].Rows)
                {
                    conn.getDataSet("use " + databaseName + " insert into item(itemNum,item,description,quantity,weight,size,price,UPC,Barcode,SerialNo) values('" + r[0].ToString().Trim() + "','" + r[1].ToString().Trim() + "','" + r[2].ToString().Trim() + "','" + r[3].ToString().Trim() + "','" + r[4].ToString().Trim() + "','" + r[5].ToString().Trim() + "','" + r[6].ToString().Trim() + "','" + r[7].ToString().Trim() + "','" + r[8].ToString().Trim() + "','" + r[9].ToString().Trim() + "');");
                    conn.addText("Client: " + localIPAddress + " has added new Item: " + r[0].ToString().Trim() + "\r\n");
                }
                foreach (String s in modifiedItem)
                {
                    int rowNum = Convert.ToInt32(s) - 100;
                    String itemName = itemGridView.Rows[rowNum].Cells["Item#"].Value.ToString();
                    String description = itemGridView.Rows[rowNum].Cells["Description"].Value.ToString();
                    String quantity = itemGridView.Rows[rowNum].Cells["Quantity"].Value.ToString();
                    String weight = itemGridView.Rows[rowNum].Cells["Weight(LBS)"].Value.ToString();
                    String size = itemGridView.Rows[rowNum].Cells["Size"].Value.ToString();
                    String price = itemGridView.Rows[rowNum].Cells["Price"].Value.ToString();
                    String UPC = itemGridView.Rows[rowNum].Cells["UPC"].Value.ToString();
                    String Barcode = itemGridView.Rows[rowNum].Cells["Barcode"].Value.ToString();
                    String SerialNo = itemGridView.Rows[rowNum].Cells["SerialNo"].Value.ToString();
                    conn.getDataSet("use " + databaseName + " update item set item='"+itemName+"',description='" + description + "',quantity='" + quantity + "',weight='" + weight + "',size='" + size + "',price='" + price + "',UPC='" + UPC + "',Barcode='" + Barcode + "',SerialNo='" + SerialNo + "' where itemNum='" + s + "';");
                    conn.addText("Client: " + localIPAddress + " has modified Item: " + s + " "+itemName+"\r\n");

                }

                
                MessageBox.Show("You have successfully saved data into database.");
                this.Close();
            }
            
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            hasClickSaveBtn = false;
            ds.Tables[0].Rows.Add(itemGridView.Rows.Count + 99, "Modify name here...", "Modify description here...", "1", "1", "", "", "", "");
            DataRow r = newItem.Tables[0].NewRow();
            r[0] = itemGridView.Rows.Count + 98;
            r[1] = "Modify name here...";
            r[2] = "Modify description here...";
            r[3] = "1";
            r[4] = "1";
            r[5] = "";
            r[6] = "";
            r[7] = "";
            r[8] = "";
            r[9] = "";
            newItem.Tables[0].Rows.Add(r);
            itemBox.Items.Add((itemGridView.Rows.Count + 98).ToString());
            itemGridView.Rows[itemGridView.Rows.Count - 2].Cells["Number"].Style.BackColor = System.Drawing.Color.Red;
            itemGridView.CurrentCell = itemGridView.Rows[itemGridView.Rows.Count - 2].Cells[1];
        }


        private void itemGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (itemGridView.CurrentCell.RowIndex==(itemGridView.Rows.Count-1))
                itemGridView.ReadOnly = true;
            else
            {
                itemGridView.ReadOnly = false;
                itemGridView.Columns[0].ReadOnly = true;
            }
        }

        private void itemGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            hasClickSaveBtn = false;
            String s = itemGridView.Rows[itemGridView.CurrentCell.RowIndex].Cells["Number"].Value.ToString();
            if(!modifiedItem.Contains(s))
                modifiedItem.Add(s);
         
            if (itemGridView.CurrentCell.ColumnIndex == 3)//quantity
            {
                try
                {
                    if (Convert.ToInt32(itemGridView.CurrentCell.Value) <= 0)
                    {
                        itemGridView.CurrentCell.Value = currentValue;
                        MessageBox.Show("It should be number greater than 0");
                    }
                }
                catch (Exception ex)
                {
                    itemGridView.CurrentCell.Value = currentValue;
                    MessageBox.Show("It should be number greater than 0");
                }
            }
            if (itemGridView.CurrentCell.ColumnIndex == 4)//weight
            {
                try
                {
                    if (Convert.ToDouble(itemGridView.CurrentCell.Value) <= 0)
                    {
                        itemGridView.CurrentCell.Value = currentValue;
                        MessageBox.Show("It should be number greater than 0");
                    }
                }
                catch (Exception ex)
                {
                    itemGridView.CurrentCell.Value = currentValue;
                    MessageBox.Show("It should be number greater than 0");
                }
            }
            if (itemGridView.CurrentCell.ColumnIndex >= 3)
            {
                if(itemGridView.CurrentCell.Value.ToString().Length > 49)
                {
                    itemGridView.CurrentCell.Value=currentValue;
                    MessageBox.Show("The length cannot be longer than 50 letters.");
                }
            }
            if (itemGridView.CurrentCell.ColumnIndex == 1)
            {
                if (itemGridView.CurrentCell.Value.ToString().Length > 49)
                {
                    itemGridView.CurrentCell.Value = currentValue;
                    MessageBox.Show("The length cannot be longer than 200 letters.");
                }
            }
            if (itemGridView.CurrentCell.ColumnIndex == 2)
            {
                if (itemGridView.CurrentCell.Value.ToString().Length > 199)
                {
                    itemGridView.CurrentCell.Value = currentValue;
                    MessageBox.Show("The length cannot be longer than 200 letters.");
                }
            }
        }

        private void itemGridView_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            currentValue = itemGridView.CurrentCell.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!hasClickSaveBtn)
            {
                if (MessageBox.Show(this, "You have not clicked 'Save' button after the changes. Do you want to close?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            else this.Close();
        }

        private void itemBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < itemGridView.Rows.Count - 1; i++)
            {
                if (itemBox.SelectedIndex != -1)
                {
                    if (itemBox.SelectedItem.ToString().Trim() == itemGridView.Rows[i].Cells["Item#"].Value.ToString())
                    {
                        itemGridView.CurrentCell = itemGridView.Rows[i].Cells["Description"];
                        break;
                    }
                }
            }
        }

        private void qtyCheckBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (qtyCheckBtn.Checked)
            {
                screenText.Visible = false;
                screenText.Text = "";
                qtyText.Visible = true;
                signBox.Visible = true;
                option = "Quantity";
                signBox.DroppedDown = true;
            }
        }

        private void weightCheckBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (weightCheckBtn.Checked)
            {
                screenText.Visible = false;
                screenText.Text = "";
                qtyText.Visible = true;
                signBox.Visible = true;
                option = "Weight(LBS)";
                signBox.DroppedDown = true;
            }
        }

        private void decpCheckBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (decpCheckBtn.Checked)
            {
                screenText.Visible = true;
                qtyText.Visible = false;
                qtyText.Text = "";
                signBox.Visible = false;
                signBox.SelectedIndex = -1;
                option = "Description";
            }
        }

        private void numCheckBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (numCheckBtn.Checked)
            {
                screenText.Visible = true;
                qtyText.Visible = false;
                qtyText.Text = "";
                signBox.Visible = false;
                signBox.SelectedIndex = -1;
                option = "Item#";
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            String[] sArray = Regex.Split(screenText.Text," ",RegexOptions.IgnoreCase);
            if (numCheckBtn.Checked || decpCheckBtn.Checked)
            {
                itemBox.Items.Clear();
                for (int i=0; i < itemGridView.Rows.Count-1; i++)
                {
                    String str = itemGridView.Rows[i].Cells[option].Value.ToString().ToLower();
                    if (itemGridView.Rows[i].Visible != false)
                    {
                        int count=0;
                        for (int k = 0; k < sArray.Length; k++)
                        {
                            sArray[k] = sArray[k].ToLower();
                            if (str.IndexOf(sArray[k]) != -1)
                                count++;
                            else break;
                            if (count == sArray.Length)
                                itemBox.Items.Add(itemGridView.Rows[i].Cells["Item#"].Value.ToString());
                        }
                     }
                                
                }
            }   
            if (qtyCheckBtn.Checked || weightCheckBtn.Checked)
            {
                if (signBox.SelectedIndex == -1)
                    MessageBox.Show("Option is unselected!");
                else if (qtyText.Text == "")
                    MessageBox.Show("Quantity is empty!");
                else
                {
                    itemBox.Items.Clear();
                    for (int i = 0; i < itemGridView.Rows.Count - 1; i++)
                    {
                        Double num = Convert.ToDouble(itemGridView.Rows[i].Cells[option].Value);
                        Double qty =Convert.ToDouble(quantity);
                        if(signBox.SelectedIndex == 0 && qty > num && itemGridView.Rows[i].Visible != false)
                            itemBox.Items.Add(itemGridView.Rows[i].Cells["Item#"].Value.ToString());
                        if (signBox.SelectedIndex == 1 && qty < num && itemGridView.Rows[i].Visible != false)
                            itemBox.Items.Add(itemGridView.Rows[i].Cells["Item#"].Value.ToString());
                        if (signBox.SelectedIndex == 2 && qty == num && itemGridView.Rows[i].Visible != false)
                            itemBox.Items.Add(itemGridView.Rows[i].Cells["Item#"].Value.ToString());
                    }
                }
                
            }
            itemBox.DroppedDown = true;
        }

        private void qtyText_TextChanged(object sender, EventArgs e)
        {
            Boolean valid = true;
            quantity=qtyText.Text;
            if (quantity.Length > 7)
            {
                qtyText.Text = quantity.Remove(quantity.Length - 1);
                MessageBox.Show("The length of quantity should be less than 8.");
            }
            foreach (char c in quantity)
            {
                if (!(c >= '0' && c <= '9' || c =='.'))
                {
                    valid = false;
                    quantity= quantity.Remove(quantity.Length - 1);
                }
            }
            if (!valid)
            {
                qtyText.Text = quantity;
                MessageBox.Show("Quantity should be composed by digits or '.'");
            }
        }

        private void screenBtn_Click(object sender, EventArgs e)
        {
            itemBox.SelectedIndex = -1;
            defaultList();
            if (numCheckBtn.Checked || decpCheckBtn.Checked)
            {
                String[] sArray = Regex.Split(screenText.Text, " ", RegexOptions.IgnoreCase);
                for (int i = 0; i < itemGridView.Rows.Count - 1; i++)
                {
                    String str = itemGridView.Rows[i].Cells[option].Value.ToString().ToLower();
                    int count = 0;

                    for (int k = 0; k < sArray.Length; k++)
                    {
                        sArray[k] = sArray[k].ToLower();
                        if (str.IndexOf(sArray[k]) == -1)
                            break;
                        else
                            count++;
                    }
                    cm.SuspendBinding();
                    if(count<sArray.Length)
                        itemGridView.Rows[i].Visible = false;
                    cm.ResumeBinding();
                }
            }
            if (qtyCheckBtn.Checked || weightCheckBtn.Checked)
            {
                if (signBox.SelectedIndex == -1)
                    MessageBox.Show("Option is unselected!");
                else if (qtyText.Text == "")
                    MessageBox.Show("Quantity is empty!");
                else
                {
                    for (int i = 0; i < itemGridView.Rows.Count - 1; i++)
                    {
                        Double num = Convert.ToDouble(itemGridView.Rows[i].Cells[option].Value);
                        Double qty = Convert.ToDouble(quantity);
                        cm.SuspendBinding();
                        if (signBox.SelectedIndex == 0 && qty <= num)
                            itemGridView.Rows[i].Visible = false;
                        if (signBox.SelectedIndex == 1 && qty >= num)
                            itemGridView.Rows[i].Visible = false;
                        if (signBox.SelectedIndex == 2 && qty != num)
                            itemGridView.Rows[i].Visible = false;
                        cm.ResumeBinding();
                    }
                }
            }
            
            for (int i = 0; i < itemBox.Items.Count; i++)
            {
                for (int k = 0; k < itemGridView.Rows.Count - 1; k++ )
                {
                    //MessageBox.Show();
                    if (itemBox.Items[i].ToString().Trim() == itemGridView.Rows[k].Cells[1].Value.ToString().Trim())
                    {
                        if (itemGridView.Rows[Convert.ToInt16(itemGridView.Rows[k].Cells[0].Value.ToString().Trim()) - 100].Visible == false)
                        {
                            itemBox.Items.Remove(itemBox.Items[i]);
                            i--;
                            break;
                        }
                    }
                }
            }
        }

        private void defalutBtn_Click(object sender, EventArgs e)
        {
            defaultList();
        }

        private void defaultList()
        {
            for (int i = 0; i < itemGridView.Rows.Count - 1; i++)
                itemGridView.Rows[i].Visible = true;
            itemBox.Items.Clear();

            SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            itemDataSet = conn1.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item;");
            for (int i = 0; i < itemGridView.Rows.Count - 1; i++)
                itemBox.Items.Add(itemGridView.Rows[i].Cells["Item#"].Value.ToString().Trim());
        }

        private void screenText_TextChanged(object sender, EventArgs e)
        {
            String str = screenText.Text;
            if (str.Length > 19)
            {
                screenText.Text = str.Remove(str.Length - 1);
                MessageBox.Show("The length of text should be less than 20.");
            }
        }

        /*
        private void itemBox_KeyDown(object sender, KeyEventArgs e)
        {
            itemBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            if (itemBox.Text != "")
                itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item where charindex('" + itemBox.Text + "',[item])!=0");
            else
                itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item");
         
                if (itemDataSet.Tables[0].Rows.Count != 0)
                {
                    DataTable dt = itemDataSet.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                        itemBox.Items.Add(dt.Rows[i][0].ToString().Trim());
                }
                else
                    itemBox.Items.Add("(Null)");
                itemBox.DroppedDown = true;
            }
        */
        /*
        private void itemBox_DropDown(object sender, EventArgs e)
        {
            itemBox.Items.Clear();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            if (itemBox.Text != "")
                itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item where charindex('" + itemBox.Text + "',[item])!=0");
             else
                itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item");
         
                if (itemDataSet.Tables[0].Rows.Count != 0)
                {
                    DataTable dt = itemDataSet.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                        itemBox.Items.Add(dt.Rows[i][0].ToString().Trim());
                }
                else
                    itemBox.Items.Add("(Null)");
            }
        */
        /*
        private void itemBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                input = itemBox.Text;
                itemBox.Items.Clear();
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                if (input != "")
                {
                    itemDataSet = conn.getDataSet("use " + databaseName + " select [item],description, quantity, weight from item where charindex('" + input + "',[item])!=0");

                    if (itemDataSet.Tables[0].Rows.Count != 0)
                    {
                        DataTable dt = itemDataSet.Tables[0];
                        for (int i = 0; i < dt.Rows.Count; i++)
                            itemBox.Items.Add(dt.Rows[i][0].ToString().Trim());
                    }
                    else
                        itemBox.Items.Add("(Null)");
                }
            }

            itemBox.DroppedDown = true;
        }
      */
    }
}
