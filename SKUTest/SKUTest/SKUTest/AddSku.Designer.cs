﻿namespace SKUTest
{
    partial class AddSku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.skuDescriptionTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.addSkuDataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.addItemDataGridView = new System.Windows.Forms.DataGridView();
            this.changeBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.quantityNumber = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.skuBox = new System.Windows.Forms.ComboBox();
            this.okBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addSkuDataGridView)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addItemDataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumber)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.skuDescriptionTextBox);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.changeBtn);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.okBtn);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(581, 464);
            this.panel1.TabIndex = 1;
            // 
            // skuDescriptionTextBox
            // 
            this.skuDescriptionTextBox.Location = new System.Drawing.Point(9, 59);
            this.skuDescriptionTextBox.Name = "skuDescriptionTextBox";
            this.skuDescriptionTextBox.ReadOnly = true;
            this.skuDescriptionTextBox.Size = new System.Drawing.Size(563, 49);
            this.skuDescriptionTextBox.TabIndex = 11;
            this.skuDescriptionTextBox.TabStop = false;
            this.skuDescriptionTextBox.Text = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.addSkuDataGridView);
            this.groupBox4.Location = new System.Drawing.Point(12, 115);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(563, 150);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Edited SKUs";
            // 
            // addSkuDataGridView
            // 
            this.addSkuDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.addSkuDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addSkuDataGridView.Location = new System.Drawing.Point(3, 17);
            this.addSkuDataGridView.Name = "addSkuDataGridView";
            this.addSkuDataGridView.RowTemplate.Height = 23;
            this.addSkuDataGridView.Size = new System.Drawing.Size(557, 130);
            this.addSkuDataGridView.TabIndex = 5;
            this.addSkuDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.addSkuDataGridView_CellClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.addItemDataGridView);
            this.groupBox3.Location = new System.Drawing.Point(9, 271);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(569, 190);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Included Items";
            // 
            // addItemDataGridView
            // 
            this.addItemDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.addItemDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addItemDataGridView.Location = new System.Drawing.Point(3, 17);
            this.addItemDataGridView.Name = "addItemDataGridView";
            this.addItemDataGridView.RowTemplate.Height = 23;
            this.addItemDataGridView.Size = new System.Drawing.Size(563, 170);
            this.addItemDataGridView.TabIndex = 6;
            // 
            // changeBtn
            // 
            this.changeBtn.Location = new System.Drawing.Point(281, 19);
            this.changeBtn.Name = "changeBtn";
            this.changeBtn.Size = new System.Drawing.Size(75, 23);
            this.changeBtn.TabIndex = 2;
            this.changeBtn.Text = "Add";
            this.changeBtn.UseVisualStyleBackColor = true;
            this.changeBtn.Click += new System.EventHandler(this.changeBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(494, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.quantityNumber);
            this.groupBox2.Location = new System.Drawing.Point(146, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(129, 47);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Quantity(-100~100)";
            // 
            // quantityNumber
            // 
            this.quantityNumber.Location = new System.Drawing.Point(6, 16);
            this.quantityNumber.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.quantityNumber.Name = "quantityNumber";
            this.quantityNumber.Size = new System.Drawing.Size(75, 21);
            this.quantityNumber.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.skuBox);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(137, 50);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SKU";
            // 
            // skuBox
            // 
            this.skuBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.skuBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.skuBox.FormattingEnabled = true;
            this.skuBox.Location = new System.Drawing.Point(6, 20);
            this.skuBox.Name = "skuBox";
            this.skuBox.Size = new System.Drawing.Size(121, 20);
            this.skuBox.TabIndex = 0;
            this.skuBox.SelectedIndexChanged += new System.EventHandler(this.skuBox_SelectedIndexChanged);
            // 
            // okBtn
            // 
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(413, 19);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 3;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // AddSku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 464);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "AddSku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add SKUs";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddSku_FormClosing);
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.addSkuDataGridView)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.addItemDataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumber)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox skuBox;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown quantityNumber;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button changeBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView addSkuDataGridView;
        private System.Windows.Forms.DataGridView addItemDataGridView;
        private System.Windows.Forms.RichTextBox skuDescriptionTextBox;
    }
}