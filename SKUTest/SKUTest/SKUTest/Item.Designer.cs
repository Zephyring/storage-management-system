﻿namespace SKUTest
{
    partial class Item
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.itemGridView = new System.Windows.Forms.DataGridView();
            this.addBtn = new System.Windows.Forms.Button();
            this.itemBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.defalutBtn = new System.Windows.Forms.Button();
            this.weightCheckBtn = new System.Windows.Forms.RadioButton();
            this.screenBtn = new System.Windows.Forms.Button();
            this.qtyText = new System.Windows.Forms.TextBox();
            this.signBox = new System.Windows.Forms.ComboBox();
            this.qtyCheckBtn = new System.Windows.Forms.RadioButton();
            this.searchBtn = new System.Windows.Forms.Button();
            this.decpCheckBtn = new System.Windows.Forms.RadioButton();
            this.numCheckBtn = new System.Windows.Forms.RadioButton();
            this.screenText = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.itemGridView);
            this.groupBox1.Location = new System.Drawing.Point(12, 100);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(746, 256);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Items List";
            // 
            // itemGridView
            // 
            this.itemGridView.AllowUserToResizeColumns = false;
            this.itemGridView.AllowUserToResizeRows = false;
            this.itemGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemGridView.Location = new System.Drawing.Point(3, 17);
            this.itemGridView.Name = "itemGridView";
            this.itemGridView.RowTemplate.Height = 23;
            this.itemGridView.Size = new System.Drawing.Size(740, 236);
            this.itemGridView.TabIndex = 0;
            this.itemGridView.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.itemGridView_CellBeginEdit);
            this.itemGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemGridView_CellEndEdit);
            this.itemGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemGridView_CellEnter);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(509, 35);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(87, 47);
            this.addBtn.TabIndex = 3;
            this.addBtn.Text = "Add New Item";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // itemBox
            // 
            this.itemBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.itemBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.itemBox.DropDownHeight = 140;
            this.itemBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.itemBox.FormattingEnabled = true;
            this.itemBox.IntegralHeight = false;
            this.itemBox.Location = new System.Drawing.Point(231, 16);
            this.itemBox.Name = "itemBox";
            this.itemBox.Size = new System.Drawing.Size(113, 20);
            this.itemBox.TabIndex = 5;
            this.itemBox.SelectedIndexChanged += new System.EventHandler(this.itemBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.defalutBtn);
            this.groupBox2.Controls.Add(this.itemBox);
            this.groupBox2.Controls.Add(this.weightCheckBtn);
            this.groupBox2.Controls.Add(this.screenBtn);
            this.groupBox2.Controls.Add(this.qtyText);
            this.groupBox2.Controls.Add(this.signBox);
            this.groupBox2.Controls.Add(this.qtyCheckBtn);
            this.groupBox2.Controls.Add(this.searchBtn);
            this.groupBox2.Controls.Add(this.decpCheckBtn);
            this.groupBox2.Controls.Add(this.numCheckBtn);
            this.groupBox2.Controls.Add(this.screenText);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(451, 82);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search / List Screen";
            // 
            // defalutBtn
            // 
            this.defalutBtn.Location = new System.Drawing.Point(370, 47);
            this.defalutBtn.Name = "defalutBtn";
            this.defalutBtn.Size = new System.Drawing.Size(75, 23);
            this.defalutBtn.TabIndex = 6;
            this.defalutBtn.Text = "Default";
            this.defalutBtn.UseVisualStyleBackColor = true;
            this.defalutBtn.Click += new System.EventHandler(this.defalutBtn_Click);
            // 
            // weightCheckBtn
            // 
            this.weightCheckBtn.AutoSize = true;
            this.weightCheckBtn.Location = new System.Drawing.Point(264, 48);
            this.weightCheckBtn.Name = "weightCheckBtn";
            this.weightCheckBtn.Size = new System.Drawing.Size(59, 16);
            this.weightCheckBtn.TabIndex = 10;
            this.weightCheckBtn.Text = "Weight";
            this.weightCheckBtn.UseVisualStyleBackColor = true;
            this.weightCheckBtn.CheckedChanged += new System.EventHandler(this.weightCheckBtn_CheckedChanged);
            // 
            // screenBtn
            // 
            this.screenBtn.Location = new System.Drawing.Point(370, 15);
            this.screenBtn.Name = "screenBtn";
            this.screenBtn.Size = new System.Drawing.Size(75, 23);
            this.screenBtn.TabIndex = 7;
            this.screenBtn.Text = "Screen";
            this.screenBtn.UseVisualStyleBackColor = true;
            this.screenBtn.Click += new System.EventHandler(this.screenBtn_Click);
            // 
            // qtyText
            // 
            this.qtyText.Location = new System.Drawing.Point(77, 17);
            this.qtyText.Name = "qtyText";
            this.qtyText.Size = new System.Drawing.Size(74, 21);
            this.qtyText.TabIndex = 9;
            this.qtyText.Visible = false;
            this.qtyText.TextChanged += new System.EventHandler(this.qtyText_TextChanged);
            // 
            // signBox
            // 
            this.signBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.signBox.FormattingEnabled = true;
            this.signBox.Items.AddRange(new object[] {
            "  <",
            "  >",
            "  ="});
            this.signBox.Location = new System.Drawing.Point(7, 18);
            this.signBox.Name = "signBox";
            this.signBox.Size = new System.Drawing.Size(59, 20);
            this.signBox.TabIndex = 8;
            this.signBox.Visible = false;
            // 
            // qtyCheckBtn
            // 
            this.qtyCheckBtn.AutoSize = true;
            this.qtyCheckBtn.Location = new System.Drawing.Point(157, 48);
            this.qtyCheckBtn.Name = "qtyCheckBtn";
            this.qtyCheckBtn.Size = new System.Drawing.Size(101, 16);
            this.qtyCheckBtn.TabIndex = 7;
            this.qtyCheckBtn.TabStop = true;
            this.qtyCheckBtn.Text = "Rest Quantity";
            this.qtyCheckBtn.UseVisualStyleBackColor = true;
            this.qtyCheckBtn.CheckedChanged += new System.EventHandler(this.qtyCheckBtn_CheckedChanged);
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(157, 15);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(68, 23);
            this.searchBtn.TabIndex = 6;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // decpCheckBtn
            // 
            this.decpCheckBtn.AutoSize = true;
            this.decpCheckBtn.Location = new System.Drawing.Point(62, 48);
            this.decpCheckBtn.Name = "decpCheckBtn";
            this.decpCheckBtn.Size = new System.Drawing.Size(89, 16);
            this.decpCheckBtn.TabIndex = 2;
            this.decpCheckBtn.TabStop = true;
            this.decpCheckBtn.Text = "Description";
            this.decpCheckBtn.UseVisualStyleBackColor = true;
            this.decpCheckBtn.CheckedChanged += new System.EventHandler(this.decpCheckBtn_CheckedChanged);
            // 
            // numCheckBtn
            // 
            this.numCheckBtn.AutoSize = true;
            this.numCheckBtn.Checked = true;
            this.numCheckBtn.Location = new System.Drawing.Point(7, 48);
            this.numCheckBtn.Name = "numCheckBtn";
            this.numCheckBtn.Size = new System.Drawing.Size(53, 16);
            this.numCheckBtn.TabIndex = 1;
            this.numCheckBtn.TabStop = true;
            this.numCheckBtn.Text = "Item#";
            this.numCheckBtn.UseVisualStyleBackColor = true;
            this.numCheckBtn.CheckedChanged += new System.EventHandler(this.numCheckBtn_CheckedChanged);
            // 
            // screenText
            // 
            this.screenText.Location = new System.Drawing.Point(7, 17);
            this.screenText.Name = "screenText";
            this.screenText.Size = new System.Drawing.Size(144, 21);
            this.screenText.TabIndex = 0;
            this.screenText.TextChanged += new System.EventHandler(this.screenText_TextChanged);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(602, 35);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 47);
            this.saveBtn.TabIndex = 6;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(683, 35);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 47);
            this.button1.TabIndex = 7;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 368);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Item";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Items";
            this.Load += new System.EventHandler(this.Item_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView itemGridView;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.ComboBox itemBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton decpCheckBtn;
        private System.Windows.Forms.RadioButton numCheckBtn;
        private System.Windows.Forms.TextBox screenText;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.RadioButton qtyCheckBtn;
        private System.Windows.Forms.ComboBox signBox;
        private System.Windows.Forms.RadioButton weightCheckBtn;
        private System.Windows.Forms.Button defalutBtn;
        private System.Windows.Forms.Button screenBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox qtyText;
    }
}