﻿namespace SKUTest
{
    partial class MainFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrame));
            this.dataMaintenance = new System.Windows.Forms.GroupBox();
            this.skuBtn = new System.Windows.Forms.Button();
            this.logBtn = new System.Windows.Forms.Button();
            this.reportBtn = new System.Windows.Forms.Button();
            this.itemBtn = new System.Windows.Forms.Button();
            this.accountManagement = new System.Windows.Forms.GroupBox();
            this.modifyBtn = new System.Windows.Forms.Button();
            this.registerNewBtn = new System.Windows.Forms.Button();
            this.packingSlip = new System.Windows.Forms.GroupBox();
            this.bookSlipBtn = new System.Windows.Forms.Button();
            this.checkSlipBtn = new System.Windows.Forms.Button();
            this.display = new System.Windows.Forms.GroupBox();
            this.slipGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.allCheckBox = new System.Windows.Forms.CheckBox();
            this.shipCheckBox = new System.Windows.Forms.CheckBox();
            this.packCheckBox = new System.Windows.Forms.CheckBox();
            this.updateOrderBtn = new System.Windows.Forms.Button();
            this.slipOrderGridView = new System.Windows.Forms.DataGridView();
            this.filter = new System.Windows.Forms.GroupBox();
            this.historyCheckBox = new System.Windows.Forms.CheckBox();
            this.liveCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.slipSearchBtn = new System.Windows.Forms.Button();
            this.radioShipTime = new System.Windows.Forms.RadioButton();
            this.radioAddress = new System.Windows.Forms.RadioButton();
            this.radioCustomer = new System.Windows.Forms.RadioButton();
            this.radioSlipNum = new System.Windows.Forms.RadioButton();
            this.shipTimePicker = new System.Windows.Forms.DateTimePicker();
            this.slipText = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.addressText = new System.Windows.Forms.TextBox();
            this.timeText = new System.Windows.Forms.TextBox();
            this.slipCheckBtn = new System.Windows.Forms.Button();
            this.slipOrderBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.customerText = new System.Windows.Forms.TextBox();
            this.skuGroupBox = new System.Windows.Forms.GroupBox();
            this.skuDescBtn = new System.Windows.Forms.RadioButton();
            this.skuNameBtn = new System.Windows.Forms.RadioButton();
            this.skuSearch = new System.Windows.Forms.Button();
            this.skuSearchText = new System.Windows.Forms.TextBox();
            this.skuDcpText = new System.Windows.Forms.TextBox();
            this.skuAddNewBtn = new System.Windows.Forms.Button();
            this.skuCheckBtn = new System.Windows.Forms.Button();
            this.skuBox = new System.Windows.Forms.ComboBox();
            this.calendarGroupBox = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.monthlyReportBtn = new System.Windows.Forms.Button();
            this.selectedDayReportBtn = new System.Windows.Forms.Button();
            this.dailyReportBtn = new System.Windows.Forms.Button();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.accountGroupBox = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.detailsGroupBox = new System.Windows.Forms.GroupBox();
            this.statusCheckBox = new System.Windows.Forms.CheckBox();
            this.revertAccButton = new System.Windows.Forms.Button();
            this.telephoneText = new System.Windows.Forms.TextBox();
            this.saveAccountBtn = new System.Windows.Forms.Button();
            this.nameText = new System.Windows.Forms.TextBox();
            this.passwordText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.accountBox = new System.Windows.Forms.ComboBox();
            this.registerGroup = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tipText = new System.Windows.Forms.TextBox();
            this.tipTitle = new System.Windows.Forms.TextBox();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.registerBtn = new System.Windows.Forms.Button();
            this.checkBtn = new System.Windows.Forms.Button();
            this.rgsTel = new System.Windows.Forms.TextBox();
            this.rgsName = new System.Windows.Forms.TextBox();
            this.confirmPwd = new System.Windows.Forms.TextBox();
            this.rgsPwd = new System.Windows.Forms.TextBox();
            this.rgsAcc = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.loginBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.roleBox = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.currentAccountRoleBox = new System.Windows.Forms.ComboBox();
            this.dataMaintenance.SuspendLayout();
            this.accountManagement.SuspendLayout();
            this.packingSlip.SuspendLayout();
            this.display.SuspendLayout();
            this.slipGroupBox.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slipOrderGridView)).BeginInit();
            this.filter.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.skuGroupBox.SuspendLayout();
            this.calendarGroupBox.SuspendLayout();
            this.accountGroupBox.SuspendLayout();
            this.detailsGroupBox.SuspendLayout();
            this.registerGroup.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataMaintenance
            // 
            this.dataMaintenance.Controls.Add(this.skuBtn);
            this.dataMaintenance.Controls.Add(this.logBtn);
            this.dataMaintenance.Controls.Add(this.reportBtn);
            this.dataMaintenance.Controls.Add(this.itemBtn);
            this.dataMaintenance.Enabled = false;
            this.dataMaintenance.Location = new System.Drawing.Point(12, 129);
            this.dataMaintenance.Name = "dataMaintenance";
            this.dataMaintenance.Size = new System.Drawing.Size(146, 221);
            this.dataMaintenance.TabIndex = 0;
            this.dataMaintenance.TabStop = false;
            this.dataMaintenance.Text = "Data Maintenance";
            // 
            // skuBtn
            // 
            this.skuBtn.Location = new System.Drawing.Point(5, 20);
            this.skuBtn.Name = "skuBtn";
            this.skuBtn.Size = new System.Drawing.Size(132, 40);
            this.skuBtn.TabIndex = 2;
            this.skuBtn.Text = "SKUs";
            this.skuBtn.UseVisualStyleBackColor = true;
            this.skuBtn.Click += new System.EventHandler(this.skuBtn_Click);
            // 
            // logBtn
            // 
            this.logBtn.Location = new System.Drawing.Point(5, 172);
            this.logBtn.Name = "logBtn";
            this.logBtn.Size = new System.Drawing.Size(132, 43);
            this.logBtn.TabIndex = 5;
            this.logBtn.Text = "Logs";
            this.logBtn.UseVisualStyleBackColor = true;
            this.logBtn.Click += new System.EventHandler(this.logBtn_Click);
            // 
            // reportBtn
            // 
            this.reportBtn.Location = new System.Drawing.Point(5, 121);
            this.reportBtn.Name = "reportBtn";
            this.reportBtn.Size = new System.Drawing.Size(132, 45);
            this.reportBtn.TabIndex = 4;
            this.reportBtn.Text = "Reports";
            this.reportBtn.UseVisualStyleBackColor = true;
            this.reportBtn.Click += new System.EventHandler(this.reportBtn_Click);
            // 
            // itemBtn
            // 
            this.itemBtn.Location = new System.Drawing.Point(5, 72);
            this.itemBtn.Name = "itemBtn";
            this.itemBtn.Size = new System.Drawing.Size(132, 43);
            this.itemBtn.TabIndex = 3;
            this.itemBtn.Text = "Items";
            this.itemBtn.UseVisualStyleBackColor = true;
            this.itemBtn.Click += new System.EventHandler(this.itemBtn_Click);
            // 
            // accountManagement
            // 
            this.accountManagement.Controls.Add(this.modifyBtn);
            this.accountManagement.Controls.Add(this.registerNewBtn);
            this.accountManagement.Enabled = false;
            this.accountManagement.Location = new System.Drawing.Point(12, 12);
            this.accountManagement.Name = "accountManagement";
            this.accountManagement.Size = new System.Drawing.Size(146, 111);
            this.accountManagement.TabIndex = 1;
            this.accountManagement.TabStop = false;
            this.accountManagement.Text = "User Management";
            // 
            // modifyBtn
            // 
            this.modifyBtn.Location = new System.Drawing.Point(6, 65);
            this.modifyBtn.Name = "modifyBtn";
            this.modifyBtn.Size = new System.Drawing.Size(132, 39);
            this.modifyBtn.TabIndex = 1;
            this.modifyBtn.Text = "Current Accounts";
            this.modifyBtn.UseVisualStyleBackColor = true;
            this.modifyBtn.Click += new System.EventHandler(this.modifyBtn_Click);
            // 
            // registerNewBtn
            // 
            this.registerNewBtn.Location = new System.Drawing.Point(6, 20);
            this.registerNewBtn.Name = "registerNewBtn";
            this.registerNewBtn.Size = new System.Drawing.Size(132, 39);
            this.registerNewBtn.TabIndex = 0;
            this.registerNewBtn.Text = "New Account";
            this.registerNewBtn.UseVisualStyleBackColor = true;
            this.registerNewBtn.Click += new System.EventHandler(this.registerNewBtn_Click);
            // 
            // packingSlip
            // 
            this.packingSlip.Controls.Add(this.bookSlipBtn);
            this.packingSlip.Controls.Add(this.checkSlipBtn);
            this.packingSlip.Enabled = false;
            this.packingSlip.Location = new System.Drawing.Point(11, 356);
            this.packingSlip.Name = "packingSlip";
            this.packingSlip.Size = new System.Drawing.Size(146, 122);
            this.packingSlip.TabIndex = 2;
            this.packingSlip.TabStop = false;
            this.packingSlip.Text = "Packing Slip";
            // 
            // bookSlipBtn
            // 
            this.bookSlipBtn.Location = new System.Drawing.Point(6, 78);
            this.bookSlipBtn.Name = "bookSlipBtn";
            this.bookSlipBtn.Size = new System.Drawing.Size(132, 38);
            this.bookSlipBtn.TabIndex = 8;
            this.bookSlipBtn.Text = "New Order";
            this.bookSlipBtn.UseVisualStyleBackColor = true;
            this.bookSlipBtn.Click += new System.EventHandler(this.bookSlipBtn_Click);
            // 
            // checkSlipBtn
            // 
            this.checkSlipBtn.Location = new System.Drawing.Point(6, 20);
            this.checkSlipBtn.Name = "checkSlipBtn";
            this.checkSlipBtn.Size = new System.Drawing.Size(132, 46);
            this.checkSlipBtn.TabIndex = 7;
            this.checkSlipBtn.Text = "Slip Orders";
            this.checkSlipBtn.UseVisualStyleBackColor = true;
            this.checkSlipBtn.Click += new System.EventHandler(this.checkSlipBtn_Click);
            // 
            // display
            // 
            this.display.Controls.Add(this.slipGroupBox);
            this.display.Controls.Add(this.skuGroupBox);
            this.display.Controls.Add(this.calendarGroupBox);
            this.display.Controls.Add(this.accountGroupBox);
            this.display.Controls.Add(this.registerGroup);
            this.display.Location = new System.Drawing.Point(164, 12);
            this.display.Name = "display";
            this.display.Size = new System.Drawing.Size(943, 573);
            this.display.TabIndex = 3;
            this.display.TabStop = false;
            this.display.Text = "Offline";
            // 
            // slipGroupBox
            // 
            this.slipGroupBox.Controls.Add(this.groupBox5);
            this.slipGroupBox.Controls.Add(this.updateOrderBtn);
            this.slipGroupBox.Controls.Add(this.slipOrderGridView);
            this.slipGroupBox.Controls.Add(this.filter);
            this.slipGroupBox.Controls.Add(this.groupBox4);
            this.slipGroupBox.Controls.Add(this.groupBox3);
            this.slipGroupBox.Location = new System.Drawing.Point(19, 35);
            this.slipGroupBox.Name = "slipGroupBox";
            this.slipGroupBox.Size = new System.Drawing.Size(902, 492);
            this.slipGroupBox.TabIndex = 12;
            this.slipGroupBox.TabStop = false;
            this.slipGroupBox.Text = "Packing Slip";
            this.slipGroupBox.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.allCheckBox);
            this.groupBox5.Controls.Add(this.shipCheckBox);
            this.groupBox5.Controls.Add(this.packCheckBox);
            this.groupBox5.Location = new System.Drawing.Point(534, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(153, 93);
            this.groupBox5.TabIndex = 26;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "State";
            // 
            // allCheckBox
            // 
            this.allCheckBox.AutoSize = true;
            this.allCheckBox.Checked = true;
            this.allCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.allCheckBox.Location = new System.Drawing.Point(7, 68);
            this.allCheckBox.Name = "allCheckBox";
            this.allCheckBox.Size = new System.Drawing.Size(42, 16);
            this.allCheckBox.TabIndex = 3;
            this.allCheckBox.Text = "All";
            this.allCheckBox.UseVisualStyleBackColor = true;
            this.allCheckBox.CheckedChanged += new System.EventHandler(this.allCheckBox_CheckedChanged);
            // 
            // shipCheckBox
            // 
            this.shipCheckBox.AutoSize = true;
            this.shipCheckBox.Enabled = false;
            this.shipCheckBox.Location = new System.Drawing.Point(6, 40);
            this.shipCheckBox.Name = "shipCheckBox";
            this.shipCheckBox.Size = new System.Drawing.Size(66, 16);
            this.shipCheckBox.TabIndex = 2;
            this.shipCheckBox.Text = "Shipped";
            this.shipCheckBox.UseVisualStyleBackColor = true;
            this.shipCheckBox.CheckedChanged += new System.EventHandler(this.shipCheckBox_CheckedChanged);
            // 
            // packCheckBox
            // 
            this.packCheckBox.AutoSize = true;
            this.packCheckBox.Enabled = false;
            this.packCheckBox.Location = new System.Drawing.Point(6, 18);
            this.packCheckBox.Name = "packCheckBox";
            this.packCheckBox.Size = new System.Drawing.Size(60, 16);
            this.packCheckBox.TabIndex = 1;
            this.packCheckBox.Text = "Packed";
            this.packCheckBox.UseVisualStyleBackColor = true;
            this.packCheckBox.CheckedChanged += new System.EventHandler(this.packCheckBox_CheckedChanged);
            // 
            // updateOrderBtn
            // 
            this.updateOrderBtn.Location = new System.Drawing.Point(754, 75);
            this.updateOrderBtn.Name = "updateOrderBtn";
            this.updateOrderBtn.Size = new System.Drawing.Size(122, 48);
            this.updateOrderBtn.TabIndex = 25;
            this.updateOrderBtn.Text = "Update Slip Orders";
            this.updateOrderBtn.UseVisualStyleBackColor = true;
            this.updateOrderBtn.Click += new System.EventHandler(this.updateOrderBtn_Click);
            // 
            // slipOrderGridView
            // 
            this.slipOrderGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.slipOrderGridView.Location = new System.Drawing.Point(8, 200);
            this.slipOrderGridView.Name = "slipOrderGridView";
            this.slipOrderGridView.RowTemplate.Height = 23;
            this.slipOrderGridView.Size = new System.Drawing.Size(921, 292);
            this.slipOrderGridView.TabIndex = 24;
            this.slipOrderGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.slipOrderGridView_CellClick);
            // 
            // filter
            // 
            this.filter.Controls.Add(this.historyCheckBox);
            this.filter.Controls.Add(this.liveCheckBox);
            this.filter.Location = new System.Drawing.Point(534, 114);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(153, 80);
            this.filter.TabIndex = 23;
            this.filter.TabStop = false;
            this.filter.Text = "Range";
            // 
            // historyCheckBox
            // 
            this.historyCheckBox.AutoSize = true;
            this.historyCheckBox.Location = new System.Drawing.Point(6, 47);
            this.historyCheckBox.Name = "historyCheckBox";
            this.historyCheckBox.Size = new System.Drawing.Size(66, 16);
            this.historyCheckBox.TabIndex = 1;
            this.historyCheckBox.Text = "History";
            this.historyCheckBox.UseVisualStyleBackColor = true;
            this.historyCheckBox.CheckedChanged += new System.EventHandler(this.historyCheckBox_CheckedChanged);
            // 
            // liveCheckBox
            // 
            this.liveCheckBox.AutoSize = true;
            this.liveCheckBox.Checked = true;
            this.liveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.liveCheckBox.Location = new System.Drawing.Point(6, 23);
            this.liveCheckBox.Name = "liveCheckBox";
            this.liveCheckBox.Size = new System.Drawing.Size(60, 16);
            this.liveCheckBox.TabIndex = 0;
            this.liveCheckBox.Text = "Living";
            this.liveCheckBox.UseVisualStyleBackColor = true;
            this.liveCheckBox.CheckedChanged += new System.EventHandler(this.liveCheckBox_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.slipSearchBtn);
            this.groupBox4.Controls.Add(this.radioShipTime);
            this.groupBox4.Controls.Add(this.radioAddress);
            this.groupBox4.Controls.Add(this.radioCustomer);
            this.groupBox4.Controls.Add(this.radioSlipNum);
            this.groupBox4.Controls.Add(this.shipTimePicker);
            this.groupBox4.Controls.Add(this.slipText);
            this.groupBox4.Location = new System.Drawing.Point(8, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(520, 64);
            this.groupBox4.TabIndex = 22;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search";
            // 
            // slipSearchBtn
            // 
            this.slipSearchBtn.Location = new System.Drawing.Point(360, 18);
            this.slipSearchBtn.Name = "slipSearchBtn";
            this.slipSearchBtn.Size = new System.Drawing.Size(148, 37);
            this.slipSearchBtn.TabIndex = 26;
            this.slipSearchBtn.Text = "Search";
            this.slipSearchBtn.UseVisualStyleBackColor = true;
            this.slipSearchBtn.Click += new System.EventHandler(this.slipSearchBtn_Click);
            // 
            // radioShipTime
            // 
            this.radioShipTime.AutoSize = true;
            this.radioShipTime.Location = new System.Drawing.Point(276, 46);
            this.radioShipTime.Name = "radioShipTime";
            this.radioShipTime.Size = new System.Drawing.Size(77, 16);
            this.radioShipTime.TabIndex = 25;
            this.radioShipTime.TabStop = true;
            this.radioShipTime.Text = "Ship Time";
            this.radioShipTime.UseVisualStyleBackColor = true;
            this.radioShipTime.CheckedChanged += new System.EventHandler(this.radioShipTime_CheckedChanged);
            // 
            // radioAddress
            // 
            this.radioAddress.AutoSize = true;
            this.radioAddress.Location = new System.Drawing.Point(209, 46);
            this.radioAddress.Name = "radioAddress";
            this.radioAddress.Size = new System.Drawing.Size(65, 16);
            this.radioAddress.TabIndex = 24;
            this.radioAddress.Text = "Address";
            this.radioAddress.UseVisualStyleBackColor = true;
            this.radioAddress.CheckedChanged += new System.EventHandler(this.radioAddress_CheckedChanged);
            // 
            // radioCustomer
            // 
            this.radioCustomer.AutoSize = true;
            this.radioCustomer.Location = new System.Drawing.Point(104, 46);
            this.radioCustomer.Name = "radioCustomer";
            this.radioCustomer.Size = new System.Drawing.Size(101, 16);
            this.radioCustomer.TabIndex = 23;
            this.radioCustomer.Text = "Customer Name";
            this.radioCustomer.UseVisualStyleBackColor = true;
            this.radioCustomer.CheckedChanged += new System.EventHandler(this.radioCustomer_CheckedChanged);
            // 
            // radioSlipNum
            // 
            this.radioSlipNum.AutoSize = true;
            this.radioSlipNum.Checked = true;
            this.radioSlipNum.Location = new System.Drawing.Point(10, 46);
            this.radioSlipNum.Name = "radioSlipNum";
            this.radioSlipNum.Size = new System.Drawing.Size(89, 16);
            this.radioSlipNum.TabIndex = 22;
            this.radioSlipNum.TabStop = true;
            this.radioSlipNum.Text = "Slip Number";
            this.radioSlipNum.UseVisualStyleBackColor = true;
            this.radioSlipNum.CheckedChanged += new System.EventHandler(this.radioSlipNum_CheckedChanged);
            // 
            // shipTimePicker
            // 
            this.shipTimePicker.Location = new System.Drawing.Point(10, 20);
            this.shipTimePicker.Name = "shipTimePicker";
            this.shipTimePicker.Size = new System.Drawing.Size(344, 21);
            this.shipTimePicker.TabIndex = 27;
            this.shipTimePicker.Visible = false;
            this.shipTimePicker.ValueChanged += new System.EventHandler(this.shipTimePicker_ValueChanged);
            // 
            // slipText
            // 
            this.slipText.Location = new System.Drawing.Point(10, 20);
            this.slipText.Name = "slipText";
            this.slipText.Size = new System.Drawing.Size(344, 21);
            this.slipText.TabIndex = 21;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.addressText);
            this.groupBox3.Controls.Add(this.timeText);
            this.groupBox3.Controls.Add(this.slipCheckBtn);
            this.groupBox3.Controls.Add(this.slipOrderBox);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.customerText);
            this.groupBox3.Location = new System.Drawing.Point(8, 88);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(520, 106);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Information";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 20;
            this.label6.Text = "Slip Number";
            // 
            // addressText
            // 
            this.addressText.Location = new System.Drawing.Point(84, 73);
            this.addressText.Name = "addressText";
            this.addressText.ReadOnly = true;
            this.addressText.Size = new System.Drawing.Size(424, 21);
            this.addressText.TabIndex = 17;
            // 
            // timeText
            // 
            this.timeText.Location = new System.Drawing.Point(358, 44);
            this.timeText.Name = "timeText";
            this.timeText.ReadOnly = true;
            this.timeText.Size = new System.Drawing.Size(150, 21);
            this.timeText.TabIndex = 19;
            // 
            // slipCheckBtn
            // 
            this.slipCheckBtn.Enabled = false;
            this.slipCheckBtn.Location = new System.Drawing.Point(358, 17);
            this.slipCheckBtn.Name = "slipCheckBtn";
            this.slipCheckBtn.Size = new System.Drawing.Size(150, 21);
            this.slipCheckBtn.TabIndex = 11;
            this.slipCheckBtn.Text = "Check/Modify";
            this.slipCheckBtn.UseVisualStyleBackColor = true;
            this.slipCheckBtn.Click += new System.EventHandler(this.slipCheckBtn_Click);
            // 
            // slipOrderBox
            // 
            this.slipOrderBox.DropDownHeight = 120;
            this.slipOrderBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.slipOrderBox.FormattingEnabled = true;
            this.slipOrderBox.IntegralHeight = false;
            this.slipOrderBox.Location = new System.Drawing.Point(84, 20);
            this.slipOrderBox.Name = "slipOrderBox";
            this.slipOrderBox.Size = new System.Drawing.Size(204, 20);
            this.slipOrderBox.TabIndex = 0;
            this.slipOrderBox.SelectedIndexChanged += new System.EventHandler(this.slipOrderBox_SelectedIndexChanged);
            this.slipOrderBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.slipOrderBox_MouseClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "Customer";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 12);
            this.label10.TabIndex = 13;
            this.label10.Text = "Address";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(295, 53);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 18;
            this.label12.Text = "Ship Time";
            // 
            // customerText
            // 
            this.customerText.Location = new System.Drawing.Point(84, 46);
            this.customerText.Name = "customerText";
            this.customerText.ReadOnly = true;
            this.customerText.Size = new System.Drawing.Size(204, 21);
            this.customerText.TabIndex = 15;
            // 
            // skuGroupBox
            // 
            this.skuGroupBox.Controls.Add(this.skuDescBtn);
            this.skuGroupBox.Controls.Add(this.skuNameBtn);
            this.skuGroupBox.Controls.Add(this.skuSearch);
            this.skuGroupBox.Controls.Add(this.skuSearchText);
            this.skuGroupBox.Controls.Add(this.skuDcpText);
            this.skuGroupBox.Controls.Add(this.skuAddNewBtn);
            this.skuGroupBox.Controls.Add(this.skuCheckBtn);
            this.skuGroupBox.Controls.Add(this.skuBox);
            this.skuGroupBox.Location = new System.Drawing.Point(6, 20);
            this.skuGroupBox.Name = "skuGroupBox";
            this.skuGroupBox.Size = new System.Drawing.Size(535, 318);
            this.skuGroupBox.TabIndex = 3;
            this.skuGroupBox.TabStop = false;
            this.skuGroupBox.Text = "SKU";
            this.skuGroupBox.Visible = false;
            // 
            // skuDescBtn
            // 
            this.skuDescBtn.AutoSize = true;
            this.skuDescBtn.Location = new System.Drawing.Point(99, 42);
            this.skuDescBtn.Name = "skuDescBtn";
            this.skuDescBtn.Size = new System.Drawing.Size(89, 16);
            this.skuDescBtn.TabIndex = 17;
            this.skuDescBtn.Text = "Description";
            this.skuDescBtn.UseVisualStyleBackColor = true;
            // 
            // skuNameBtn
            // 
            this.skuNameBtn.AutoSize = true;
            this.skuNameBtn.Checked = true;
            this.skuNameBtn.Location = new System.Drawing.Point(37, 42);
            this.skuNameBtn.Name = "skuNameBtn";
            this.skuNameBtn.Size = new System.Drawing.Size(47, 16);
            this.skuNameBtn.TabIndex = 16;
            this.skuNameBtn.TabStop = true;
            this.skuNameBtn.Text = "Name";
            this.skuNameBtn.UseVisualStyleBackColor = true;
            // 
            // skuSearch
            // 
            this.skuSearch.Location = new System.Drawing.Point(226, 15);
            this.skuSearch.Name = "skuSearch";
            this.skuSearch.Size = new System.Drawing.Size(151, 43);
            this.skuSearch.TabIndex = 15;
            this.skuSearch.Text = "Search";
            this.skuSearch.UseVisualStyleBackColor = true;
            this.skuSearch.Click += new System.EventHandler(this.skuSearch_Click);
            // 
            // skuSearchText
            // 
            this.skuSearchText.Location = new System.Drawing.Point(10, 17);
            this.skuSearchText.Name = "skuSearchText";
            this.skuSearchText.Size = new System.Drawing.Size(208, 21);
            this.skuSearchText.TabIndex = 14;
            this.skuSearchText.TextChanged += new System.EventHandler(this.skuSearchText_TextChanged);
            // 
            // skuDcpText
            // 
            this.skuDcpText.Location = new System.Drawing.Point(9, 132);
            this.skuDcpText.Multiline = true;
            this.skuDcpText.Name = "skuDcpText";
            this.skuDcpText.ReadOnly = true;
            this.skuDcpText.Size = new System.Drawing.Size(367, 152);
            this.skuDcpText.TabIndex = 13;
            this.skuDcpText.Text = "Product Details...";
            // 
            // skuAddNewBtn
            // 
            this.skuAddNewBtn.Location = new System.Drawing.Point(383, 15);
            this.skuAddNewBtn.Name = "skuAddNewBtn";
            this.skuAddNewBtn.Size = new System.Drawing.Size(136, 43);
            this.skuAddNewBtn.TabIndex = 12;
            this.skuAddNewBtn.Text = "Add New";
            this.skuAddNewBtn.UseVisualStyleBackColor = true;
            this.skuAddNewBtn.Click += new System.EventHandler(this.skuAddNewBtn_Click);
            // 
            // skuCheckBtn
            // 
            this.skuCheckBtn.Enabled = false;
            this.skuCheckBtn.Location = new System.Drawing.Point(227, 69);
            this.skuCheckBtn.Name = "skuCheckBtn";
            this.skuCheckBtn.Size = new System.Drawing.Size(150, 41);
            this.skuCheckBtn.TabIndex = 11;
            this.skuCheckBtn.Text = "Check/Modify";
            this.skuCheckBtn.UseVisualStyleBackColor = true;
            this.skuCheckBtn.Click += new System.EventHandler(this.skuCheckBtn_Click);
            // 
            // skuBox
            // 
            this.skuBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.skuBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.skuBox.DropDownHeight = 120;
            this.skuBox.FormattingEnabled = true;
            this.skuBox.IntegralHeight = false;
            this.skuBox.Location = new System.Drawing.Point(9, 82);
            this.skuBox.Name = "skuBox";
            this.skuBox.Size = new System.Drawing.Size(209, 20);
            this.skuBox.TabIndex = 0;
            this.skuBox.SelectedIndexChanged += new System.EventHandler(this.skuBox_SelectedIndexChanged);
            // 
            // calendarGroupBox
            // 
            this.calendarGroupBox.Controls.Add(this.richTextBox1);
            this.calendarGroupBox.Controls.Add(this.monthlyReportBtn);
            this.calendarGroupBox.Controls.Add(this.selectedDayReportBtn);
            this.calendarGroupBox.Controls.Add(this.dailyReportBtn);
            this.calendarGroupBox.Controls.Add(this.calendar);
            this.calendarGroupBox.Location = new System.Drawing.Point(6, 71);
            this.calendarGroupBox.Name = "calendarGroupBox";
            this.calendarGroupBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.calendarGroupBox.Size = new System.Drawing.Size(856, 449);
            this.calendarGroupBox.TabIndex = 15;
            this.calendarGroupBox.TabStop = false;
            this.calendarGroupBox.Text = "Report";
            this.calendarGroupBox.Visible = false;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(10, 20);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(185, 521);
            this.richTextBox1.TabIndex = 9;
            this.richTextBox1.TabStop = false;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // monthlyReportBtn
            // 
            this.monthlyReportBtn.Location = new System.Drawing.Point(267, 381);
            this.monthlyReportBtn.Name = "monthlyReportBtn";
            this.monthlyReportBtn.Size = new System.Drawing.Size(132, 59);
            this.monthlyReportBtn.TabIndex = 8;
            this.monthlyReportBtn.Text = "Monthly Report";
            this.monthlyReportBtn.UseVisualStyleBackColor = true;
            this.monthlyReportBtn.Click += new System.EventHandler(this.monthlyReportBtn_Click);
            // 
            // selectedDayReportBtn
            // 
            this.selectedDayReportBtn.Location = new System.Drawing.Point(267, 306);
            this.selectedDayReportBtn.Name = "selectedDayReportBtn";
            this.selectedDayReportBtn.Size = new System.Drawing.Size(132, 59);
            this.selectedDayReportBtn.TabIndex = 7;
            this.selectedDayReportBtn.Text = "Weekly Report";
            this.selectedDayReportBtn.UseVisualStyleBackColor = true;
            this.selectedDayReportBtn.Click += new System.EventHandler(this.selectedDayReportBtn_Click);
            // 
            // dailyReportBtn
            // 
            this.dailyReportBtn.Location = new System.Drawing.Point(267, 229);
            this.dailyReportBtn.Name = "dailyReportBtn";
            this.dailyReportBtn.Size = new System.Drawing.Size(132, 59);
            this.dailyReportBtn.TabIndex = 6;
            this.dailyReportBtn.Text = " Daily Report";
            this.dailyReportBtn.UseVisualStyleBackColor = true;
            this.dailyReportBtn.Click += new System.EventHandler(this.dailyReportBtn_Click);
            // 
            // calendar
            // 
            this.calendar.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.calendar.Location = new System.Drawing.Point(224, 20);
            this.calendar.MaxSelectionCount = 1;
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 0;
            this.calendar.TodayDate = new System.DateTime(2011, 7, 29, 0, 0, 0, 0);
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateChanged);
            this.calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.calendar_DateSelected);
            // 
            // accountGroupBox
            // 
            this.accountGroupBox.Controls.Add(this.label5);
            this.accountGroupBox.Controls.Add(this.detailsGroupBox);
            this.accountGroupBox.Controls.Add(this.accountBox);
            this.accountGroupBox.Location = new System.Drawing.Point(48, 35);
            this.accountGroupBox.Name = "accountGroupBox";
            this.accountGroupBox.Size = new System.Drawing.Size(576, 392);
            this.accountGroupBox.TabIndex = 16;
            this.accountGroupBox.TabStop = false;
            this.accountGroupBox.Text = "Account";
            this.accountGroupBox.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "Choose one to modify";
            // 
            // detailsGroupBox
            // 
            this.detailsGroupBox.Controls.Add(this.groupBox7);
            this.detailsGroupBox.Controls.Add(this.statusCheckBox);
            this.detailsGroupBox.Controls.Add(this.revertAccButton);
            this.detailsGroupBox.Controls.Add(this.telephoneText);
            this.detailsGroupBox.Controls.Add(this.saveAccountBtn);
            this.detailsGroupBox.Controls.Add(this.nameText);
            this.detailsGroupBox.Controls.Add(this.passwordText);
            this.detailsGroupBox.Controls.Add(this.label3);
            this.detailsGroupBox.Controls.Add(this.label4);
            this.detailsGroupBox.Controls.Add(this.label1);
            this.detailsGroupBox.Controls.Add(this.label2);
            this.detailsGroupBox.Location = new System.Drawing.Point(146, 10);
            this.detailsGroupBox.Name = "detailsGroupBox";
            this.detailsGroupBox.Size = new System.Drawing.Size(411, 365);
            this.detailsGroupBox.TabIndex = 2;
            this.detailsGroupBox.TabStop = false;
            this.detailsGroupBox.Text = "Details";
            this.detailsGroupBox.Visible = false;
            // 
            // statusCheckBox
            // 
            this.statusCheckBox.AutoSize = true;
            this.statusCheckBox.Checked = true;
            this.statusCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusCheckBox.Location = new System.Drawing.Point(80, 218);
            this.statusCheckBox.Name = "statusCheckBox";
            this.statusCheckBox.Size = new System.Drawing.Size(84, 16);
            this.statusCheckBox.TabIndex = 10;
            this.statusCheckBox.Text = "Activating";
            this.statusCheckBox.UseVisualStyleBackColor = true;
            // 
            // revertAccButton
            // 
            this.revertAccButton.Location = new System.Drawing.Point(222, 286);
            this.revertAccButton.Name = "revertAccButton";
            this.revertAccButton.Size = new System.Drawing.Size(85, 63);
            this.revertAccButton.TabIndex = 4;
            this.revertAccButton.Text = "Revert";
            this.revertAccButton.UseVisualStyleBackColor = true;
            this.revertAccButton.Click += new System.EventHandler(this.revertAccButton_Click);
            // 
            // telephoneText
            // 
            this.telephoneText.Location = new System.Drawing.Point(80, 156);
            this.telephoneText.Name = "telephoneText";
            this.telephoneText.Size = new System.Drawing.Size(257, 21);
            this.telephoneText.TabIndex = 9;
            this.telephoneText.TextChanged += new System.EventHandler(this.telephoneText_TextChanged);
            // 
            // saveAccountBtn
            // 
            this.saveAccountBtn.Location = new System.Drawing.Point(65, 286);
            this.saveAccountBtn.Name = "saveAccountBtn";
            this.saveAccountBtn.Size = new System.Drawing.Size(85, 63);
            this.saveAccountBtn.TabIndex = 3;
            this.saveAccountBtn.Text = "Save";
            this.saveAccountBtn.UseVisualStyleBackColor = true;
            this.saveAccountBtn.Click += new System.EventHandler(this.saveAccountBtn_Click);
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(80, 95);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(257, 21);
            this.nameText.TabIndex = 8;
            this.nameText.TextChanged += new System.EventHandler(this.nameText_TextChanged);
            // 
            // passwordText
            // 
            this.passwordText.Location = new System.Drawing.Point(80, 31);
            this.passwordText.Name = "passwordText";
            this.passwordText.Size = new System.Drawing.Size(257, 21);
            this.passwordText.TabIndex = 5;
            this.passwordText.UseSystemPasswordChar = true;
            this.passwordText.TextChanged += new System.EventHandler(this.passwordText_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "Telephone";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "Status";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // accountBox
            // 
            this.accountBox.FormattingEnabled = true;
            this.accountBox.Items.AddRange(new object[] {
            "admin",
            "UserA",
            "UserB"});
            this.accountBox.Location = new System.Drawing.Point(6, 19);
            this.accountBox.Name = "accountBox";
            this.accountBox.Size = new System.Drawing.Size(134, 20);
            this.accountBox.TabIndex = 0;
            this.accountBox.SelectedIndexChanged += new System.EventHandler(this.accountBox_SelectedIndexChanged);
            this.accountBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.accountBox_MouseClick);
            // 
            // registerGroup
            // 
            this.registerGroup.Controls.Add(this.groupBox6);
            this.registerGroup.Controls.Add(this.groupBox2);
            this.registerGroup.Controls.Add(this.cancelBtn);
            this.registerGroup.Controls.Add(this.registerBtn);
            this.registerGroup.Controls.Add(this.checkBtn);
            this.registerGroup.Controls.Add(this.rgsTel);
            this.registerGroup.Controls.Add(this.rgsName);
            this.registerGroup.Controls.Add(this.confirmPwd);
            this.registerGroup.Controls.Add(this.rgsPwd);
            this.registerGroup.Controls.Add(this.rgsAcc);
            this.registerGroup.Controls.Add(this.label13);
            this.registerGroup.Controls.Add(this.label14);
            this.registerGroup.Controls.Add(this.label15);
            this.registerGroup.Controls.Add(this.label16);
            this.registerGroup.Controls.Add(this.label17);
            this.registerGroup.Location = new System.Drawing.Point(111, 20);
            this.registerGroup.Name = "registerGroup";
            this.registerGroup.Size = new System.Drawing.Size(601, 440);
            this.registerGroup.TabIndex = 16;
            this.registerGroup.TabStop = false;
            this.registerGroup.Text = "Register";
            this.registerGroup.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tipText);
            this.groupBox2.Controls.Add(this.tipTitle);
            this.groupBox2.Location = new System.Drawing.Point(325, 89);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 225);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Notice";
            // 
            // tipText
            // 
            this.tipText.Location = new System.Drawing.Point(6, 61);
            this.tipText.Multiline = true;
            this.tipText.Name = "tipText";
            this.tipText.ReadOnly = true;
            this.tipText.Size = new System.Drawing.Size(182, 104);
            this.tipText.TabIndex = 15;
            // 
            // tipTitle
            // 
            this.tipTitle.Location = new System.Drawing.Point(6, 34);
            this.tipTitle.Name = "tipTitle";
            this.tipTitle.ReadOnly = true;
            this.tipTitle.Size = new System.Drawing.Size(110, 21);
            this.tipTitle.TabIndex = 14;
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(187, 364);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 59);
            this.cancelBtn.TabIndex = 22;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // registerBtn
            // 
            this.registerBtn.Location = new System.Drawing.Point(64, 364);
            this.registerBtn.Name = "registerBtn";
            this.registerBtn.Size = new System.Drawing.Size(75, 59);
            this.registerBtn.TabIndex = 21;
            this.registerBtn.Text = "Register";
            this.registerBtn.UseVisualStyleBackColor = true;
            this.registerBtn.Click += new System.EventHandler(this.registerBtn_Click);
            // 
            // checkBtn
            // 
            this.checkBtn.Location = new System.Drawing.Point(465, 17);
            this.checkBtn.Name = "checkBtn";
            this.checkBtn.Size = new System.Drawing.Size(116, 39);
            this.checkBtn.TabIndex = 20;
            this.checkBtn.Text = "Check";
            this.checkBtn.UseVisualStyleBackColor = true;
            this.checkBtn.Click += new System.EventHandler(this.checkBtn_Click);
            // 
            // rgsTel
            // 
            this.rgsTel.Location = new System.Drawing.Point(93, 293);
            this.rgsTel.Name = "rgsTel";
            this.rgsTel.Size = new System.Drawing.Size(203, 21);
            this.rgsTel.TabIndex = 19;
            this.rgsTel.TextChanged += new System.EventHandler(this.rgsTel_TextChanged);
            this.rgsTel.Enter += new System.EventHandler(this.rgsTel_Enter);
            // 
            // rgsName
            // 
            this.rgsName.Location = new System.Drawing.Point(93, 220);
            this.rgsName.Name = "rgsName";
            this.rgsName.Size = new System.Drawing.Size(203, 21);
            this.rgsName.TabIndex = 18;
            this.rgsName.TextChanged += new System.EventHandler(this.rgsName_TextChanged);
            this.rgsName.Enter += new System.EventHandler(this.rgsName_Enter);
            // 
            // confirmPwd
            // 
            this.confirmPwd.Location = new System.Drawing.Point(91, 151);
            this.confirmPwd.Name = "confirmPwd";
            this.confirmPwd.Size = new System.Drawing.Size(203, 21);
            this.confirmPwd.TabIndex = 17;
            this.confirmPwd.UseSystemPasswordChar = true;
            this.confirmPwd.TextChanged += new System.EventHandler(this.confirmPwd_TextChanged);
            this.confirmPwd.Enter += new System.EventHandler(this.confirmPwd_Enter);
            // 
            // rgsPwd
            // 
            this.rgsPwd.Location = new System.Drawing.Point(91, 89);
            this.rgsPwd.Name = "rgsPwd";
            this.rgsPwd.Size = new System.Drawing.Size(203, 21);
            this.rgsPwd.TabIndex = 16;
            this.rgsPwd.UseSystemPasswordChar = true;
            this.rgsPwd.TextChanged += new System.EventHandler(this.rgsPwd_TextChanged);
            this.rgsPwd.Enter += new System.EventHandler(this.rgsPwd_Enter);
            // 
            // rgsAcc
            // 
            this.rgsAcc.Location = new System.Drawing.Point(91, 24);
            this.rgsAcc.Name = "rgsAcc";
            this.rgsAcc.Size = new System.Drawing.Size(203, 21);
            this.rgsAcc.TabIndex = 15;
            this.rgsAcc.TextChanged += new System.EventHandler(this.rgsAcc_TextChanged);
            this.rgsAcc.Enter += new System.EventHandler(this.rgsAcc_Enter);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.Location = new System.Drawing.Point(20, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 12);
            this.label13.TabIndex = 14;
            this.label13.Text = "Telephone";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label14.Location = new System.Drawing.Point(50, 223);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(29, 12);
            this.label14.TabIndex = 13;
            this.label14.Text = "Name";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label15.Location = new System.Drawing.Point(8, 156);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 12);
            this.label15.TabIndex = 12;
            this.label15.Text = "Confirm Pwd";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.Location = new System.Drawing.Point(26, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 11;
            this.label16.Text = "Password";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label17.Location = new System.Drawing.Point(32, 27);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 12);
            this.label17.TabIndex = 10;
            this.label17.Text = "Account";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.loginBtn);
            this.groupBox1.Controls.Add(this.logoutBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 484);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(145, 101);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Log In/Out";
            // 
            // loginBtn
            // 
            this.loginBtn.Location = new System.Drawing.Point(6, 20);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(132, 35);
            this.loginBtn.TabIndex = 10;
            this.loginBtn.Text = "Login";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Enabled = false;
            this.logoutBtn.Location = new System.Drawing.Point(6, 61);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(132, 34);
            this.logoutBtn.TabIndex = 9;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // roleBox
            // 
            this.roleBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.roleBox.FormattingEnabled = true;
            this.roleBox.Items.AddRange(new object[] {
            "Administrator",
            "Orderer",
            "Packer",
            "Shipper",
            "Reporter"});
            this.roleBox.Location = new System.Drawing.Point(5, 15);
            this.roleBox.Name = "roleBox";
            this.roleBox.Size = new System.Drawing.Size(121, 20);
            this.roleBox.TabIndex = 24;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.roleBox);
            this.groupBox6.Location = new System.Drawing.Point(305, 15);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(131, 43);
            this.groupBox6.TabIndex = 25;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Role";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.currentAccountRoleBox);
            this.groupBox7.Location = new System.Drawing.Point(198, 198);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(143, 53);
            this.groupBox7.TabIndex = 11;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Role";
            // 
            // currentAccountRoleBox
            // 
            this.currentAccountRoleBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.currentAccountRoleBox.FormattingEnabled = true;
            this.currentAccountRoleBox.Items.AddRange(new object[] {
            "Administrator",
            "Orderer",
            "Packer",
            "Shipper",
            "Reporter"});
            this.currentAccountRoleBox.Location = new System.Drawing.Point(12, 21);
            this.currentAccountRoleBox.Name = "currentAccountRoleBox";
            this.currentAccountRoleBox.Size = new System.Drawing.Size(121, 20);
            this.currentAccountRoleBox.TabIndex = 0;
            // 
            // MainFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 597);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.packingSlip);
            this.Controls.Add(this.accountManagement);
            this.Controls.Add(this.dataMaintenance);
            this.Controls.Add(this.display);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainFrame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Frame";
            this.dataMaintenance.ResumeLayout(false);
            this.accountManagement.ResumeLayout(false);
            this.packingSlip.ResumeLayout(false);
            this.display.ResumeLayout(false);
            this.slipGroupBox.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.slipOrderGridView)).EndInit();
            this.filter.ResumeLayout(false);
            this.filter.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.skuGroupBox.ResumeLayout(false);
            this.skuGroupBox.PerformLayout();
            this.calendarGroupBox.ResumeLayout(false);
            this.accountGroupBox.ResumeLayout(false);
            this.accountGroupBox.PerformLayout();
            this.detailsGroupBox.ResumeLayout(false);
            this.detailsGroupBox.PerformLayout();
            this.registerGroup.ResumeLayout(false);
            this.registerGroup.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button modifyBtn;
        private System.Windows.Forms.Button registerNewBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox slipGroupBox;
        private System.Windows.Forms.Button slipCheckBtn;
        private System.Windows.Forms.ComboBox slipOrderBox;
        private System.Windows.Forms.GroupBox skuGroupBox;
        private System.Windows.Forms.Button skuCheckBtn;
        private System.Windows.Forms.ComboBox skuBox;
        private System.Windows.Forms.Button skuAddNewBtn;
        private System.Windows.Forms.GroupBox calendarGroupBox;
        private System.Windows.Forms.Button monthlyReportBtn;
        private System.Windows.Forms.Button selectedDayReportBtn;
        private System.Windows.Forms.GroupBox accountGroupBox;
        private System.Windows.Forms.ComboBox accountBox;
        private System.Windows.Forms.GroupBox detailsGroupBox;
        private System.Windows.Forms.TextBox telephoneText;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox passwordText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveAccountBtn;
        private System.Windows.Forms.Button dailyReportBtn;
        public System.Windows.Forms.GroupBox dataMaintenance;
        public System.Windows.Forms.GroupBox accountManagement;
        public System.Windows.Forms.GroupBox packingSlip;
        public System.Windows.Forms.Button loginBtn;
        public System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.CheckBox statusCheckBox;
        private System.Windows.Forms.Button revertAccButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox skuDcpText;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox timeText;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox addressText;
        private System.Windows.Forms.TextBox customerText;
        public System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.GroupBox registerGroup;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox rgsTel;
        private System.Windows.Forms.TextBox rgsName;
        private System.Windows.Forms.TextBox confirmPwd;
        private System.Windows.Forms.TextBox rgsPwd;
        private System.Windows.Forms.TextBox rgsAcc;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button registerBtn;
        private System.Windows.Forms.Button checkBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tipText;
        private System.Windows.Forms.TextBox tipTitle;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radioAddress;
        private System.Windows.Forms.RadioButton radioCustomer;
        private System.Windows.Forms.RadioButton radioSlipNum;
        private System.Windows.Forms.TextBox slipText;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker shipTimePicker;
        private System.Windows.Forms.Button slipSearchBtn;
        private System.Windows.Forms.RadioButton radioShipTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox filter;
        private System.Windows.Forms.CheckBox historyCheckBox;
        private System.Windows.Forms.CheckBox liveCheckBox;
        public System.Windows.Forms.GroupBox display;
        private System.Windows.Forms.Button skuSearch;
        private System.Windows.Forms.TextBox skuSearchText;
        private System.Windows.Forms.RadioButton skuDescBtn;
        private System.Windows.Forms.RadioButton skuNameBtn;
        private System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.Button bookSlipBtn;
        public System.Windows.Forms.Button checkSlipBtn;
        public System.Windows.Forms.Button reportBtn;
        public System.Windows.Forms.Button itemBtn;
        public System.Windows.Forms.Button skuBtn;
        public System.Windows.Forms.Button logBtn;
        private System.Windows.Forms.DataGridView slipOrderGridView;
        private System.Windows.Forms.Button updateOrderBtn;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox shipCheckBox;
        private System.Windows.Forms.CheckBox packCheckBox;
        private System.Windows.Forms.CheckBox allCheckBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox roleBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox currentAccountRoleBox;

    }
}