﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;


namespace SKUTest
{
    public partial class PackingSlip : Form
    {
        private DataSet zipcodeDs;
        private DataSet slipDetailsDs;
        public DataSet skuDs;
        public DataSet itemDs;
        private DataSet extraItem;
        private String databaseName=Config.databaseName;
        private String IPAddress=Config.IPAddress;
        private String port=Config.port;
        private String localIPAddress = Config.localIPAddress;
    
        public String slipOrder;
        private AddSku addSku = null;
        private AddSlipItem addSlipItem = null;
        private String processor;
        private String role;
        private String bornTime = null;
        private int newRunningSlipCount;
        private Boolean isNew = false;
        private String checkinTime;
        private UpDownBase up;
        private Boolean zipcodeOK = true;
        private Boolean drop = false;
        private String skuCurrentValue = null;
        private String itemCurrentValue = null;
   
 
             
        public List<String[]> skuList = new List<String[]>();//new added skus     
        public List<String[]> newItem = new List<String[]>();//extra items
        public List<String[]> editItem = new List<String[]>();//new added items
        public List<String[]> editItemBySku = new List<string[]>();//new added items by addSku
        public List<String[]> editItemByItem = new List<string[]>();//new added items by addSlipItem

       

        public PackingSlip(String slipOrder,String processor,String role)
        {
            this.processor = processor;
            this.role = role;
            this.slipOrder = slipOrder;
            InitializeComponent();         
            initRunningSlipOrder();
            initZipcodeDataSet();
            drop = false;
            initSlipDetails();//if open as check, slip details should be initialized.
            drop = true;
            initSkuDataGridView();//if open as check, sku data should be initialized.
            initItemDataGridView();//if open as check, item data should be initialized
            initialAuthentication();//auth for diff roles

        }

        public PackingSlip(String processor,String role)
        {
            isNew = true;
            this.processor = processor;
            this.role = role;
            InitializeComponent();
            initSlipOrder();//generate a new slipOrder by the maximum plus one
            initZipcodeDataSet();
            initNullDataGridView();
            initialAuthentication();//auth for diff roles
            reportBtn.Enabled = false;
        }

        private void initialAuthentication()
        {
            if (role.Equals("Administrator"))
            {
                
            }
            else if (role.Equals("Orderer"))
            {
                packingGroupBox.Enabled = false;
                shippingGroupBox.Enabled = false;
                reportBtn.Enabled = false;

            }
            else if (role.Equals("Packer"))
            {
                informationGroupBox.Enabled = false;
                shippingGroupBox.Enabled = false;
                addSkuBtn.Enabled = false;
                addItemBtn.Enabled = false;
                checkExtraItemBtn.Enabled = false;
                reportBtn.Enabled = false;

            }
            else if (role.Equals("Shipper"))
            {
                informationGroupBox.Enabled = false;
                packingGroupBox.Enabled = false;
                if (packingCheckBox.Checked)
                    shippingGroupBox.Enabled = true;
                addSkuBtn.Enabled = false;
                addItemBtn.Enabled = false;
                checkExtraItemBtn.Enabled = false;
                reportBtn.Enabled = false;
            }
            else if (role.Equals("Reporter"))
            {
                panel.Enabled = false;
                checkBtn.Enabled = false;
                saveBtn.Enabled = false;
                skuDataGridView.ReadOnly = true;
                itemDataGridView.ReadOnly = true;
                reportBtn.Enabled = true;
            }
        }

        private void initNullDataGridView()
        {
            //init sku dataGridView
            skuDs = new DataSet();
            DataTable skuDt = new DataTable();
            DataColumn skuDc0 = new DataColumn("SKU#");
            DataColumn skuDc1 = new DataColumn("Product Details");
            DataColumn skuDc2 = new DataColumn("Quantity");
            skuDt.Columns.Add(skuDc0);
            skuDt.Columns.Add(skuDc1);
            skuDt.Columns.Add(skuDc2);
            skuDs.Tables.Add(skuDt);
            skuDataGridView.DataSource = skuDs.Tables[0];
           
            skuDataGridView.Columns[0].Width = 100;
            skuDataGridView.Columns[1].Width = 500;
            skuDataGridView.Columns[2].Width = 90;
            skuDataGridView.Columns[0].ReadOnly = true;
            skuDataGridView.Columns[1].ReadOnly = true;
            skuDataGridView.Columns[2].ReadOnly = false;

            //init item dataGridView
            itemDs = new DataSet();
            DataTable itemDt = new DataTable();
            DataColumn itemDc0 = new DataColumn("Item#");
            DataColumn itemDc1 = new DataColumn("Description");
            DataColumn itemDc2 = new DataColumn("Quantity");
            itemDt.Columns.Add(itemDc0);
            itemDt.Columns.Add(itemDc1);
            itemDt.Columns.Add(itemDc2);
            itemDs.Tables.Add(itemDt);
            itemDataGridView.DataSource = itemDs.Tables[0];
            itemDataGridView.Columns[0].Width = 100;
            itemDataGridView.Columns[1].Width = 500;
            itemDataGridView.Columns[2].Width = 90;
            itemDataGridView.Columns[0].ReadOnly = true;
            itemDataGridView.Columns[1].ReadOnly = true;
            itemDataGridView.Columns[2].ReadOnly = false;

        }

        private void initRunningSlipOrder()
        {
            checkinTime = DateTime.Now.ToString();
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            List<String[]> runningSlipOrder = conn.getRunningSlipOrder();
            conn.addRunningSlipOrder(slipOrder,localIPAddress,checkinTime);
            if (runningSlipOrder.Count != 0)
            {
                for (int i = 0; i < runningSlipOrder.Count; i++)
                {
                    if (runningSlipOrder[i][0].Equals(slipOrder) && Convert.ToDateTime(runningSlipOrder[i][2]).CompareTo(Convert.ToDateTime(checkinTime)) < 0)
                    {
                        checkBtn.Enabled = false;
                        addSkuBtn.Enabled = false;
                        addItemBtn.Enabled = false;
                        MessageBox.Show("Client: "+runningSlipOrder[i][1]+" is using this Slip Order. Now you could only read this Slip Order.");
                    }
                }
            }
        }

        private void initSlipOrder()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            slipOrder = (Convert.ToInt32(conn.getDataSet("use " + databaseName + " select count(*) from slipOrder").Tables[0].Rows[0][0].ToString()) + 10000).ToString();
            conn.addNewRunningSlipCount();
            newRunningSlipCount = conn.getNewRunningSlipCount();
            slipOrder = (Convert.ToInt32(slipOrder) + newRunningSlipCount-1).ToString();
            

            bornTimeTextBox.Text = DateTime.Now.ToString();
            bornTime = DateTime.Now.ToString();
            slipOrderLabel.Text = slipOrder;
            processorText.Text = processor;
        }

        private void initZipcodeDataSet()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            zipcodeDs = conn.getDataSet("use " + databaseName + " select * from zipcode");
            up = (UpDownBase)zipcodeNumber;
            up.TextChanged += new System.EventHandler(this.zipcodeNumber_TextChanged);
            //up.LostFocus += new System.EventHandler(this.zipcodeNumber_ValueChanged);
        }


       private void checkBtn_Click(object sender, EventArgs e)
       {
           zipcodeNumber_ValueChanged(sender, e);
           Boolean shipMethod = true;
           Boolean orderLevel = true;
           Boolean payWay = true;
           if (shippingCheckBox.Checked)
           {
               if (upsCombo.SelectedIndex == -1 && uspsCombo.SelectedIndex == -1)
                   shipMethod = false;
               if (expCombo.SelectedIndex == -1 && radioRgl.Checked==false)
                   orderLevel = false;
               if (payCombo.SelectedItem == null)
                   payWay = false;
           }
           
           String warning = "The Slip Order has some errors: \n";
           if (address1Text.Text == "")
               warning+= "Address Null!\n";
           if (cityText.Text == "")
               warning+="City Null! \n";
           if (stateText.Text == "")
               warning += "State Null! \n";
           if (up.Text == "")
               warning += "Zipcode Null! \n";
           if (!shipMethod)
               warning += "Ship Method Null! \n";
           if (!orderLevel)
               warning += "Order Level Null! \n";
           if (!payWay)
               warning += "Payment Way Null! \n";
           if (packingCheckBox.Checked)
               if (lSizeText.Text == "" || wSizeText.Text == "" || hSizeText.Text == "")
               warning += "Package Size Null! \n";
           if (packingCheckBox.Checked && actualWeightText.Text == "")
               warning += "Actual Weight Null! \n";
        
           foreach (DataRow r in itemDs.Tables[0].Rows)
           {
               if (Convert.ToDecimal(r[2]) < 0)
                   warning += "Item#: " + r[0] + " quantity is less than 0.";
           }
           if (warning != "The Slip Order has some errors: \n")
               MessageBox.Show(warning);
           else if (warningWeight())
           {
               if (zipcodeOK)
               {
                   saveBtn.Enabled = true;
                   MessageBox.Show("This Slip Order is valid now.");
               }
           }
           else saveBtn.Enabled = false;
           
       }
       private void zipcodeNumber_TextChanged(object sender, EventArgs e)
       {
           String zipcode = up.Text;
           if (zipcode.Length > 5 )
           {
               up.Text = zipcode.Remove(zipcode.Length - 1);
               MessageBox.Show("Zipcode should be at length of 5.");
               zipcodeOK = false;

           }
           if (zipcode.Length == 5)
           {
               zipcodeNumber_ValueChanged(sender, e);
           }

           saveBtn.Enabled = false;
       }

       private void zipcodeNumber_ValueChanged(object sender, EventArgs e)
       {
           if (up.Text.Length == 5)
           {
               Boolean exist = false;

               for (int i = 0; i < zipcodeDs.Tables[0].Rows.Count; i++)
                   if (zipcodeDs.Tables[0].Rows[i][0].ToString().Trim().Equals(zipcodeNumber.Value.ToString()))
                   {
                       cityText.Text = zipcodeDs.Tables[0].Rows[i][1].ToString().Trim().ToLower();
                       stateText.Text = zipcodeDs.Tables[0].Rows[i][2].ToString().Trim().ToLower();
                       exist = true;
                       break;
                   }
               if (!exist)
               {

                   for (int i = 0; i < zipcodeDs.Tables[0].Rows.Count; i++)
                       if (zipcodeDs.Tables[0].Rows[i][1].ToString().Trim().ToLower().Equals(cityText.Text.ToString().Trim()) && zipcodeDs.Tables[0].Rows[i][2].ToString().Trim().ToLower().Equals(stateText.Text.ToString().Trim()))
                       {
                           up.Text = zipcodeDs.Tables[0].Rows[i][0].ToString().Trim();
                           break;
                       }
                   MessageBox.Show("This zipcode does not exist.");
                   zipcodeOK = false;

               }
               else zipcodeOK = true;
           }
           else
           {
               zipcodeOK = false;
               MessageBox.Show("Zipcode should be at length of 5.");
               for (int i = 0; i < zipcodeDs.Tables[0].Rows.Count; i++)
                   if (zipcodeDs.Tables[0].Rows[i][1].ToString().Trim().ToLower().Equals(cityText.Text.ToString().Trim()) && zipcodeDs.Tables[0].Rows[i][2].ToString().Trim().ToLower().Equals(stateText.Text.ToString().Trim()))
                   {
                       up.Text = zipcodeDs.Tables[0].Rows[i][0].ToString().Trim();          
                       break;
                   }  
           }
           saveBtn.Enabled = false;
       }

       private void addSkuBtn_Click(object sender, EventArgs e)
       {
           saveBtn.Enabled = false;
           //allow only one addSku winform exists  
           addSku = new AddSku(this);
           addSku.ShowDialog();        
           
       }

       private void addItemBtn_Click(object sender, EventArgs e)
       {
           saveBtn.Enabled = false;
           //allow only one addItem winform exists
           addSlipItem = new AddSlipItem(this);
           addSlipItem.ShowDialog();              
           
       }

       private void initSkuDataGridView()
       {
           SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
           skuDs = conn.getDataSet("use " + databaseName + " select sku.sku as 'SKU#',sku.productDetails as 'Product Details',skuQuantity as 'Quantity' from slipOrder_sku,sku where slipOrder='" + slipOrder + "' and sku.sku=slipOrder_sku.sku");


           skuDataGridView.DataSource = skuDs.Tables[0];
          
           skuDataGridView.Columns[0].Width = 100;
           skuDataGridView.Columns[1].Width = 500;
           skuDataGridView.Columns[2].Width = 90;
           skuDataGridView.Columns[0].ReadOnly = true;
           skuDataGridView.Columns[1].ReadOnly = true;
           skuDataGridView.Columns[2].ReadOnly = false;
           
       }

       private void initItemDataGridView()//triggered by check slipOrder
       {
           itemDs = new DataSet();
           DataTable dt = new DataTable();
           DataColumn dc0 = new DataColumn("Item#");
           DataColumn dc1 = new DataColumn("Description");
           DataColumn dc2 = new DataColumn("Quantity");
           dt.Columns.Add(dc0);
           dt.Columns.Add(dc1);
           dt.Columns.Add(dc2);
           itemDs.Tables.Add(dt);

           initItemTable(dt);

           itemDataGridView.DataSource = dt;
           itemDataGridView.Columns[0].Width = 100;
           itemDataGridView.Columns[1].Width = 500;
           itemDataGridView.Columns[2].Width = 90;
           itemDataGridView.Columns[0].ReadOnly = true;
           itemDataGridView.Columns[1].ReadOnly = true;
           itemDataGridView.Columns[2].ReadOnly = false;
       }

       private void initItemTable(DataTable dt)
       {
           //init extra Item
           SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
           extraItem = conn1.getDataSet("use " + databaseName + " select slipOrder_newItem.item, slipOrder_newItem.itemQuantity, item.description from slipOrder_newItem, item where item.item= slipOrder_newItem.item and slipOrder_newItem.slipOrder='" + slipOrder + "'");
           
           if (extraItem.Tables[0].Rows.Count != 0)
           {
               //row[0]=item, row[1]=quantity, row[2]=description
               foreach (DataRow row in extraItem.Tables[0].Rows)
               {
                   DataRow r = dt.NewRow();
                   r[0] = row[0].ToString().Trim();
                   r[1] = row[2].ToString().Trim();
                   r[2] = row[1].ToString().Trim();
                   dt.Rows.Add(r);
                   String[] s = new String[2];
                   s[0] = row[0].ToString().Trim();
                   s[1] = row[1].ToString().Trim();
                   newItem.Add(s);
               }
           }

           //init items in skus
           foreach (DataRow skuDsRow in skuDs.Tables[0].Rows)
           {
               //get all items about that sku
               SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
               DataSet temp = conn.getDataSet("use " + databaseName + " select item.item as 'Item#',item.description as 'Description',itemQuantity as 'Quantity' from item,sku_item where item.item=sku_item.item and sku_item.sku='" + skuDsRow[0].ToString() + "'");


               foreach (DataRow tempRow in temp.Tables[0].Rows)
               {
                   //check if that particular item has already existed in the itemDs
                   Boolean isNew = true;
                   foreach (DataRow r in dt.Rows)
                   {
                       if (r[0].ToString().Trim().Equals(tempRow[0].ToString().Trim()))
                       {
                           isNew = false;
                           break;
                       }
                   }
                   //if the item is new, add it to a new line 
                   if (isNew)
                   {
                       DataRow newRow = dt.NewRow();
                       newRow[0] = tempRow[0].ToString().Trim();//item#
                       newRow[1] = tempRow[1].ToString().Trim(); ;//description
                       newRow[2] = (Convert.ToInt32(tempRow[2]) * Convert.ToInt32(skuDsRow[2])).ToString();//quantity
                       dt.Rows.Add(newRow);
                   }
                   else//if the item exists, add quantity of that item
                   {
                       for (int i = 0; i < dt.Rows.Count; i++)
                       {
                           if (dt.Rows[i][0].ToString().Trim().Equals(tempRow[0].ToString().Trim()))
                           {
                               int quantity = Convert.ToInt32(dt.Rows[i][2]) + Convert.ToInt32(tempRow[2]) * Convert.ToInt32(skuDsRow[2]);
                               //MessageBox.Show(quantity.ToString());
                               if (quantity == 0)
                               {
                                   dt.Rows.Remove(dt.Rows[i]);
                               }
                               else
                               {
                                   dt.Rows[i][2] = quantity.ToString();
                               }
                           }
                       }
                   }
               }
           }
           calculateWeight();



       }

       public DataTable refreshItemDataGridView(DataTable skuTable,DataTable itemTable)
       {
           itemDs.Tables[0].Clear();
           SkuServer.ConnectSql conn1 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
           
           //calculate qty in newItem
           if (newItem.Count!=0)
           {
               foreach (String[] item_Qty in newItem)
               {
                   Boolean isNew = true;
                   for (int i = 0; i < itemTable.Rows.Count; i++)
                   {
                       if (itemTable.Rows[i][0].ToString().Equals(item_Qty[0].ToString().Trim()))
                       {
                           isNew = false;
                           break;
                       }
                   }
                   if (isNew)
                   {
                       DataRow newRow = itemTable.NewRow();
                       newRow[0] = item_Qty[0].Trim();//item#
                       newRow[1] = conn1.getDataSet("use "+databaseName+" select description from item where item='"+item_Qty[0]+"'").Tables[0].Rows[0][0].ToString();//description
                       newRow[2] = item_Qty[1].Trim();//quantity
                       itemTable.Rows.Add(newRow);
                       
                   }
                   else//if the item exists, add quantity of that item
                   {
                       for (int j = 0; j < itemTable.Rows.Count; j++)
                       {
                           if (itemTable.Rows[j][0].ToString().Equals(item_Qty[0].ToString().Trim()))
                           {
                               itemTable.Rows[j][2] = (Convert.ToInt32(itemTable.Rows[j][2]) + Convert.ToInt32(item_Qty[1])).ToString();
                           }

                       }
                   }
                    
               }
           }
           //calculate qty in editItemByItem
           foreach (String[] s in editItemByItem)
           {
               Boolean isN = true;
               foreach (DataRow r in itemTable.Rows)
               {
                   if (r[0].ToString().Equals(s[0]))
                       isN = false;
               }
               
               if (!isN)
               {
                   foreach (DataRow r in itemTable.Rows)
                   {
                       if (r[0].ToString().Equals(s[0]))
                       {
                           r[2] = Convert.ToDecimal(r[2]) + Convert.ToDecimal(s[1]);                  
                       }

                   }
               }
               else
               {          
                   DataRow r = itemTable.NewRow();
                   r[0] = s[0];
                   r[1] = conn1.getDataSet("use " + databaseName + " select description from item where item='" + s[0] + "';").Tables[0].Rows[0][0].ToString();
                   r[2] = s[1];
                   itemTable.Rows.Add(r);            
               }
           }


           //calcuate qty in skuTable=skuList+original skus
           foreach (DataRow skuDsRow in skuTable.Rows)
           {
               //get all items about that sku
               SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
               DataSet temp = conn.getDataSet("use " + databaseName + " select item.item as 'Item#',item.description as 'Description',itemQuantity as 'Quantity' from item,sku_item where item.item=sku_item.item and sku_item.sku='" + skuDsRow[0].ToString() + "'");

               //temp stores all items about that sku
               foreach (DataRow tempRow in temp.Tables[0].Rows)
               {
                   //check if that particular item has already existed in the itemDs
                   Boolean isNew = true;
                   foreach (DataRow r in itemTable.Rows)
                   {
                       if (r[0].ToString().Trim().Equals(tempRow[0].ToString().Trim()))
                       {
                           isNew = false;
                           break;
                       }
                   }
                   //if the item is new, add it to a new line 
                   if (isNew)
                   {
                       DataRow newRow = itemTable.NewRow();
                       newRow[0] = tempRow[0].ToString().Trim();//item#
                       newRow[1] = tempRow[1].ToString().Trim();//description
                       newRow[2] = (Convert.ToInt32(tempRow[2]) * Convert.ToInt32(skuDsRow[2])).ToString();//quantity
                       itemTable.Rows.Add(newRow);
                   }
                   else//if the item exists, add quantity of that item
                   {
                       for (int i=0; i<itemTable.Rows.Count; i++)
                       {
                           if (itemTable.Rows[i][0].ToString().Trim().Equals(tempRow[0].ToString().Trim()))
                           {
                               int quantity = Convert.ToInt32(itemTable.Rows[i][2]) + Convert.ToInt32(tempRow[2]) * Convert.ToInt32(skuDsRow[2]);
                               //MessageBox.Show(quantity.ToString());
                               itemTable.Rows[i][2] = quantity.ToString();
                               
                           }
                       }
                   }
               }
           }
           calculateWeight();
           return itemTable;
        }

        private void initSlipDetails()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            slipDetailsDs = conn.getDataSet("use " + databaseName + " select * from slipOrder where [slipOrder]='" + slipOrder + "'");
            bornTimeTextBox.Text = conn.getDataSet("use " + databaseName + " select bornTime from slipOrder_bornTime where slipOrder='" + slipOrder + "';").Tables[0].Rows[0][0].ToString();

            DataTable dt = slipDetailsDs.Tables[0];

            //row[0][0] stores slipOrder
            slipOrderLabel.Text=dt.Rows[0][0].ToString().Trim();

            //row[0][1] stores name
            nameText.Text = dt.Rows[0][1].ToString().Trim();
            
            //row [0][2] stores address
            char[] separator = { '@' };
            string[] addressArray=dt.Rows[0][2].ToString().Trim().Split(separator);
            address1Text.Text = addressArray[0];
            address2Text.Text = addressArray[1];
            cityText.Text = addressArray[2];
            stateText.Text = addressArray[3];
            zipcodeNumber.Value = Convert.ToInt32(addressArray[4]);

            //row[0][3] stores shipMethod
            foreach ( String m in upsCombo.Items)
            {
                if (m.Equals(dt.Rows[0][3].ToString().Trim()))
                {
                    upsCombo.SelectedItem = m;
                    radioUps.Checked = true;
                }
            }
            foreach (String m in uspsCombo.Items)
            {
                if (m.Equals(dt.Rows[0][3].ToString().Trim()))
                {
                    uspsCombo.SelectedItem = m;
                    radioUsps.Checked = true;
                }
            }

            //row[0][4] stores orderLevel
            if (dt.Rows[0][4].ToString().Trim() == radioRgl.Text)
                radioRgl.Checked = true;
            else
            {
                foreach (String o in expCombo.Items)
                {
                    if (o.Equals(dt.Rows[0][4].ToString().Trim()))
                    {
                        expCombo.SelectedItem = o;
                        radioExp.Checked = true;
                    }
                }
            }

            //row[0][5] stores  processor
            processorText.Text = dt.Rows[0][5].ToString().Trim();

            //row[0][6] stores  shipDate
            shipDatePicker.Value = Convert.ToDateTime(dt.Rows[0][6].ToString().Trim());
            
            //row[0][7] stores shipState
            if (dt.Rows[0][7].ToString().Trim().Equals("00"))
            {
                packingCheckBox.Checked = false;
                shippingCheckBox.Checked = false;
                
                lSizeText.Text = "";
                actualWeightText.Text = "";
                lSizeText.ReadOnly = true;
                actualWeightText.ReadOnly = true;
            }
            else if (dt.Rows[0][7].ToString().Trim().Equals("10"))
            {
                packingCheckBox.Checked = true;
                shippingCheckBox.Checked = false;
                shipMethodGroupBox.Enabled = false;
                orderLevelGroupBox.Enabled = false;
                payWayGroupBox.Enabled = false;
                lSizeText.ReadOnly = false;
                actualWeightText.ReadOnly = false;
            }
            else if (dt.Rows[0][7].ToString().Trim().Equals("11"))
            {
                packingCheckBox.Checked = true;
                shippingCheckBox.Checked = true;
                shipMethodGroupBox.Enabled = true;
                orderLevelGroupBox.Enabled = true;
                payWayGroupBox.Enabled = true;
                lSizeText.ReadOnly = true;
                actualWeightText.ReadOnly = true;
            }

            //row[0][8] stores payWay
            foreach (String p in payCombo.Items)
            {
                if (p.Equals(dt.Rows[0][8].ToString().Trim()))
                    payCombo.SelectedItem = p;
            }
            
            //row[0][9] stores packageSize
            string[] sizeArray = dt.Rows[0][9].ToString().Trim().Split(separator);
            if (sizeArray.Length ==3)
            {
                lSizeText.Text = sizeArray[0];
                wSizeText.Text = sizeArray[1];
                hSizeText.Text = sizeArray[2];
            }
            //row[0][10] stores weight
            actualWeightText.Text = dt.Rows[0][10].ToString().Trim();
            DateTime t =Convert.ToDateTime(dt.Rows[0][6].ToString().Trim());
            if (t.AddDays(1) < DateTime.Now)
            {
                panel.Enabled = false;
                checkBtn.Enabled = false;
                saveBtn.Enabled = false;
                skuDataGridView.ReadOnly = true;
                itemDataGridView.ReadOnly = true;
                slipOrderLabel.Text = slipOrderLabel.Text + " (History)";
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            filterList();  
            if (detectRepeatOrder())
            {
                if (!detectOverflow())
                {
                    if (checkSlipOrder())//check the slipOrder in case of other users are using the same one
                    {
                        updateRestItemQuantity();
                        saveLog();             
                        saveSku();
                        saveDetails();
                        saveExtraItem();
                        

                        if (bornTime != null)
                            saveBornTime();
                        MessageBox.Show("Saved!");
                        
                        this.Close();
                    }
                }
                
                    
            }
            
            
        }

        private void minusRunningSlipOrder()
        {
            if (isNew)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                conn.minusNewRunningSlipCount();
            }
        }

        private Boolean checkSlipOrder()
        {
            if (isNew)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                if (newRunningSlipCount != conn.getNewRunningSlipCount())
                {
                    slipOrder = (Convert.ToInt32(conn.getDataSet("use " + databaseName + " select count(*) from slipOrder").Tables[0].Rows[0][0].ToString()) + 9999).ToString();
                    slipOrder = (Convert.ToInt32(conn.getNewRunningSlipCount()) + Convert.ToInt32(slipOrder)).ToString();
                    MessageBox.Show("Warning! Someone has cancelled the previous Slip Order. Yours will be changed to " + slipOrder + ".");
                    newRunningSlipCount = conn.getNewRunningSlipCount();
                    slipOrderLabel.Text = slipOrder;
                    return false;

                }
                else
                {
                    return true;
                }
            }
            return true;
        }

       
        private void saveBornTime()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            conn.getDataSet("use " + databaseName + " insert into slipOrder_bornTime (slipOrder, bornTime) values('" + slipOrder + "','" + bornTime + "');");
        }

        private Boolean detectRepeatOrder()
        {
            if (skuDs.Tables[0].Rows.Count != 0)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                DataSet ds = conn.getDataSet("use " + databaseName + " select * from slipOrder_bornTime");
                if (ds.Tables[0].Rows.Count != 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        //row[0] stores slipOrder, row[1] stores bornTime
                        DateTime bornDay = Convert.ToDateTime(row[1]);
                        if (bornDay.AddDays(5) > DateTime.Now)
                        {
                            DataSet dss = conn.getDataSet("use " + databaseName + " select name, sku from slipOrder, slipOrder_sku where slipOrder.slipOrder='" + row[0].ToString().Trim() + "' and slipOrder_sku.slipOrder='" + row[0].ToString().Trim() + "';");
                            
                            if (dss.Tables[0].Rows.Count != 0)
                            {
                                List<String> skuList = new List<String>();

                                if (dss.Tables[0].Rows[0][0].ToString().Equals(nameText.Text.Trim()))
                                {

                                    foreach (DataRow r in dss.Tables[0].Rows)
                                    {
                                        //r[0] stores customerName, r[1] stores sku
                                        skuList.Add(r[1].ToString());
                                    }
                                }
                                else continue;
                                if (skuDs.Tables[0].Rows.Count == skuList.Count)
                                {
                                    foreach (DataRow dr in skuDs.Tables[0].Rows)
                                    {
                                        for (int i = 0; i < skuList.Count; i++)
                                        {
                                            if (skuList[i].Equals(dr[0].ToString()))
                                                skuList.Remove(skuList[i]);
                                        }
                                    }
                                }
                                else continue;

                                if (skuList.Count == 0 && skuDs.Tables[0].Rows.Count != 0 && !row[0].ToString().Equals(slipOrder))
                                {
                                    if (MessageBox.Show(this, "Warning! This order is very similar to the previous Order " + row[0].ToString() + " . Do you still continue?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        conn.addText("Client: " + localIPAddress + " is tending to create a Slip Order that is similar to the previous Order: " + row[0].ToString() + "\r\n");
                                        return true;
                                    }
                                    else return false;
                                }
                                else continue;
                            }
                            else continue;

                        }
                        else continue;
                    }
                }
                else return true;
                
            }
            return true;
        }

        private Boolean warningWeight()
        {
            if (!totalWeightText.Text.Equals(actualWeightText.Text) && packingCheckBox.Checked)
            {
                if (MessageBox.Show(this, "Warning! The actual weight does not equal to the total weight. Do you still continue?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    return true;
                else return false;
                
            }
            return true;
        }

        public void filterList()
        {
            for (int i = 0; i < editItem.Count; i++)
            {
                for (int j = i + 1; j < editItem.Count; j++)
                {
                    if (editItem[i][0].Equals(editItem[j][0]))
                    {
                        editItem[i][1] = (Convert.ToInt32(editItem[i][1]) + Convert.ToInt32(editItem[j][1])).ToString();
                        editItem.Remove(editItem[j]);
                        j--;
                    }
                }
            }

            for (int i = 0; i < skuList.Count; i++)
            {
                for (int j = i + 1; j < skuList.Count; j++)
                {
                    if (skuList[i][0].Equals(skuList[j][0]))
                    {
                        skuList[i][1] = (Convert.ToInt32(skuList[i][1]) + Convert.ToInt32(skuList[j][1])).ToString();
                        skuList.Remove(skuList[j]);
                        j--;
                    }
                }
            }
        }

        private void saveLog()
        {
            //save manipulation log into database.table.name= [slipOrderLog]
            DateTime currentTime = DateTime.Now;
            String log = null;

            /*
            foreach (String[] s in newSku)
            {
                //MessageBox.Show(s[0] + "newSku");
                log += "\r\nAdd " + s[1] + " new SKU: " + s[0] +" Total: ";
            }

            foreach (String[] s in editSku)
            {
                //MessageBox.Show(s[0] + "edit");
                if (Convert.ToInt32(s[1]) > 0)
                    log += "\r\nAdd " + s[1] + " SKU: " + s[0];
                else
                    log += "\r\nMinus " + (-Convert.ToInt32(s[1])).ToString() + " SKU: " + s[0];
            }
            
            foreach (String s in deleteSku)
            {
                //MessageBox.Show("delete sku" + s);
                log += "\r\nDelete SKU: " + s;
            }
            
            for (int i = 0; i < newItem.Count; i++ )
            {
                //MessageBox.Show("new Item"+newItem[i][0]);
                if (Convert.ToInt32(newItem[i][1]) > 0)
                    log += "\r\nAdd " + newItem[i][1] + " Item:" + newItem[i][0];
                else
                    log += "\r\nMinus " + (-Convert.ToInt32(newItem[i][1])).ToString() + " Item: " + newItem[i][0];
            }
            */
            filterList();
            
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            foreach (string[] s in skuList)
            {
                String totalQty = null;
                foreach (DataRow r in skuDs.Tables[0].Rows)
                {
                    if (s[0].Equals(r[0].ToString()))
                        totalQty = r[2].ToString();
                }
                
                log += "\r\nSKU: "+s[0]+" New Edited Quantity: "+s[1]+" Total Quantity: "+totalQty+"    Description: "+conn.getDataSet("use "+databaseName+" select productDetails from sku where sku='"+s[0]+"'").Tables[0].Rows[0][0].ToString()+"\r\n";
            }
            foreach (string[] s in editItem)
            {
                String totalQty = null;
                foreach (DataRow r in itemDs.Tables[0].Rows)
                {
                    if (s[0].Equals(r[0].ToString()))
                        totalQty = r[2].ToString();
                }
                
                log += "\r\nNew Edited Item: " + s[0] + " Quantity: " + s[1] +" Total Quantity: "+totalQty+ "    Description: " + conn.getDataSet("use " + databaseName + " select description from item where item='" + s[0] + "'").Tables[0].Rows[0][0].ToString()+"\r\n";
            }
            if (log != null)
            {
                conn.getDataSet("use " + databaseName + " insert into slipOrderLog(slipOrder, processor, date, manipulationLog) values('" + slipOrder + "','" + processor + "','" + currentTime + "','" + log + "');");
            
            }
        }

        private void saveDetails()
        {
            //save slip order details
            String name=null;
            String address=null;
            String shipMethod = null;
            String orderLevel = null;
            String processor=null;  
            String shipDate=null;
            String shipState = null;
            String payWay = null;
            String packageSize=null;
            String weight=null;

            //row[0][0] stores slipOrder
            slipOrder = slipOrderLabel.Text;

            //row[0][1] stores name
            name = nameText.Text;

            //row [0][2] stores address
            address = address1Text.Text.Trim() + "@" + address2Text.Text.Trim() + "@" + cityText.Text.Trim() + "@" + stateText.Text.Trim() + "@" + zipcodeNumber.Value.ToString();

            if (packingCheckBox.Checked)
            {
                if (shippingCheckBox.Checked)
                {
                    //row[0][3] stores shipMethod
                    if (radioUps.Checked)
                        shipMethod = upsCombo.SelectedItem.ToString();
                    else
                        shipMethod = uspsCombo.SelectedItem.ToString();
                    /*
                    foreach (Control ctrl in shipMethodGroupBox.Controls)
                    {
                        if (ctrl is RadioButton)
                            if (((RadioButton)ctrl).Checked)
                                shipMethod = ((RadioButton)ctrl).Text;
                    }
                    */

                    //row[0][4] stores orderLevel
                    if (radioRgl.Checked)
                        orderLevel = radioRgl.Text;
                    else
                        orderLevel = expCombo.SelectedItem.ToString();

                    //row[0][8] stores payWay
                    if (payCombo.SelectedItem != null)
                        payWay = payCombo.SelectedItem.ToString().Trim();
                    /*
                    foreach (Control ctrl in payWayGroupBox.Controls)
                    {
                        if (ctrl is RadioButton)
                            if (((RadioButton)ctrl).Checked)
                                payWay = ((RadioButton)ctrl).Text;
              
                    }
                
                    */


                }
                //row[0][9] stores packageSize
                packageSize = lSizeText.Text.Trim() + "@" + wSizeText.Text.Trim() + "@" + hSizeText.Text.Trim();

                //row[0][10] stores weight
                weight = actualWeightText.Text.Trim();

            }
            /*
            foreach (Control ctrl in orderLevelGroupBox.Controls)
            {
                if (ctrl is RadioButton)
                    if (((RadioButton)ctrl).Checked)
                        orderLevel = ((RadioButton)ctrl).Text;
            }
             */ 

            //row[0][5] stores  processor
            processor = processorText.Text.Trim();

            //row[0][6] stores  shipDate
            shipDate = shipDatePicker.Value.ToString().Trim();

            //row[0][7] stores shipState
            if (!packingCheckBox.Checked && !shippingCheckBox.Checked)
                shipState = "00";
            else if (packingCheckBox.Checked && !shippingCheckBox.Checked)
                shipState = "10";
            else if (packingCheckBox.Checked && shippingCheckBox.Checked)
                shipState = "11";

            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            conn.getDataSet("use " + databaseName + " delete from slipOrder where slipOrder='" + slipOrder + "';");
            
            conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            conn.getDataSet("use " + databaseName + " insert into slipOrder (slipOrder, name, address, shipMethod, orderLevel, processor, shipDate, shipState, payWay, packageSize, weight) values ('" + slipOrder + "', '" + name + "', '" + address + "', '" + shipMethod + "', '" + orderLevel + "', '" + processor + "', '" + shipDate + "', '" + shipState + "', '" + payWay + "', '" + packageSize + "', '" + weight + "');");
            conn.addText("Client: " + localIPAddress + " has modified Slip Order: " + slipOrder + " at " + DateTime.Now.ToString() + "\r\n");
            
        }

        private void saveSku()
        {
            //delete all skus about that slip order
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            conn.getDataSet("use " + databaseName + " delete from slipOrder_sku where slipOrder='" + slipOrder + "';");
            foreach (String[] s in skuList)
            {
                conn.addText("Client: " + localIPAddress + " has varied SKU: " + s[0] + " quantity by " + s[1] + " in Slip Order: "+slipOrder+"\r\n");
            }
            //insert all skus to that slip order
            foreach (DataRow dr in skuDs.Tables[0].Rows)
            {
                if (!dr[2].ToString().Equals("0"))
                {
                    conn.getDataSet("use " + databaseName + " insert into slipOrder_sku (slipOrder, sku, skuQuantity) values('" + slipOrder + "','" + dr[0].ToString() + "','" + dr[2].ToString() + "');");
                }
                
            }            
        }

        private void saveExtraItem()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            conn.getDataSet("use " + databaseName + " delete from slipOrder_newItem where slipOrder='" + slipOrder + "';");
            foreach (String[] s in editItemByItem)
            {
                Boolean isNew = true;
                foreach (String[] ss in newItem)
                {
                    if (ss[0].Equals(s[0]))
                    {
                        isNew = false;
                        
                        break;
                    }
                }

                if (!isNew)
                {
                    foreach (String[] rr in newItem)
                    {
                        if (rr[0].ToString().Equals(s[0]))
                            rr[1] = (Convert.ToDecimal(rr[1]) + Convert.ToDecimal(s[1])).ToString();
                    }

                }
                else//new item
                {
                    String[] dr = new String[2];
                    dr[0] = s[0];//item
                    dr[1] = s[1];//quantity
                    newItem.Add(dr);
                }
            }
           
            foreach (String[] s in newItem)
            {           
                if (!s[1].Equals("0"))
                {
                    conn.getDataSet("use " + databaseName + " insert into slipOrder_newItem (slipOrder, item, itemQuantity) values('" + slipOrder + "','" +s[0] + "','" + s[1] + "');");
                }
            }
      
        }        
     
        private Boolean detectOverflow()
        {
            //create a temp list so that true editItem will not be influenced
            List<String[]> tempEditItem = new List<String[]>();
       
            foreach (String[] s in editItemBySku)
            {
                String[] ts = new String[2];
                ts[0] = s[0].ToString();
                ts[1] = s[1].ToString();
                tempEditItem.Add(ts);
               
            }
            foreach (String[] s in editItemByItem)
            {
                String[] ts = new String[2];
                ts[0] = s[0].ToString();
                ts[1] = s[1].ToString();
                tempEditItem.Add(ts);

            }

            //filter the tempEditItem
            
            for (int i = 0; i < tempEditItem.Count; i++)
            {
                for (int j = i + 1; j < tempEditItem.Count; j++)
                {
                    if (tempEditItem[i][0].Equals(tempEditItem[j][0]))
                    {
                        tempEditItem[i][1] = (Convert.ToInt32(tempEditItem[i][1]) + Convert.ToInt32(tempEditItem[j][1])).ToString();
                        tempEditItem.Remove(tempEditItem[j]);
                        j--;
                    }
                }
            }
              
     

            SkuServer.ConnectSql conn2 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            foreach (String[] newItemRow in tempEditItem)
            {
                Decimal costQty = Convert.ToDecimal(newItemRow[1]);
                DataSet ds1 = conn2.getDataSet("use " + databaseName + " select quantity from item where item='" + newItemRow[0] + "';");
                Decimal restQty = Convert.ToDecimal(ds1.Tables[0].Rows[0][0]) - costQty;
                if (restQty < 0)
                {
                    MessageBox.Show("Save Error! This leads to quantity of Item: " + newItemRow[0] + " less than 0.");
                    saveBtn.Enabled = false;
                    return true;
                }
            }
            return false;        
        }

        private void updateRestItemQuantity()
        {
            SkuServer.ConnectSql conn2 = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");

            //mix editItemBySku editItemByItem into editItem 
            editItem.Clear();
            foreach (String[] s in editItemBySku)
            {
                editItem.Add(s);
            }
            foreach (String[] s in editItemByItem)
            {
                editItem.Add(s);
            }
            //editItemBySku.Clear();
            //editItemByItem.Clear();
            filterList();

            foreach (string[] newItemRow in editItem)
            {
                Decimal costQty = Convert.ToInt32(newItemRow[1]);
                DataSet ds1 = conn2.getDataSet("use " + databaseName + " select quantity from item where item='" + newItemRow[0] + "';");
                Decimal restQty = Convert.ToInt32(ds1.Tables[0].Rows[0][0]) - costQty;
                conn2.getDataSet("use " + databaseName + " update item set quantity='" + restQty.ToString() + "' where item='" + newItemRow[0] + "'");
                conn2.addText("Client: " + localIPAddress + " has varied Item: " + newItemRow[0] + " quantity by "+newItemRow[1]+" in Slip Order: "+slipOrder+"\r\n");
            }
        }

        private void cityText_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String city = cityText.Text;
            if (city.Length > 20)
            {
                cityText.Text = city.Remove(city.Length - 1);
                MessageBox.Show("The length should be less than 20 letters.");
            }
        }

        private void stateText_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String state = stateText.Text;
            if (state.Length > 20)
            {
                stateText.Text = state.Remove(state.Length - 1);
                MessageBox.Show("The length should be less than 20 letters.");
            }
        }

        private void address1Text_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String address = address1Text.Text;
            if (address.Length > 40)
            {
                address1Text.Text = address.Remove(address.Length - 1);
                MessageBox.Show("The lehgth of address should be less than 40 letters.");
            }
            else
            {
                foreach (char c in address)
                {
                    if (c == '@')
                    {
                        address1Text.Text = address.Remove(address.Length - 1);
                        MessageBox.Show("Address cannot include @");
                    }
                }
            }
        }

        private void address2Text_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String address = address2Text.Text;
            if (address.Length > 40)
            {
                address2Text.Text = address.Remove(address.Length - 1);
                MessageBox.Show("Address should be less than 40 letters.");
            }
            else
            {
                foreach (char c in address)
                {
                    if (c == '@')
                    {
                        address2Text.Text = address.Remove(address.Length - 1);
                        MessageBox.Show("Address cannot include @");
                    }
                }
            }
        }

        private void sizeText_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String packageSize = lSizeText.Text;
            if (packageSize.Length > 6)
            {
                lSizeText.Text = packageSize.Remove(packageSize.Length - 1);
                MessageBox.Show("The length should be less than 7.");
            }
                        String size = lSizeText.Text;
                        for (int i = 0; i < lSizeText.Text.Length; i++)
                        {
                            char c = size[i];
                            if (c != '.')
                            {
                                if (!('0' <= c && c <= '9'))
                                    lSizeText.Text = size.Remove(i, 1);
                            }
                        }
        }

        private void packingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (!packingCheckBox.Checked)
            {
                shippingCheckBox.Checked = false;
                shippingCheckBox.Enabled = false;
                actualWeightText.ReadOnly = true;
                lSizeText.ReadOnly = true;
                wSizeText.ReadOnly = true;
                hSizeText.ReadOnly = true;
                shipMethodGroupBox.Enabled = false;
                orderLevelGroupBox.Enabled = false;
                payWayGroupBox.Enabled = false;
                if (role.Equals("Administrator"))
                {
                    addSkuBtn.Enabled = true;
                    addItemBtn.Enabled = true;
                }
                skuDataGridView.ReadOnly = false;
                itemDataGridView.ReadOnly = false;
            }
            else
            {
                shippingCheckBox.Enabled = true;
                actualWeightText.ReadOnly = false;
                lSizeText.ReadOnly = false;
                wSizeText.ReadOnly = false;
                hSizeText.ReadOnly = false;
                shipMethodGroupBox.Enabled = false;
                payWayGroupBox.Enabled = false;
                addSkuBtn.Enabled = false;
                addItemBtn.Enabled = false;
                skuDataGridView.ReadOnly = true;
                itemDataGridView.ReadOnly = true;
            }
        }

        public void calculateWeight()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            
            double totalWeight = 0;
            foreach (DataRow r in itemDs.Tables[0].Rows)
            {
                String item = r[0].ToString();
                double quantity = Convert.ToInt32(r[2].ToString());
                double weight = Convert.ToDouble(conn.getDataSet("use " + databaseName + " select weight from item where item='" + item + "';").Tables[0].Rows[0][0].ToString().Trim());
                
                totalWeight += quantity * weight;
            }
            totalWeightText.Text = totalWeight.ToString();
            
        }

        private void shippingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (shippingCheckBox.Checked)
            {
                actualWeightText.ReadOnly = true;
                lSizeText.ReadOnly = true;
                wSizeText.ReadOnly = true;
                hSizeText.ReadOnly = true;
                shipMethodGroupBox.Enabled = true;
                payWayGroupBox.Enabled = true;
            }
            else
            {
                actualWeightText.ReadOnly = false;
                lSizeText.ReadOnly = false;
                wSizeText.ReadOnly = false;
                hSizeText.ReadOnly = false;
                shipMethodGroupBox.Enabled =false;
                orderLevelGroupBox.Enabled = false;
                payWayGroupBox.Enabled = false;
            }
            saveBtn.Enabled = false;
        }

        private void checkExtraItemBtn_Click(object sender, EventArgs e)
        {
            //SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            ExtraItem ex = new ExtraItem(this);
            ex.ShowDialog();
        }

        private void PackingSlip_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            //if is new slipOrder
            minusRunningSlipOrder();

            //if is check slipOrder
            if (!isNew)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                conn.removeRunningSlipOrder(slipOrder, localIPAddress, checkinTime);
            }
            

        }

        private void actualWeightText_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String weight = actualWeightText.Text;
            for (int i = 0; i < weight.Length; i++)
            {
                char c = weight[i];
                if (c != '.')
                {
                    if (!('0' <= c && c <= '9'))
                        actualWeightText.Text = weight.Remove(i, 1);
                }
            }
            if (actualWeightText.Text.Length > 10)
            {
                actualWeightText.Text = actualWeightText.Text.Remove(actualWeightText.Text.Length - 1);
            }
                
        }

 
        private void closeBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Do you want to close this window?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void nameText_TextChanged(object sender, EventArgs e)
        {
            String name = nameText.Text;
            if (name.Length > 20)
            {
                nameText.Text = name.Remove(name.Length - 1);
                MessageBox.Show("The length of name should be less than 20.");
            }
            else
            {
                Boolean valid = true;

                for (int i = 0; i < name.Length; i++)
                {
                    char c = name[i];
                    if (!(c=='/'||c=='\\' ||c == '.' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c ==' '))
                    {
                        name = name.Remove(i,1);
                        valid = false;
                    }
                }
                nameText.Text = name;
                if (!valid)
                    MessageBox.Show("The name can only be composed by letters.");
            }
        }

        private void checkNewAddedItemBtn_Click(object sender, EventArgs e)
        {
            EditedSkuOrItem exForm = new EditedSkuOrItem(this);
            exForm.ShowDialog();
        }

        private void radioUps_CheckedChanged(object sender, EventArgs e)
        {
            if (radioUps.Checked)
            {
                upsCombo.Enabled = true;
                uspsCombo.Enabled = false;
                upsCombo.DroppedDown = drop;
            }
            else
            {
                upsCombo.Enabled = false;
                upsCombo.SelectedIndex = -1;
            }
        }

        private void radioUsps_CheckedChanged(object sender, EventArgs e)
        {
            if (radioUsps.Checked)
            {
                uspsCombo.Enabled = true;
                upsCombo.Enabled = false;
                uspsCombo.DroppedDown = drop;
            }
            else
            {
                uspsCombo.Enabled = false;
                uspsCombo.SelectedIndex = -1;
            }
        }

        private void upsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (upsCombo.SelectedIndex == -1 && uspsCombo.SelectedIndex == -1)
            {
                expCombo.SelectedIndex = -1;
                expCombo.Items.Clear();
                orderLevelGroupBox.Enabled = false;
            }   
            if (upsCombo.SelectedIndex!=-1 && uspsCombo.SelectedIndex ==-1)
            {
                orderLevelGroupBox.Enabled = true;
                expCombo.Items.Clear();
                expCombo.Items.Add("Expedited");
                expCombo.Items.Add("UPS 3-Day");
                expCombo.Items.Add("UPS 2-Day");
                expCombo.Items.Add("UPS Next-Day");
            }

        }

        private void uspsCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (upsCombo.SelectedIndex == -1 && uspsCombo.SelectedIndex == -1)
            {
                expCombo.SelectedIndex = -1;
                expCombo.Items.Clear();
                orderLevelGroupBox.Enabled = false;
            }
            if (uspsCombo.SelectedIndex != -1 && upsCombo.SelectedIndex == -1)
            {
                orderLevelGroupBox.Enabled = true;
                expCombo.Items.Clear();
                expCombo.Items.Add("Expedited");
                expCombo.Items.Add("USPS Mail");
                expCombo.Items.Add("USPS Priority");
                expCombo.Items.Add("USPS Express");
            }
        }

        private void radioExp_CheckedChanged(object sender, EventArgs e)
        {
            if (radioExp.Checked)
            {
                expCombo.Enabled = true;
                expCombo.DroppedDown = drop;
            }
            else
            {
                expCombo.Enabled = false;
                expCombo.SelectedIndex = -1;
            }
        }

        private void updateEditItemBySku()
        {
            SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
            editItemBySku.Clear();
            foreach (String[] s in skuList)
            {
                DataTable dt = conn.getDataSet("use " + databaseName + " select item, itemQuantity from sku_item where sku='" + s[0] + "';").Tables[0];
                foreach (DataRow rr in dt.Rows)
                {
                    //rr[0] item, rr[1] itemQuantity  in database
                    Boolean isNe = true;
                    foreach (String[] ss in editItemBySku)
                    {
                        if (ss[0].Equals(rr[0].ToString()))
                            isNe = false;
                    }
                    if (isNe)
                    {
                        String[] row = new String[2];
                        row[0] = rr[0].ToString();
                        row[1] = (Convert.ToDecimal(rr[1]) * Convert.ToDecimal(s[1])).ToString();
                        editItemBySku.Add(row);
                    }
                    else
                    {
                        foreach (String[] ss in editItemBySku)
                        {
                            if (ss[0].Equals(rr[0].ToString()))
                                ss[1] = (Convert.ToDecimal(ss[1]) + Convert.ToDecimal(rr[1]) * Convert.ToDecimal(s[1])).ToString();
                        }
                    }
                }
            }
        }

 
        private void skuDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //make sure that quantity is digit only
            Boolean valid = true;
            foreach (char c in skuDataGridView.CurrentCell.Value.ToString())
            {
                if (c > '9' || c < '0')
                    valid = false;                
            }
            if (skuDataGridView.CurrentCell.Value.ToString() == "")
                valid = false;
            if (!valid)
            {
                skuDataGridView.CurrentCell.Value = skuCurrentValue;
                MessageBox.Show("Quantity should be digit greater than 0.");
            }

            //check overflow and signal the skuList
            if (valid)
            {
                Boolean hasOverflow = false;
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                
                Decimal editedSkuQty = Convert.ToDecimal(skuDataGridView.CurrentCell.Value)-Convert.ToDecimal(skuCurrentValue);
                Decimal totalEditedSkuQty = editedSkuQty;
                filterList();
                //update skuList
                Boolean isNew = true;
                foreach (String[] s in skuList)
                {
                    if (s[0].Equals(skuDataGridView[0, e.RowIndex].Value.ToString()))
                        isNew = false;
                }
                if (isNew)
                {
                    String[] ss = new String[2];
                    ss[0] = skuDataGridView[0, e.RowIndex].Value.ToString();
                    ss[1] = editedSkuQty.ToString();
                    skuList.Add(ss);
                }
                else
                {
                    foreach (String[] s in skuList)
                    {
                        if (s[0].Equals(skuDataGridView[0, e.RowIndex].Value.ToString()))
                        {
                            totalEditedSkuQty += Convert.ToDecimal(s[1]);
                            s[1] = totalEditedSkuQty.ToString();
                        }

                    }
                }
                
                //update editItemBySku
                updateEditItemBySku();

                List<String> overflowItem = new List<String>();
                foreach (String[] r in editItemBySku)
                {
                    Decimal qtyInSku = Convert.ToDecimal(r[1]);
                    Decimal restQtyInDatabase = Convert.ToDecimal(conn.getDataSet("use " + databaseName + " select quantity from item where item='" + r[0].ToString() + "';").Tables[0].Rows[0][0]);
                    Decimal totalEditedQty = qtyInSku;
                    foreach (String[] s in editItemByItem)
                    {
                        if (s[0].Equals(r[0].ToString()))
                            totalEditedQty += Convert.ToDecimal(s[1]);
                    }

                    Decimal restQty = restQtyInDatabase - totalEditedQty;
                    if (restQty < 0)
                    {
                        hasOverflow = true;
                        overflowItem.Add(r[0].ToString());
                    }
                }
                if (hasOverflow)
                {
                    //revert the data
                    skuDataGridView.CurrentCell.Value = skuCurrentValue;
                    foreach (String[] s in skuList)
                    {
                        if (s[0].Equals(skuDataGridView[0, e.RowIndex].Value.ToString()))
                        {
                            s[1] = (Convert.ToDecimal(s[1]) - editedSkuQty).ToString();
                        }
                    }
                    updateEditItemBySku();

                    String overflowItems = null;
                    foreach (String s in overflowItem)
                        overflowItems += s + " ";
                    MessageBox.Show("Error! This may causes Item#: "+overflowItems+" quantity overflow.");
                }
            }

            refreshItemDataGridView(skuDs.Tables[0],itemDs.Tables[0]);
        }

        private void skuDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        { 
            if (skuDataGridView.CurrentCell.Value.ToString() != "")
            {
                skuDataGridView.Columns[2].ReadOnly = false;
                skuCurrentValue = skuDataGridView.CurrentCell.Value.ToString();
            }
            else
                skuDataGridView.Columns[2].ReadOnly = true;
        }

        private void itemDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (itemDataGridView.CurrentCell.Value.ToString() != "")
            {
                itemDataGridView.Columns[2].ReadOnly = false;
                itemCurrentValue = itemDataGridView.CurrentCell.Value.ToString();
            }
            else
                itemDataGridView.Columns[2].ReadOnly = true;
        }

        private void itemDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //make sure that quantity is digit only
            Boolean valid = true;
            foreach (char c in itemDataGridView.CurrentCell.Value.ToString())
            {
                if (c > '9' || c < '0')
                    valid = false;
            }
            if (itemDataGridView.CurrentCell.Value.ToString() == "")
                valid = false;
            if (!valid)
            {
                itemDataGridView.CurrentCell.Value = itemCurrentValue;
                MessageBox.Show("Quantity should be digit greater than 0.");
            }

            //check overflow and signal the skuList
            if (valid)
            {
                SkuServer.ConnectSql conn = (SkuServer.ConnectSql)Activator.GetObject(typeof(SkuServer.ConnectSql), "tcp://" + IPAddress + ":" + port + "/ConnectSql");
                
                Decimal editedQty = Convert.ToDecimal(itemDataGridView.CurrentCell.Value) - Convert.ToDecimal(itemCurrentValue);
                Decimal totalEditedQty = editedQty;
                               
                foreach (String[] r in editItemBySku)
                {
                    if (r[0].Equals(itemDataGridView[0, e.RowIndex].Value.ToString()))
                        totalEditedQty += Convert.ToDecimal(r[1]);
                }
                Decimal restQtyInDatabase = Convert.ToDecimal(conn.getDataSet("use " + databaseName + " select quantity from item where item='" + itemDataGridView[0, e.RowIndex].Value.ToString() + "';").Tables[0].Rows[0][0]);
                    
                foreach (String[] s in editItemByItem)
                {
                    if (s[0].Equals(itemDataGridView[0, e.RowIndex].Value.ToString()))
                        totalEditedQty += Convert.ToDecimal(s[1]);
                }

                Decimal restQty = restQtyInDatabase - totalEditedQty;
                
                
                if (restQty>=0)
                {
                    //store the data
                    Boolean isNew = true;
                    foreach (String[] s in editItemByItem)
                    {
                        if (s[0].Equals(itemDataGridView[0, e.RowIndex].Value.ToString()))
                        {
                            isNew = false;
                        }
                    }
                    if (isNew)
                    {
                        String[] s = new String[2];
                        s[0] = itemDataGridView[0, e.RowIndex].Value.ToString();
                        s[1] = editedQty.ToString();
                        editItemByItem.Add(s);
                    }
                    else
                    {
                        foreach (String[] s in editItemByItem)
                        {
                            if (s[0].Equals(itemDataGridView[0, e.RowIndex].Value.ToString()))
                            {
                                s[1] = (Convert.ToDecimal(s[1]) + editedQty).ToString();
                            }
                        }
                    }
                }
                else
                {
                     MessageBox.Show("Error! This may causes Item#: " + itemDataGridView[0, e.RowIndex].Value.ToString() + " quantity overflow.");
                     itemDataGridView.CurrentCell.Value = itemCurrentValue;
                }      
            }
            calculateWeight();
        }

        private void wSizeText_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String packageSize = wSizeText.Text;
            if (packageSize.Length > 6)
            {
                wSizeText.Text = packageSize.Remove(packageSize.Length - 1);
                MessageBox.Show("The length should be less than 7.");
            }
            String size = wSizeText.Text;
            for (int i = 0; i < wSizeText.Text.Length; i++)
            {
                char c = size[i];
                if (c != '.')
                {
                    if (!('0' <= c && c <= '9'))
                        wSizeText.Text = size.Remove(i, 1);
                }
            }
        }

        private void hSizeText_TextChanged(object sender, EventArgs e)
        {
            saveBtn.Enabled = false;
            String packageSize = hSizeText.Text;
            if (packageSize.Length > 6)
            {
                hSizeText.Text = packageSize.Remove(packageSize.Length - 1);
                MessageBox.Show("The length should be less than 7.");
            }
            String size = hSizeText.Text;
            for (int i = 0; i < hSizeText.Text.Length; i++)
            {
                char c = size[i];
                if (c != '.')
                {
                    if (!('0' <= c && c <= '9'))
                        hSizeText.Text = size.Remove(i, 1);
                }
            }
        }

        private void reportBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "Do you want to open this report in PDF?", "Warning Message!", MessageBoxButtons.YesNo) == DialogResult.No)
                return;

            Document report = new Document(PageSize.A4);
            String direct = Directory.GetCurrentDirectory() + @"\Report\SlipOrder\";
            if(!Directory.Exists(direct))
                Directory.CreateDirectory(direct);
            PdfWriter writer = PdfWriter.GetInstance(report, new FileStream( direct + "S"+ slipOrderLabel.Text + ".pdf" , FileMode.Create));
            report.Open();
            report.Add(new Paragraph("SlipOrder Report: " + slipOrderLabel.Text +"\n"));
            float[] widthTitle= {0.3f,0.7f};
            PdfPTable table = new PdfPTable(widthTitle);
            table.SpacingBefore = 15f;
            PdfPCell cell = new PdfPCell(new Phrase(slipOrderLabel.Text));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.PaddingTop = 3.5f;
            cell.FixedHeight = 20;
            cell.Colspan = 2;
            cell.BorderWidthTop = 1.5f;
            table.AddCell(cell);
            table.AddCell("ShipDate");
            table.AddCell(Convert.ToDateTime(shipDatePicker.Value).ToString());
            table.AddCell("Customer Name");
            table.AddCell(nameText.Text);
            table.AddCell("Address");
            table.AddCell(address1Text.Text+" "+address2Text.Text);
            table.AddCell("City");
            table.AddCell(cityText.Text+", "+stateText.Text);
            table.WidthPercentage = 100;
            if (packingCheckBox.Checked)
            {
                cell = new PdfPCell(new Phrase("Pack Details"));
                cell.PaddingTop = 3.5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Colspan = 2;
                cell.BorderWidthTop = 1.5f;
                table.AddCell(cell);
                table.AddCell("Size(L*W*H)");
                table.AddCell(lSizeText.Text+" * "+wSizeText.Text+" * "+hSizeText.Text);
                table.AddCell("Weight(LBS)");
                table.AddCell(totalWeightText.Text+" LBS");
            }
            if (shippingCheckBox.Checked)
            {
                cell = new PdfPCell(new Phrase("Ship Details"));
                cell.PaddingTop = 3.5f;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.Colspan = 2;
                cell.BorderWidthTop = 1.5f;
                table.AddCell(cell);
                table.AddCell("Payment Way");
                table.AddCell(payCombo.SelectedItem.ToString());
                table.AddCell("Ship Method");
                if(radioUps.Checked)
                    table.AddCell("UPS"+ " "+ upsCombo.SelectedItem.ToString());
                else
                    table.AddCell("USPS" + " " + uspsCombo.SelectedItem.ToString());
                table.AddCell("Order Level");
                if(radioRgl.Checked)
                    table.AddCell("Regular");
                else
                    table.AddCell("Expedited"+" "+expCombo.SelectedItem.ToString());
            }
            report.Add(table);

            float[] widthDetail = { 0.2f, 0.6f, 0.2f };
            table = new PdfPTable(widthDetail);
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell = new PdfPCell(new Phrase("SKU Details"));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.PaddingTop = 3.5f;
            cell.FixedHeight = 20;
            cell.Colspan = 3;
            cell.BorderWidthTop = 1.5f;
            table.AddCell(cell);
            table.DefaultCell.BackgroundColor = new BaseColor(200, 200, 200);
            table.AddCell("SKU#");
            table.AddCell("Description");
            table.AddCell("Quantity");
            table.DefaultCell.BackgroundColor = BaseColor.WHITE;
            if (skuDataGridView.Rows.Count >= 2)
                for (int i = 0; i < skuDataGridView.Rows.Count-1; i++)
                {
                    table.AddCell(skuDataGridView.Rows[i].Cells[0].Value.ToString());
                    table.AddCell(skuDataGridView.Rows[i].Cells[1].Value.ToString());
                    table.AddCell(skuDataGridView.Rows[i].Cells[2].Value.ToString());
                }
            else
                table.AddCell("(Null)");
            table.DefaultCell.BackgroundColor = new BaseColor(200, 200, 200);
            table.AddCell("Item#");
            table.AddCell("Description");
            table.AddCell("Quantity");
            table.DefaultCell.BackgroundColor = BaseColor.WHITE;
            if (itemDataGridView.Rows.Count >= 2)
                for (int i = 0; i < itemDataGridView.Rows.Count - 1; i++)
                {
                    table.AddCell(itemDataGridView.Rows[i].Cells[0].Value.ToString());
                    table.AddCell(itemDataGridView.Rows[i].Cells[1].Value.ToString());
                    table.AddCell(itemDataGridView.Rows[i].Cells[2].Value.ToString());
                }
            else
                table.AddCell("(Null)");
            table.WidthPercentage = 100;
            report.Add(table);
            report.Close();

            Process pr = new Process();
            pr.StartInfo.FileName = direct + "S" + slipOrderLabel.Text + ".pdf";
            
            /*this code for print directly
             * 
            pr.StartInfo.Verb = "Print";
            pr.StartInfo.CreateNoWindow = true;
             *
             */ 
            pr.Start();
        }


       
 
    }
}
